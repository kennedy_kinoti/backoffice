<?php

    return [ 
    // Side menu
    'dashboard' => 'Dashibodi',
    'payments' => 'Malipo',
    'refunds' => 'Marejesho',
    'manage_users' => 'Simamia Watumiaji',
    'manage_sub_accounts' => 'Simamia Akaunti Ndogo',
    'remittance_details' => 'Maelezo ya Usaidizi',
    'remittance_reports' => 'Ripoti za Ruhusa',
    'invoicing' => 'Invoice',
    'disburse' => 'Tumia',
    'archived_online_data' => 'Data iliyohifadhiwa mtandaoni',
    'analytics' => 'Analytics',
    'mail' => 'Barua',
    'log_out' => 'Ingia nje',

    // Top nav

    // Breadcrumbs

    // Body div

        // Accounts Page
        'account_details' => 'Maelezo ya Akaunti',
        'bank_details' => 'Maelezo ya Benki',
        'bank_name' => 'Jina la Benki',

        'monday' => 'Jumatatu',  
        'tuesday' => 'Jumanne', 
        'wednesday' => 'Jumatano',  
        'thursday' => 'Alhamisi',  
        'friday' => 'Ijumaa',
    ];
?>