<!DOCTYPE html>

<html>

<head>

	<title>{{ __('header.online_transactions') }}</title>

</head>

<body>

	<h1>{{ $title }}</h1>

	<table style="width:100%">
		<tr bgcolor="3377FF">
			<th>{{ __('header.order_id') }}</th>
			<th>{{ __('header.sender') }}</th> 
			<th>{{ __('header.email') }}</th>
			<th>{{ __('header.date') }}</th>
			<th>{{ __('header.code') }}</th>
			<th>{{ __('header.channel') }}</th>
			<th>{{ __('header.amount') }}</th>
		</tr>
		<tr>
			<td>16631-6A5BD7</td>
			<td>Smith</td> 
			<td>gabjairus@gmail.com</td>
			<td>2018-03-23 12:49:35</td>
			<td>1539630706</td>
			<td>Visa</td>
			<td>5.00</td>
		</tr>
		<tr>
			<td>52311-6A5BD7</td>
			<td>Tonny</td> 
			<td>tonny@gmail.com</td>
			<td>2018-03-23 12:49:35</td>
			<td>1539630706</td>
			<td>Mastercard</td>
			<td>395.00</td>
		</tr>
	</table>

</body>

</html>
