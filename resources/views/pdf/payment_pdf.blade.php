<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<!-- <body> -->
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.ipay_backoffice') }}</div>

	<div class="panel-body" id='app'>
		<div class="table-responsive">

			<table border="1" style="width:100%" class="table table-hover table-striped table-condensed table-bordered">
				<caption>{{ __('header.payments_summary') }}</caption>
				<thead>
					<tr>
						<th>{{ __('header.order_id') }}</th>
						<th>{{ __('header.sender') }}</th>
						<th>{{ __('header.date') }}</th>
						<th>{{ __('header.code') }}</th>
						<th>{{ __('header.channel') }}</th>
						<th>{{ __('header.amount') }}</th>
					</tr>
				</thead>
				<tbody>
					@if(empty($data))
						<tr>
							<td colspan="6" align="center">
								{{ __('header.data_found') }} !!
							</td>
						</tr>
					@else
					@foreach($data as $payment)
						<tr>
							<td>{{$payment['txncode']}}</td>
							<td>{{$payment['fname']}} {{$payment['lname']}}</td>
							<td>{{$payment['receivingdatetime']}}</td>
							<td>{{$payment['txncode']}}</td>
							<td>{{$payment['sendernumber']}}</td>
							<td>{{$payment['txnamt']}}</td>						
						</tr>
					@endforeach 
					@endif 
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- </body> -->

</html>
