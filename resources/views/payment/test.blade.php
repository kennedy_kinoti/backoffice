@extends('layouts.app')

@section('title', 'Payments')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard')}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Payments</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">Payments</div>
    <div class="panel-body">

        <div class="row">
               
            <form method ="POST" action="{{ url('payments/search')}}">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="form-group col-md-4">
                    <!-- <label>Transaction code</label> -->
                    <input type="text" name="code" class="form-control" id="code" placeholder="TRANSACTION Code">
                </div>
                <!-- <div class="form-group col-md-2">
                    <label>From</label>
                    <input type="date" class="form-control datepicker" id="date_from" placeholder="From Date" name="date_from">
                </div>
                <div class="form-group col-md-2">
                    <label>To</label>
                    <input type="date" class="form-control datepicker" id="date_to" placeholder="To Date" value="<?php echo date('Y-M-d')?>" name="date_to">
                </div> -->
                <div class="form-group col-md-4">
                <!-- <label>From</label> -->
                    <input type="text"  class="form-control" name="datefilter" value="" placeholder="Date Range"/>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success">Search</button>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"> Export</a>
                </div>
            </form>
            
            <!-- <div class="form-group col-md-1">
                <a href="{{url('/payments/export')}}" name="export" class="btn btn-danger"> PDF</a>
            </div> -->
            <div class="form-group col-md-1">
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">
                    <span class="glyphicon glyphicon-collapse-down"></span> Advanced
                </button>
            </div>
            <div id="demo" class="collapse col-md-12">
                <div class="form-group col-md-3">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled>CHANNEL</option>
                        <option>MPESA</option>
                        <option>Airtel Money</option>
                        <option>Equitel</option>
                        <option>eLipa</option>
                        <option>Kenswitch</option>
                        <option>Visa</option>
                        <option>Mastercard</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="oid" placeholder="Order ID" name="oid"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="fname" placeholder="Client First Name" name="fname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="lname" placeholder="Client Last Name" name="lname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"/>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed" id="payment-table">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Channel</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

 <script>
         $(function() {
               $('#payment-table').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ url('payments.search') }}',
               columns: [
                        { data: 'txncode', name: 'txncode' },
                        { data: 'sendernumber', name: 'sendernumber' },
                        { data: 'receivingdatetime', name: 'receivingdatetime' },
                        { data: 'txnamt', name: 'txnamt' },
                        { data: 'fname', name: 'fname' }
                     ]
            });
         });
         </script>
@endsection