@extends('layouts.app')

@section('title', 'Payments')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.payments') }}</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.payments') }}</div>
    <div class="panel-body">

        <div class="row">
               
            <form method ="GET" action="{{ url('payments/search')}}">
               @csrf
                <div class="form-group col-md-4">
                    <!-- <label>Transaction code</label> -->
                    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('header.transaction_code') }}" autocomplete="off">
                </div>
                <!-- <div class="form-group col-md-2"> -->
                    <!-- <label>From</label> -->
                    <!-- <input type="text"  value="" class="form-control datepicker" id="date_from" placeholder="From" name="date_from" autocomplete="off"> -->
                <!-- </div> -->
                <!-- <div class="form-group col-md-2"> -->
                    <!-- <label>To</label> -->
                    <!-- <input type="text"  value="" class="form-control datepicker" id="date_to" placeholder="To"  name="date_to" autocomplete="off"> -->
                <!-- </div> -->
                <div class="form-group col-md-4">
                <!-- <label>From</label> -->
                    <input type="text"  class="form-control" name="datefilter" value="" placeholder="{{ __('header.date_range') }}" autocomplete="off"/>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success">{{ __('header.search') }}</button>
                </div>

                @if (!empty($data))
                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"
                        
                    > {{ __('header.export') }}</a>
                </div>
                @endif
            </form>
            
            <!-- <div class="form-group col-md-1">
                <a href="{{url('/payments/export')}}" name="export" class="btn btn-danger"> PDF</a>
            </div> -->
            <div class="form-group col-md-1">
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">
                    <span class="glyphicon glyphicon-collapse-down"></span> {{ __('header.advanced') }}
                </button>
            </div>
            <div id="demo" class="collapse col-md-12">
                <div class="form-group col-md-3">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled>{{ __('header.channel') }}</option>
                        <option>MPESA</option>
                        <option>Airtel Money</option>
                        <option>Equitel</option>
                        <option>eLipa</option>
                        <option>Kenswitch</option>
                        <option>Visa</option>
                        <option>Mastercard</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="oid" placeholder="Order ID" name="oid"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="fname" placeholder="Client First Name" name="fname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="lname" placeholder="Client Last Name" name="lname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"/>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th>{{ __('header.code') }}</th>
                        <th>{{ __('header.channel') }}</th>
                        <th>{{ __('header.date') }}</th>
                        <th>{{ __('header.amount') }}</th>
                        <th>{{ __('header.name') }}</th>
                        <th>{{ __('header.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($data))
                        <tr>
                            <td colspan="5" align="center">
                                {{ __('header.search_first') }}
                            </td>
                        </tr>
                    @else
                    @foreach($data as $payment)
                        <tr>
                            <td>{{$payment->txncode}}</td>
                            <td>{{$payment->sendernumber}}</td>
                            <td>{{$payment->receivingdatetime}}</td>  
                            <td>{{$payment->txnamt}}</td>
                            <td>{{$payment->fname}} {{$payment->lname}}</td>
                            <td>
                                <button name="refund" data-payid="{{$payment->id}}" data-toggle="modal" data-target="#detailsModal{{$payment->id}}" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-eye"></i> {{ __('header.view') }}</button>

                                <div class="modal fade" id="detailsModal{{$payment->id}}" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                
                                                <h5 class="modal-title" id="detailsModalLabel">{{__('header.transaction_details') }}</h5>
                                                
                                                <button type="button" class="close" data-dismiss="modal" data-modal="" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>   
                                            </div>

                                            <div class="modal-body">
                                                @if(empty($payment->id))
                                                    <tr><td>{{ __('header.response') }}</td></tr>
                                                @else
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><b>{{ __('header.client_name') }} </b>:</td>
                                                        <td>{{$payment->fname}} {{$payment->lname}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.transaction_code')}}</b>:</td>
                                                        <td>{{$payment->txncode}}</td>
                                                        <input name="email" value="{{$payment->txncode}}" placeholder="email" hidden/>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.order_id') }} </b>:</td>
                                                        <td>{{$payment->runid}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.date_time') }} </b>:</td>
                                                        <td>{{$payment->receivingdatetime}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.channel') }} </b>:</td>
                                                        <td>{{$payment->sendernumber}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.amount') }} </b>:</td>
                                                        <td>{{$payment->txnamt}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.type') }} </b>:</td>
                                                        <td>{{$payment->curr}}</td>
                                                    </tr>
                                    
                                                    <tr hidden>
                                                        <td><b>{{ __('header.card_mask') }} </b>:</td>
                                                        <td>3213 XXXX XXXX XXXX</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">{{ __('header.close') }}</button>
                                                <a href="{{url('/payments/export')}}"><button type="submit" class="btn btn-danger">{{ __('header.export') }}</button></a>

                                                <a hidden href="{{url('/refund/partial_initiate/'.$payment->id)}}"><button type="button" class="btn btn-primary">{{ __('header.partial_refund') }}</button></a>
            
                                                <a href="{{url('/refund/full_initiate/'.$payment->id)}}"><button type="button" class="btn btn-success">{{ __('header.full_refund') }}</button></a>
                                            </div>
                                            
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach 
                    @endif 
                </tbody>
            </table>
        </div>
        @if(!empty($data))
            {{ $data }}
        @endif
    </div>
</div>
@endsection