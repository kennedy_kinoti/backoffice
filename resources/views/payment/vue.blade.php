@extends('layouts.app')

@section('title', 'Payments Vue JS')


@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.payment') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.payments_vue') }}</div>
    <div class="panel-body" id="app">
        
        <search-component></search-component>
        
    </div>
</div>
@endsection