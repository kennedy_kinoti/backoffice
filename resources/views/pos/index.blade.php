@extends('layouts.app')

@section('title', 'POS')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.pos_pdq_service') }}</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.pos_pdq_service') }}</div>
    <div class="panel-body">

        <form method ="POST" action="{{ url('pos/send')}}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            
            <div class="form-group ">
                <label>{{ __('header.amount') }} *</label>
                <input type="number" min="0" autocomplete="off" required="" name="amount" class="form-control" placeholder="{{ __('header.amount') }}" value="">
            </div>
            <div class="form-group ">
                <label>{{ __('header.narration') }}</label>
                <input type="text" autocomplete="off" required="" name="desc" class="form-control" id="desc" placeholder="eg POS Cashier 1" value="iPay Backoffice POS Request">
            </div>
            <div class="form-group">
                <button class="btn btn-hg btn-block btn-info" type="submit"> {{ __('header.send') }}</button>
            </div>
        </form>
        
    </div>
</div>
@endsection