@extends('layouts.app')
@section('title', 'Virtual POS')

@section('breadcrumb')
<nav aria-label="breadcrumb">
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
      <li class="breadcrumb-item"><a href="#">{{ __('header.pos') }}</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ __('header.virtual_pos') }}</li>
   </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
      <div class="panel-heading">
         <h3 class="panel-title"><i class="icon-file-text-alt"></i> {{ __('header.create_invoice') }}
         </h3>
      </div>
      <div class="panel-body">
         <!-- The Form -->
         <form method="post" class="form-horizontal" action="{{url('pos/virtual_pay')}}" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

            <input name="live" type="hidden" value="0"/>
            <input name="vid" type="hidden" value="demoChanged"/>
            <input name="p1" type="hidden" value="airtel"/>
            <input name="p2" type="hidden" value="020102292999"/>
            <input name="p3" type="hidden" value=""/>
            <input name="p4" type="hidden" value="100"/>
            <input name="cbk" type="hidden" value='$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]'/>
            <input name="cst" type="hidden" value="1"/>
            <input name="crl" type="hidden" value="2"/>
            <input name="hashkey" type="hidden" value="demoChanged"/>
            <?php //$generated_hash = hash_hmac('sha1',$datastring , $hashkey); ?>
            <input name="hsh" type="hidden" value="<?php //echo $generated_hash ;?>"/>
            <!-- Logo -->
            <div class="form-group">
               <label for="f_name" class="col-lg-3 control-label">{{ __('header.client_logo') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="file" value="" name="logo" class="form-control" id="logo" placeholder="Insert client's logo">                        
               </div>
            </div>
            <!-- Input First Name -->
            <div class="form-group">
               <label for="f_name" class="col-lg-3 control-label">{{ __('header.client_name') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" value="" name="name" class="form-control" id="f_name" placeholder="Insert client's name" required>                        
               </div>
            </div>
            <!-- Email Field -->
            <div class="form-group">
               <label for="eml" class="col-lg-3 control-label">{{ __('header.email') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" name="eml" class="form-control" id="eml" placeholder="Insert client's email" required>                        
               </div>
            </div>
            <!-- Phone Number Input -->
            <div class="form-group">
               <label for="tel" class="col-lg-3 control-label">{{ __('header.telephone_number') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" name="tel" class="form-control" id="tel" placeholder="Insert client's telephone number" required>                        
               </div>
            </div>
            <!-- Itinerary Textarea -->
            <div class="form-group">
               <label class="col-lg-3 control-label">{{ __('header.payment_details') }}</label>                        
               <div class="col-lg-6">                        
                  <textarea name="itinerary" class="form-control" rows="6" required></textarea>                        
               </div>
            </div>
            <?php 
                function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'){
                    $pieces = [];
                    $max = mb_strlen($keyspace, '8bit') - 1;
                    for ($i = 0; $i < $length; ++$i) {
                        $pieces []= $keyspace[random_int(0, $max)];
                    }
                    return implode('', $pieces);
                }

                $a = random_str(5, '0123456789');
                $b = random_str(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

            ?>
            <!-- Invoice Number -->
            <div class="form-group">
                <label for="inv_no" class="col-lg-3 control-label">{{ __('header.invoice_number') }}</label>                        
                <div class="col-lg-6"> 
                    <input type="text" name="inv" class="form-control" id="inv" placeholder="Generate a unique ID" value="{{strtoupper($a.'-'.$b)}}">                        
                </div>

            </div>
            <!-- Order Number -->
            <div class="form-group">
               <label for="oid" class="col-lg-3 control-label">{{ __('header.order_id') }}</label>                        
               <div class="col-lg-6">
                  <input type="text" name="oid" class="form-control" id="oid" placeholder="Generate a unique Order">                        
               </div>
            </div>
            <!-- Select currency checkbox -->
            <div class="form-group">
               <label class="col-lg-3 control-label">{{ __('header.select_currency') }}</label>                        
               <div class="col-lg-6">
                  <select class="form-control" name="curr" required>
                     <option value="">{{ __('header.pick_currency') }}</option>
                     <option value="KES"  selected="selected">KES</option>
                     <option value="USD"  selected="selected">USD</option>
                  </select>
               </div>
            </div>
            <!-- Amount -->
            <div class="form-group">
               <label for="ttl" class="col-lg-3 control-label">{{ __('header.total_amount') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="number" value="" class="form-control" id="ttl" name="ttl" placeholder="Amount" required autocomplete="off">                        
               </div>
            </div>
            <!-- Submit and Reset Buttons -->
            <div class="form-group">
               <label class="col-lg-3 control-label"></label>                    
               <div class="col-lg-6">                    
                  <button name="invoice" value="invoice" type="submit" class="btn btn-primary">{{ __('header.create_invoice') }}</button>
                  <button name="proformainvoice" value="proformainvoice" type="submit" class="btn btn-info">{{ __('header.pro_forma_invoice') }}</button>                    
                  <button type="reset" class="btn btn-default">{{ __('header.reset') }}</button>                    
               </div>
            </div>

            <?php
                // $fields = array("live"=> "0",
                // "oid"=> $_POST['order_id'],
                // "inv"=> $_POST['inv_no'],
                // "ttl"=> $_POST['amount'],
                // "tel"=> $_POST['tel_number'],
                // "eml"=> $_POST['email'],
                // "vid"=> "demoChanged",
                // "curr"=> $_POST['curr'],
                // "p1"=> "airtel",
                // "p2"=> "020102292999",
                // "p3"=> "",
                // "p4"=> $_POST['amount'],
                // "cbk"=> $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"],
                // "cst"=> "1",
                // "crl"=> "2"
                // );
    
                // $datastring =  $fields['live'].$fields['oid'].$fields['inv'].$fields['ttl'].$fields['tel'].$fields['eml'].$fields['vid'].$fields['curr'].$fields['p1'].$fields['p2'].$fields['p3'].$fields['p4'].$fields['cbk'].$fields['cst'].$fields['crl'];
        
                // $hashkey ="demoChanged";//use "demo" for testing where vid also is set to "demo"
        
                // $generated_hash = hash_hmac('sha1',$datastring , $hashkey);


            ?>

         </form>
      </div>
      <!-- end of panel body -->                    
   </div>
   <!-- end of panel -->
</div>
@endsection