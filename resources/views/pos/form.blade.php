@extends('layouts.app')

@section('title', 'POS')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.payments') }}</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.manually_insert_payment') }}</div>
    <div class="panel-body">

        <form method ="POST" action="{{ url('pos/submit')}}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group ">
                <label>{{ __('header.sender_number') }} *</label>
                <select class="form-control" name="sendernumber">
                    <option value="MPESA">MPESA</option>
                    <option value="AIRTEL">AIRTEL</option>
                    <option value="TELKOM">TELKOM</option>
                </select>
            </div>

            <div class="form-group ">
                <label>{{ __('header.tnx_code') }} *</label>
                <input type="text" autocomplete="off" required="" name="txncode" class="form-control" placeholder="{{ __('header.tnx_code') }}" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.first_name') }} *</label>
                <input type="text" autocomplete="off" required="" name="fname" class="form-control" placeholder="John" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.last_name ') }}*</label>
                <input type="text" autocomplete="off" required="" name="lname" class="form-control" placeholder="Doe" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.msisn_id') }} *</label>
                <input type="text" autocomplete="off" required="" name="msisn" class="form-control" placeholder="MSISN" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.vendor_id') }} *</label>
                <input type="number" min="0" autocomplete="off" required="" name="vendor_id" class="form-control" placeholder="{{ __('header.vendor_id') }}" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.ids_Commission') }} *</label>
                <input type="number" min="0" autocomplete="off" required="" name="comm" class="form-control" placeholder="1.00" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.amount') }} *</label>
                <input type="number" min="0" autocomplete="off" required="" name="amount" class="form-control" placeholder="{{ __('header.currency') }}" value="">
            </div>

            <div class="form-group ">
                <label>{{ __('header.currency') }} *</label>
                <input type="text" autocomplete="off" required="" name="currency" class="form-control" placeholder="{{ __('header.currency') }}" value="">
            </div>
            
            <div class="form-group">
                <button class="btn btn-hg btn-block btn-info" type="submit"> {{ __('header.send') }}</button>
            </div>
        </form>
        
    </div>
</div>
@endsection