<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="theme-color" content="#337ab7">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="iPay Africa Backoffice"/>
        <link rel="shortcut icon" href="{{{ asset('images/favicon.png') }}}">

        <title>{{ config('app.name') }}</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="manifest" href="{{ asset('manifest.json') }}" />

    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar" class="open">
            
                <ul class="list-unstyled components">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn animate open" style="margin-left:10px;">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                    <br><br>
                    <!-- B2B -->
                    <!--  <li {{ (Request::is('pos/form') ? 'class = active' : '') }}>
                        <a href="{{url('/pos/form')}}"> <span class="glyphicon glyphicon-ok-circle"  data-toggle="tooltip" data-placement="top" title="Form"></span> <span id="lipa"> POS Form </span> </a>
                    </li> -->

                    <!-- Payments -->
                    <!-- <li {{ (Request::is('vue-payment') ? 'class = active' : '') }} >
                        <a href="{{url('/vue-payment')}}" data-toggle="tooltip" data-placement="top" title="Payments"> <span class="fa fa-barcode" ></span> <span id="lipa" >Payments - (Vue)</span></a>
                    </li> -->
                    <li {{ (Request::is('payments') || Request::is('payments/search') ? 'class = active' : '') }} >
                        <a href="{{url('payments')}}" data-toggle="tooltip" data-placement="top" title="Payments"> <span class="fa fa-blind" ></span> <span id="lipa" >{{ __('header.payments') }} </span></a>
                    </li>


                    <!-- Refunds -->
                    
                    @if(Gate::check('is_checked', 'initiated') || Gate::check('is_checked', 'initiated') || Gate::check('is_checked', 'refunded_payments'))
                    <li>
                        <a href="#refunds" data-toggle="collapse" aria-expanded="false"> <span class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="Refunds"></span> <span id="lipa"> {{ __('header.refunds') }} </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('refund/view') || Request::is('refund/complete') || Request::is('refunded') ? 'in' : '') }}" id="refunds">
                            @can('is_checked', 'initiated')
                            <li {{ (Request::is('refund/view') ? 'class = active' : '') }}><a href="{{url('/refund/view')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.initiated') }}</a></li>
                            @endcan
                            @can('is_checked', 'completed')
                            <li {{ (Request::is('refund/complete') ? 'class = active' : '') }}><a href="{{url('/refund/complete')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.completed') }}</a></li>
                            @endcan
                            @can('is_checked', 'refunded_payments')
                            <li {{ (Request::is('refunded') ? 'class = active' : '') }}> <a href="{{url('/refunded')}}" ><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.refunded_payments') }} </a></li>
                            @endcan
                            <!-- <li {{ (Request::is('refund/view') ? 'class = active' : '') }}><a href="{{url('/refund/view')}}"> Vue Refunds </a></li> -->
                            
                        </ul>
                    </li>
                    @endif

                    <!-- User Account Management -->
                    <li>
                        <a href="#settings" data-toggle="collapse" aria-expanded="false" ><span class="glyphicon glyphicon-user"  data-toggle="tooltip" data-placement="top" title="Manage Users"></span> <span id="lipa"> {{ __('header.manage_users') }} </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('user/profile') || Request::is('user/create') || Request::is('user/manage') ? 'in' : '') }}" id="settings">
                            <li {{ (Request::is('user/profile') ? 'class = active' : '') }}><a href="{{url('user/profile')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.profile') }}</a></li>
                            @can('is_checked', 'add_users')
                            <li {{ (Request::is('user/create') ? 'class = active' : '') }}><a href="{{url('/user/create')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.create_user') }}</a></li>
                            @endcan
                            @can('is_checked', 'manage_users')
                            <li {{ (Request::is('user/manage') || Request::is('user/manage/{id?}/edit') ? 'class = active' : '') }}><a href="{{url('user/manage')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.manage_user') }}</a></li>
                            @endcan
                        </ul>
                    </li>
                    
                    <!-- Manage Account -->
                    @if(Gate::check('is_checked', 'add_sub_accounts') || Gate::check('is_checked', 'sub_accounts_action'))
                    <li>
                        <a href="#manageSub" data-toggle="collapse" aria-expanded="false"><span class="glyphicon glyphicon-cog"  data-toggle="tooltip" data-placement="top" title="Manage Sub Accounts"></span> <span id="lipa"> {{ __('header.manage_sub_accounts') }} </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('add-sub') || Request::is('list-sub') ? 'in' : '') }}" id="manageSub">
                            @can('is_checked', 'add_sub_accounts')
                            <li {{ (Request::is('add-sub') ? 'class = active' : '') }}><a href="{{url('/add-sub')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.sub_accounts') }}</a></li>
                            @endcan
                            @can('is_checked', 'sub_accounts_action')
                            <li {{ (Request::is('list-sub') ? 'class = active' : '') }}><a href="{{url('/list-sub')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.sub_accounts_action') }}</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endif
                    <!-- Account -->
                    <li {{ (Request::is('account') ? 'class = active' : '') }}>
                        <a href="{{url('/account')}}"> <span class="fa fa-cogs"  data-toggle="tooltip" data-placement="top" title="Remittance Details"></span> <span id="lipa"> {{ __('header.remittance_details') }} </span> </a>
                    </li>

                    <li {{ (Request::is('rem_account') ? 'class = active' : '') }}>
                        <a href="{{url('/rem_account')}}"> <span class="fa fa-cogs"  data-toggle="tooltip" data-placement="top" title="Remittance"></span> <span id="elipa"> {{ 'Remittance Account' }} </span> </a>
                    </li>
                    <!-- Bank Remittance -->
                    <!-- <li {{ (Request::is('remittance') ? 'class = active' : '') }}>
                        <a href="{{url('/remittance')}}"> <span class="fa fa-institution"  data-toggle="tooltip" data-placement="top" title="Remittance Reports"></span> <span id="lipa"> {{ __('header.remittance_reports') }} </span> </a>
                    </li> -->
                    <!-- Invoicing -->

                    @if(Gate::check('is_checked', 'create_invoices') || Gate::check('is_checked', 'manage_invoices') || Gate::check('is_checked', 'confirm_invoices'))
                    <li>
                        <a href="#invoicing" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-file-text" data-toggle="tooltip" data-placement="top" title="Invoicing"></span>  <span id="lipa"> {{ __('header.invoicing') }} </span> </a>

                        <ul class="collapse list-unstyled {{ (Request::is('invoice/create') || Request::is('invoice') || Request::is('invoice/list')? 'in' : '') }}" id="invoicing">
                            @can('is_checked', 'create_invoices')
                             <li {{ (Request::is('invoice/create') ? 'class = active' : '') }}><a href="{{url('invoice/create')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.create_invoice') }} </a></li> @endcan
                            @can('is_checked', 'manage_invoices')  
                            <li {{ (Request::is('invoice') ? 'class = active' : '') }}><a href="{{url('invoice')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.view_invoice') }} </a></li>@endcan
                            @can('is_checked', 'confirm_invoices')
                            <li {{ (Request::is('invoice/list') ? 'class = active' : '') }}><a href="{{url('invoice/list')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.view_sent') }}</a></li>@endcan
                        </ul>
                    </li>
                    @endif
                    
                    <!-- Disbursed Payments -->
                    @if(Gate::check('is_checked', 'manage_bulk_pay') || Gate::check('is_checked', 'view_bp_reports') || Gate::check('is_checked', 'view_bp_archived'))
                    <li>
                        <a href="#bulkpay" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-money"  data-toggle="tooltip" data-placement="top" title="Bulk Pay"></span> <span id="lipa"> {{ __('header.disburse') }} </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('bulk/manage') || Request::is('bulk') || Request::is('bulk/archived') || Request::is('b2b')? 'in' : '') }}" id="bulkpay">
                            @can('is_checked', 'manage_bulk_pay')
                            <li {{ (Request::is('bulk/manage') ? 'class = active' : '') }}><a href="{{url('bulk/manage')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.manage_bulk_pay') }}</a></li>
                            @endcan
                            @can('is_checked', 'view_bp_reports')
                            <li {{ (Request::is('bulk') ? 'class = active' : '') }}><a href="{{url('bulk')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.view_bulk_pay_reports') }}</a></li>
                            @endcan
                            @can('is_checked', 'view_bp_archived')
                            <li {{ (Request::is('bulk/archived') ? 'class = active' : '') }}><a href="{{url('bulk/archived')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i>{{ __('header.view_bulk_pay_archived') }}</a></li>
                            @endcan
                            <!-- <li {{ (Request::is('b2b') ? 'class = active' : '') }}>
                                <a href="{{url('/b2b')}}"> <span class="glyphicon glyphicon-share"  data-toggle="tooltip" data-placement="top" title="B2B"></span> <span id="lipa"> B2B </span> </a>
                            </li> -->
                        </ul>
                    </li>
                    @endif

                    <!-- B2B -->
                    <li {{ (Request::is('b2b') ? 'class = active' : '') }}>
                        <a href="{{url('/b2b')}}"> <span class="glyphicon glyphicon-share"  data-toggle="tooltip" data-placement="top" title="B2B"></span> <span id="lipa"> B2B </span> </a>
                    </li>

                    <!-- POS -->
                    @if(Gate::check('is_checked', 'pos_pdq_service') || Gate::check('is_checked', 'virtual_pos'))
                    <li>
                        <a href="#pos" data-toggle="collapse" aria-expanded="false"> <span class="glyphicon glyphicon-object-align-vertical"  data-toggle="tooltip" data-placement="top" title="POS"></span> <span id="lipa"> POS </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('pos') || Request::is('pos/virtual')? 'in' : '') }}" id="pos">
                            @can('is_checked', 'pos_pdq_service')
                            <li {{ (Request::is('pos') ? 'class = active' : '') }}><a href="{{url('/pos')}}"> <i class="fa fa-chevron-right" aria-hidden="true" title="POS"></i>{{ __('header.pos_pdq_service') }}</a></li>
                            @endcan
                            @can('is_checked', 'virtual_pos')
                            <li {{ (Request::is('pos/virtual') ? 'class = active' : '') }}><a href="{{url('/pos/virtual')}}"> <i class="fa fa-chevron-right" aria-hidden="true" title="POS"></i>{{ __('header.virtual_pos') }}</a></li>
                            @endcan
                        </ul>
                    </li>
                    @endif

                    <!-- Archived Online Data -->
                    <!-- <li>
                        <a href="#archived" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-briefcase"  data-toggle="tooltip" data-placement="top" title="Archived Data"></span> <span id="lipa"> {{ __('header.archived_online_data') }} </span></a>
                        <ul class="collapse list-unstyled {{ (Request::is('archived/success') || Request::is('archived/suspense') || Request::is('archived/pos')? 'in' : '') }}" id="archived">
                            <li {{ (Request::is('archived/success') ? 'class = active' : '') }}><a href="{{url('archived/success')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.success') }}</a></li>
                            <li {{ (Request::is('archived/suspense') ? 'class = active' : '') }}><a href="{{url('archived/suspense')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.suspense') }}</a></li>
                            <li {{ (Request::is('archived/pos') ? 'class = active' : '') }}><a href="{{url('archived/pos')}}"><i class="fa fa-chevron-right" aria-hidden="true"></i> {{ __('header.pos') }}</a></li>
                        </ul>
                    </li> -->

                    <!-- Analytics Menus -->
                    <li {{ (Request::is('dashboard') ? 'class = active' : '') }}>
                        <a href="{{url('/dashboard')}}"> <span class="glyphicon glyphicon-dashboard"  data-toggle="tooltip" data-placement="top" title="Analytics"></span> <span id="lipa"> {{ __('header.analytics') }} </span> </a>
                    </li>

                    <!-- Mail Menus -->
                    <!-- <li {{ (Request::is('basicemail') ? 'class = active' : '') }}>
                        <a href="{{url('/basicemail')}}"> <span class="glyphicon glyphicon-envelope"  data-toggle="tooltip" data-placement="top" title="Mail *"></span> <span id="lipa"> {{ __('header.mail') }} </span> </a>
                    </li> -->
                    
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="{{url('/logout')}}" class="download"> <span class="fa fa-power-off"></span> <span id="lipa"> {{ __('header.log_out') }}</span></a></li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            
                            <a href="{{url('/')}}" class="navbar-brand"><span class="text-light">{{ config('app.name') }}</span></a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                            <ul class="nav navbar-nav navbar-right">
                            
                                <li class="nav-item dropdown" >
                                    <button class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown" type="button" aria-expanded="false" role="button">
                                        <!-- <i class="fa fa-language text-dark"  data-toggle="tooltip" title="Language Switcher" ></i> -->
                                        <i class="material-icons" style="font-size: 20px">language</i>
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" id="languageSwitcher">
                                        <li><a href="{{ route('lang', 'en') }}">English</a></li>
                                        <li><a href="{{ route('lang', 'swa') }}">Swahili</a></li>
                                        <li><a href="{{ route('lang', 'fr') }}">French</a></li>
                                    </ul>
                                    &nbsp
                                    
                                    <!-- <button class="btn btn-info"> -->
                                        <!-- <span class="glyphicon glyphicon-bell text-default"></span> -->
                                        <!-- <i class="material-icons" style="font-size: 20px">notification_important</i>
                                        <span class="badge badge-dark">5</span>
                                    </button> -->
                                        
                                    &nbsp
                                    <!-- Authentication Links -->
                                    @guest
                                        <li>
                                            <button class="btn btn-info" onclick="return window.location='{{ route('login') }}'">
                                                {{ __('Login') }}
                                            </button>
                                        </li>
                                        <!-- <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li> -->
                                    @else
                                        <li class="nav-item dropdown">
                                            <button id="navbarDropdown" class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <!-- <span class="glyphicon glyphicon-user"></span> -->
                                            <i class="material-icons" style="font-size: 20px">face</i>
                                               
                                                <span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="languageSwitcher">
                                                <li><a class="dropdown-item" href="" > {{ Auth::user()->name }}'s Account </a></li>
                                                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a></li>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </ul>

                                        </li>
                                    @endguest
                              
                            </ul>
                        </div>
                    </div>
                </nav>
                @yield('breadcrumb')

                <!-- Append content -->
                @yield('content')
                <!-- End of append content -->

                <!-- Footer -->

            </div>
        
        <style>
            .badge-notify{
                background:red;
                position:relative;
                transform: (-100%, -100%);
            }
        </style>

        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function (event) {
                    console.log(event.target)
                    $('#sidebar, #content, #lipa').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    event.preventDefault();
                });
            });
        </script>

        <script>
        if ('serviceWorker' in navigator ) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('../../service-worker.js',{
                    scope: '.'
                }).then(function(registration) {
                    // Registration was successful
                    console.log('Service Worker registration successful with scope: ', registration.scope);
                }, function(err) {
                    // registration failed :(
                    console.log('Service Worker registration failed: ', err);
                });
            });
        }
        </script>

        <script type="text/javascript">
            $(function() {

                $('input[name="datefilter"]').daterangepicker({
                    showDropdowns: true,
                    autoUpdateInput: false,
                });

                $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));

                });

                $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });

            });
        </script>
    </body>
</html>
