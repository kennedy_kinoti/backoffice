@extends('layouts.app')

@section('title', 'Success Online')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.successful') }}</div>
    <div class="panel-body">
        
        <form method ="POST" action="{{ action('OnlineController@pdf')}}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group col-md-4">
                <input type="text" name="search" class="form-control" id="exampleInputSearch" placeholder="Search">
            </div>
            <div class="form-group col-md-3">
                <input type="text" class="form-control datepicker" id="from_date" placeholder="From Date" name="from_date">
            </div>
            <div class="form-group col-md-3">
                <input type="text" class="form-control datepicker" id="to_date" placeholder="To Date" value="<?php echo date('Y-M-d')?>" name="to_date">
            </div>
            <div class="form-group col-md-1">
                <a href="{{url('/success/filter')}}"  class="btn btn-success">{{ __('header.filter') }}</a>
            </div>
            
        </form>
        <div class="form-group col-md-1">
            <a href="{{url('/success/pdf')}}" class="btn btn-danger">{{ __('header.print') }}</a>
        </div>

        <table class="table table-striped table-condensed" id="list">
            <thead>
                <tr>
                    <th>{{ __('header.date') }}</th>
                    <th>{{ __('header.order_id') }}</th>
                    <th>{{ __('header.vendor_id') }}</th>
                    <th>{{ __('header.sender') }}</th>
                    <th>{{ __('header.email') }}</th>
                    <th>{{ __('header.telephone_number') }}</th>
                    <th>{{ __('header.code') }}</th>
                    <th>{{ __('header.status') }}</th>
                    <th>{{ __('header.channel') }}</th>
                    <th>{{ __('header.amount') }}</th>
                    <th>{{ __('header.action') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2018-04-10<br />17:10:27</td>
                    <td>112</td>
                    <td>0000</td>
                    <td>James </td>
                    <td>kajuej@gmailo.com</td>
                    <td>256712375678</td>
                    <td>143869176</td>
                    <td>Confirmed</td>
                    <td>VISA</td>
                    <td>KES <b>5.00</b></td>
                    <td>
                        <!-- <form action="https://backoffice.ipayafrica.com/index.php/online/refund_success" method="post" accept-charset="utf-8">            <input type="hidden" value="143869176" name="refund_details">             -->
                            <input type="hidden" value="KES" name="refund_curr">
                            <input type="hidden" value="5.00" name="refund_amt">
                            <button name="refund" data-toggle="modal" data-target="#detailsModal" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-reply"></i>{{ __('header.view_more') }}</button>
                        <!-- </form> -->
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailsModalLabel">{{ __('header.transaction_details') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <tr colspan="3">
                        <td><b>{{ __('header.order_id') }} </b>:</td>
                        <td> 1001</td>
                    </tr>
                    <tr>
                        <td><b>{{ __('header.date_time') }}Date Time </b>:</td>
                        <td>21/06/2018 11:03:12</td>
                    </tr>
                    <tr>
                        <td><b>{{ __('header.channel') }} </b>:</td>
                        <td>MPESA</td>
                    </tr>
                    <tr>
                        <td><b>{{ __('header.amount') }} </b>:</td>
                        <td>12400</td>
                    </tr>
                    <tr>
                        <td><b>{{ __('header.type') }} </b>:</td>
                        <td> Payment </td>
                    </tr>
                    <tr>
                        <td><b>{{ __('header.card_mask') }} </b>:</td>
                        <td> 3213 XXXX XXXX XXXX</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('header.close') }}</button>
                <!-- <button type="button" class="btn btn-success">Print</button> -->
                <a href="{{url('/success/pdf')}}"><button type="submit" class="btn btn-danger">{{ __('header.print') }}</button></a>
                <button type="button" class="btn btn-primary">{{ __('header.refund') }}</button>
            </div>
        </div>
    </div>
</div>
@endsection