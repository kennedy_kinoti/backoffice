@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.failed') }}</div>
    <div class="panel-body">
        
        <form class="form-inline">
            <div class="form-group">
                <label class="sr-only" for="from_date"></label>
                <input type="text" class="form-control datepicker" id="from_date" placeholder="{{ __('header.from_date') }}" name="from_date">
            </div>
            <div class="form-group">
                <label class="sr-only" for="to_date"></label>
                <input type="text" class="form-control datepicker" id="to_date" placeholder="{{ __('header.to_date') }}" value="<?php echo date('Y-M-d')?>" name="to_date">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputSearch">{{ __('header.search') }}</label>
                <input type="text" name="search" class="form-control" id="exampleInputSearch" placeholder="Search">
            </div>

            <button type="submit" class="btn btn-default">{{ __('header.filter') }}</button>
        </form>

        <table class="table table-striped table-condensed" id="list">
            <thead>
                <tr>
                    <th>{{ __('header.date') }}</th>
                    <th>{{ __('header.sender') }}</th>
                    <th>{{ __('header.code') }}</th>
                    <th>{{ __('header.email') }}</th>
                    <th>{{ __('header.amount') }}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <tr role="row" class="odd">
                <td class="sorting_1">2018-05-15 13:28:01</td>
                <td>0705118708</td>		
                <td>919736232</td>
                <td>kiseej@gmail.com</td>
                <td>KES 588.00</td>
                <!--<td>
                    <form action="https://backoffice.ipayafrica.com/index.php/online/refund" method="post" accept-charset="utf-8">			<input type="hidden" value="919736232" name="refund_details">
                    <button name="refund" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-success pending"><i class="icon-reply"></i></button>
                    </form>
                </td>-->
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection