@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.refund') }}</div>
    <div class="panel-body">

        <table class="table table-striped table-condensed" id="list">
            <thead>
                <tr>
                    <th>{{ __('header.initiate_date') }}</th>
                    <th>{{ __('header.initiate_user') }}</th>
                    <th>{{ __('header.reference') }}</th>
                    <th>{{ __('header.actions') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection