@extends('layouts.app')

@section('title', 'Manage Users')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.users') }}</li>
  </ol>
</nav>
@endsection

@section('content')

@if (session('successMsg'))
    <div class="alert alert-success">
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger">
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading"> {{ __('header.view_users') }} </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th>{{ __('header.name') }}</th>
                        <th>{{ __('header.email') }}</th>
                        <th>{{ __('header.username') }}</th>
                        <th>{{ __('header.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user):?>
                        <tr>
                            <td>{{ $user->name }} </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->username }}</td>

                            @if($user->id == auth()->user()->id)
                            <td>
                                <a class="btn btn-info" style="width: auto" id="test" onclick="this.innerHTML = 'Hello - {{ $user->name }}'" >{{ 'User logged In' }}</a>
                            </td>
                            @else
                            <td>
                                <a class="btn btn-info" href="{{URL::to('user/manage/'.$user->id.'/edit')}}">{{ __('header.edit') }}</a>
                                <a class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$user->id}}">{{ __('header.delete') }}</a>

                                <!-- Edit Modal -->
                                <div id="editModal{{$user->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">{{ __('header.edit_user_access_control') }}</h4>
                                            </div>

                                            <div class="modal-body">
                                                
                                                <!-- Edit modal data -->
                                                
                                                <!-- End of modal data -->

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('header.close') }}</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- Delete Modal -->
                                <div id="deleteModal{{$user->id}}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">{{ __('header.delete_this_user') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>{{ __('header.delete_user') }} ? </p>
                                            <p> <b>{{ __('header.name') }} : </b> {{$user->name}}</p>
                                            <p> <b>{{ __('header.username') }} : </b> {{$user->username}}</p>
                                            <p> <b>{{ __('header.email') }} : </b> {{$user->email}}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="reset" class="btn btn-secondary pull-left" data-dismiss="modal">{{ __('header.close') }}</button>
                                            <a href="{{url('user/manage/'.$user->id.'/delete')}}"><button type="button" class="btn btn-danger">{{ __('header.confirm') }}</button></a>
                                        </div>
                                        </div>

                                    </div>
                                </div>
                            </td>
                            @endif
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>
</div>

@endsection