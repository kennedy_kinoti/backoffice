@extends('layouts.app')

@section('title', 'Edit Profile')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.edit_profile') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
  <div class="panel-heading">{{ __('header.edit_profile') }}</div>
  <div class="panel-body">
    <form method="post" action="{{route('profile_update')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
      <div class="row">
        <div class="form-group col-md-12">
          <label for="name">{{ __('header.name') }}:</label>
          <input type="text" class="form-control" name="name" required value={{ Auth::user()->name}}>
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-12">
          <label for="email">{{ __('header.email') }}:</label>
          <input type="email" class="form-control" name="email" required value={{ Auth::user()->email}}>
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-12">
          <label for="vid">{{ __('header.vendor_id') }}:</label>
          <input type="text" class="form-control" name="vid" required value={{ Auth::user()->vendor_id}}>
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-12">
          <label for="username">{{ __('header.username') }}:</label>
          <input type="text" class="form-control" name="username" required value={{ Auth::user()->username}}>
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-12">
          <label for="password">{{ __('header.password') }}</label>
          <input type="password" class="form-control" name="password" placeholder="********">    
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-12">
          <label for="cpassword">{{ __('header.confirm_password') }}</label>
          <input type="password" class="form-control" name="cpassword" placeholder="********">    
        </div>
      </div>

      <div class="row">
        <div class="form-group col-md-8">
          <button type="submit" class="btn btn-success">{{ __('header.save_changes') }}</button>
          <button type="submit" class="btn btn-default">{{ __('header.cancel') }}</button>
        </div>

      </div>
    </form>
  </div>
</div>
@endsection