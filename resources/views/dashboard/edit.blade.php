@extends('layouts.app')

@section('title', 'Manage Users')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage') }}</a></li>
    <li class="breadcrumb-item"><a href="{{url('user/manage')}}">{{ __('header.users') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.edit') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="well">
    <div class="panel-heading">{{ __('header.edit_user_access_control') }}</div>
    <form method="post" autocomplete="false" action="{{url('user/manage/'.$users->id.'/update')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.edit_user_details') }}</div>
            
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-md-12">
                    <label for="name">{{ __('header.name') }}:</label>
                    <input type="text" class="form-control" name="name" value= {{ $users->name }} required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="email">{{ __('header.email') }}:</label>
                        <input type="email" class="form-control" name="email" value= {{$users->email}} required>
                    </div>
                    </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="username">{{ __('header.username') }}:</label>
                        <input type="text" class="form-control" name="username" value= {{$users->username}} required>
                    </div>
                    </div>
                <div class="row">
                    <div class="form-group col-md-12">
                    <label for="password">{{ __('header.password') }}</label>
                    <input type="password" class="form-control" name="password">    
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                    <label for="cpassword">{{ __('header.confirm_password') }}</label>
                    <input type="password" class="form-control" name="cpassword" >    
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">Edit Access Control</div>
            <div class="panel-body">

                <?php session(['test_me' => $users->views_parameters]); ?>

                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.invoicing') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="create_invoices" value="1" 
                            @if(isset(json_decode($users->views_parameters)->create_invoices) == 1) { checked = "checked" }
                            @endif > {{ __('header.create_invoices') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="manage_invoices" value="1"
                            @if(isset(json_decode($users->views_parameters)->manage_invoices) == 1) { checked = "checked" }
                            @endif > {{ __('header.view_invoices') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="confirm_invoices"  value="1" 
                            @if(isset(json_decode($users->views_parameters)->confirm_invoices) == 1) { checked = "checked" }
                            @endif > {{ __('header.confirm_invoices') }}
                        </div>
                    </div>
                </div>

                <div class="well well-sm">
                <!--user rights -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.user_settings') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="add_users" value="1" 
                            @if(isset(json_decode($users->views_parameters)->add_users) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.create_user') }}
                        </div> 

                        <div class="col-md-3"><input  type="checkbox" name="manage_users" value="1" 
                            @if(isset(json_decode($users->views_parameters)->manage_users) == 1) { checked = "checked"  }  
                            @endif > {{ __('header.view_users') }}
                        </div>

                        <div class="col-md-3"></div>
                    </div>
                </div>

                <div class="well well-sm">
                <!-- Refunds -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.refunds') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="refunded_payments" value="1" 
                            @if(isset(json_decode($users->views_parameters)->refunded_payments) == 1) { checked = "checked"  } @endif > {{ __('header.refunded_payments') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="initiated" value="1" 
                            @if(isset(json_decode($users->views_parameters)->initiated) == 1) { checked = "checked"  } @endif > {{ __('header.initiated') }}
                            
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="completed" value="1" 
                            @if(isset(json_decode($users->views_parameters)->completed) == 1) { checked = "checked"  } @endif > {{ __('header.completed') }}
                            
                        </div>
                    </div>
                </div>

                <div class="well well-sm">
                <!-- online transactions -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.online_transactions') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="online_success" value="1" 
                            @if(isset(json_decode($users->views_parameters)->online_success) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.successful') }}
                        </div>
                        
                        <div class="col-md-3"><input  type="checkbox" name="online_suspense" value="1" 
                            @if(isset(json_decode($users->views_parameters)->online_suspense) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.suspense') }}
                        </div>
                        
                        <div class="col-md-3"><input  type="checkbox" name="online_failed" value="1" 
                            @if(isset(json_decode($users->views_parameters)->online_failed) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.failed') }}
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"><input  type="checkbox" name="online_initiate_ref" value="1" 
                            @if(isset(json_decode($users->views_parameters)->online_initiate_ref) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.initiate_refunds') }}
                        </div>
                        
                        <div class="col-md-3"><input  type="checkbox" name="online_complete_ref" value="1"
                            @if(isset(json_decode($users->views_parameters)->online_complete_ref) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.complete_refunds') }}
                        </div>
                    </div>


                </div>

                <div class="well well-sm">
                <!-- pos transactions -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.pos_transactions') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="virtual_pos" value="1" 
                            @if(isset(json_decode($users->views_parameters)->virtual_pos) == 1) { checked = "checked"  } @endif > {{ __('header.virtual_pos') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="pos_pdq_service" value="1" 
                            @if(isset(json_decode($users->views_parameters)->pos_pdq_service) == 1) { checked = "checked"  } @endif > {{ __('header.pos_pdq_service') }}
                            
                        </div>

                    </div>
                </div>

                <div class="well well-sm">
                <!--Manage Accounts -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.manage_sub_accounts') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="add_sub_accounts" value="1" 
                            @if(isset(json_decode($users->views_parameters)->add_sub_accounts) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.sub_accounts') }}
                        </div> 

                        <div class="col-md-3"><input  type="checkbox" name="sub_accounts_action" value="1" 
                            @if(isset(json_decode($users->views_parameters)->sub_accounts_action) == 1) { checked = "checked"  }  
                            @endif > {{ __('header.sub_accounts_action') }}
                        </div>

                        <div class="col-md-3"></div>
                    </div>
                </div>


                <!--end of view parameters -->
                <div class="well well-sm">
                <!-- Disbursement rights -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.disburse') }}</b></div>

                        <div class="col-md-3"><input  type="checkbox" name="init_bulkpay" value="1" 
                            @if(isset(json_decode($users->views_parameters)->init_bulkpay) == 1) { checked = "checked"  } @endif > {{ __('header.initiate_bulkpay') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="fund_bulkpay" value="1" 
                            @if(isset(json_decode($users->views_parameters)->fund_bulkpay) == 1) { checked = "checked"  } @endif >  {{ 'Complete Bulkpay' }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="manage_bulk_pay" value="1" 
                            @if(isset(json_decode($users->views_parameters)->manage_bulk_pay) == 1) { checked = "checked"  } @endif > {{ __('header.manage_bulk_pay') }}
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>

                        <div class="col-md-3"><input  type="checkbox" name="view_bp_reports" value="1" 
                            @if(isset(json_decode($users->views_parameters)->view_bp_reports) == 1) { checked = "checked"  } @endif > {{ __('header.view_bulk_pay_reports') }}
                        </div>

                        <div class="col-md-3"><input  type="checkbox" name="view_bp_archived" value="1" 
                            @if(isset(json_decode($users->views_parameters)->view_bp_archived) == 1) { checked = "checked"  } @endif > {{ __('header.view_bulk_pay_archived') }}
                        </div>

                    </div>

                </div>

                <div class="well well-sm">
                <!-- bank remittance -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.bank_remittance') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="bank_remittance" value="1" 
                            @if(isset(json_decode($users->views_parameters)->bank_remittance) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.view_bank_remittance') }}
                        </div>
                    </div>
                </div>
                <!-- end of bulk pay rights -->

                <div class="well well-sm">
                <!-- device access -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.device_access') }}</b></div>
                        <div class="col-md-3"><input type="checkbox" name="device_web" value="1" 
                        @if(isset(json_decode($users->views_parameters)->device_web) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.web_only') }} </div>

                        <div class="col-md-3"><input type="checkbox" name="device_app" value="1" 
                        @if(isset(json_decode($users->views_parameters)->device_app) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.app_only') }} </div>

                        <div class="col-md-3"><input type="checkbox" name="device_all" value="1"
                        @if(isset(json_decode($users->views_parameters)->device_all) == 1) { checked = "checked"  } 
                            @endif > {{ __('header.webapp') }} </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-8">
                        <button type="submit" value="submit" class="btn btn-success">{{ __('header.save_changes') }}</button>
                        <button type="reset" value="reset" class="btn btn-default">{{ __('header.cancel') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

@endsection
