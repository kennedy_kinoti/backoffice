@extends('layouts.app')

@section('title', 'Manage Users')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.create_user') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="well border-primary">
    <div class="panel-heading">{{ __('header.user_with_access_control') }}</div>
    <form method="post" action="{{url('user/store')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.user_profile') }}</div>

            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="name">{{ __('header.name') }}:</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="email">{{ __('header.email') }}:</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                    </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="username">{{ __('header.username') }}:</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    </div>
                <div class="row">
                    <div class="form-group col-md-12">
                    <label for="password">{{ __('header.password') }}</label>
                    <input type="password" class="form-control" name="password" required>    
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                    <label for="cpassword">{{ __('header.confirm_password') }}</label>
                    <input type="password" class="form-control" name="cpassword" required>    
                    </div>
                </div>
            </div>
                
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading"> {{ __('header.user_access_control') }}</div>
            <div class="panel-body">

                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.invoicing') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="create_invoices" value="1"> {{ __('header.create_invoices') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="manage_invoices" value="1"> {{ __('header.view_invoices') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="confirm_invoices"  value="1"> {{ __('header.confirm_invoices') }}</div>
                    </div>
                </div>

                <div class="well well-sm">
                <!--users rights -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.user_settings') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="add_users" value="1"> {{ __('header.create_user') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="manage_users" value="1"> {{ __('header.view_users') }}</div>
                        <div class="col-md-3"></div>
                    </div>
                </div>

                <div class="well well-sm">
                <!-- Refunds -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.refunds') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="refunded_payments" value="1"> {{ __('header.refunded_payments') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="initiated" value="1"> {{ __('header.initiated') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="completed" value="1"> {{ __('header.completed') }}</div>
                    </div>
                </div>

                <div class="well well-sm">
                <!-- online transactions -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.online_transactions') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="online_success" value="1"> {{ __('header.successful') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="online_suspense" value="1"> {{ __('header.suspense') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="online_failed" value="1"> {{ __('header.failed') }}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-3"><input  type="checkbox" name="online_initiate_ref" value="1"> {{ __('header.initiate_refunds') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="online_complete_ref" value="1"> {{ __('header.complete_refunds') }}</div>
                        <div class="col-md-3"></div>
                    </div>

                </div>

                <div class="well well-sm">
                <!-- pos transactions -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.pos_transactions') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="pos_success" value="1"> {{ __('header.pos') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="pos_initiate_ref" value="1"> {{ __('header.initiate_refunds') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="pos_complete_ref" value="1"> {{ __('header.complete_refunds') }}</div>
                    </div>
                </div>


                <!--end of view parameters -->
                <div class="well well-sm">
                <!-- bulk pay rights -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.bulk_pay') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="confirm_bulkpay" value="1"> {{ __('header.confirm_bulkpay') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="init_bulkpay" value="1"> {{ __('header.initiate_bulkpay') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="fund_bulkpay" value="1"> {{ __('header.fund_bulkpay') }}</div>
                    </div>
                </div>

                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.bank_remittance') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="bank_remittance" value="1"> {{ __('header.view_bank_remittance') }}</div>
                        <div class="col-md-3"></div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <!-- end of bulk pay rights -->

                <div class="well well-sm">
                <!-- Device -->
                    <div class="row">
                        <div class="col-md-3"><b>{{ __('header.device_type') }}</b></div>
                        <div class="col-md-3"><input  type="checkbox" name="bank_remittance" value="1"> {{ __('header.web_only') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="bank_remittance" value="1"> {{ __('header.app_only') }}</div>
                        <div class="col-md-3"><input  type="checkbox" name="bank_remittance" value="1"> {{ __('header.webapp') }}</div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-8">
                        <button type="submit" class="btn btn-success">{{ __('header.save_changes') }}</button>
                        <button type="submit" class="btn btn-default">{{ __('header.cancel') }}</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

@endsection