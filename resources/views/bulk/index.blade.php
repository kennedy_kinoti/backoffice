@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.bulkpay') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.view') }}</li>
  </ol>
</nav>
@endsection

@section('content')

{{ $invoices->links() }}

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.view') }} {{ __('header.bulkpay') }}</div>
    <div class="panel-body">
        <!--Search Form-->
        <div class="row"> 
            <form action="{{ url('bulk/search') }}" method="get">
                @csrf
                <div class="form-group col-md-4">
                    <input type="search" name="search" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" value="search" class="btn btn-primary">{{ __('header.search') }}</button>
                </div>
            </form>
        </div>
         <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed">
                <thead>
                    <tr>
                        <th>{{ __('header.date') }}</th>
                        <th>Cr</th>
                        <th>Dr</th>
                        <th>{{ __('header.commission') }}</th>
                        <th>{{ __('header.reference') }}</th>
                        <th>{{ __('header.telephone_number') }}</th>
                        <th>{{ __('header.mobile_code') }}</th>
                        <th>{{ __('header.transaction_status') }}</th>
                        <th>{{ 'Running Balance' }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($invoices))
                        <tr>
                            <td colspan="8" align="center">
                                {{ __('header.search_first') }}
                            </td>
                        </tr>
                    @else
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->dttime}}</td>
                            <td>{{$invoice->cr}}</td>
                            <td>{{$invoice->dr}}</td>
                            <td>{{$invoice->ipay_comm}}</td>
                            <td>{{$invoice->ref}}</td>
                            <td>{{$invoice->telephone}}</td>
                            <td>{{$invoice->mmref}}</td>
                            <td><span class="badge">
                                @if($invoice->status == 1)
                                {{ __('header.successful') }}
                                @else
                                {{ __('header.failed') }}
                                @endif
                            </span></td>
                            <td><span class="badge">{{ 'upcoming' }}</span></td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{{ $invoices->links() }}



@endsection