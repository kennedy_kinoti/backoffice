@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.bulkpay') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.manage') }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
            {{ __('header.pending_confirmations') }}
    </div>
    <div class="panel-body">
        <p class="text-success">{{ __('header.pending_confirmation') }} </p>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.csv_upload') }}</div>
            <div class="panel-body">
                <input type="file" value="" name="logo" class="form-control" id="logo"/>
                <span class="help-block">
                    {{ __('header.select_csv_file') }}
                </span>

                <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }}</button>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading"> {{ __('header.fund_bulk_pay_from_bank') }}</div>
            <div class="panel-body">
                <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/savereference" method="post" accept-charset="utf-8" class="form-vertical" role="form" id="formref">                   
                    <div class="form-group">
                        <label>{{ __('header.reference') }} *</label>
                        <input type="text" autocomplete="off" required value="" name="ref" class="form-control" id="ref" placeholder="Bank Reference">
                    </div>
                    <div class="form-group">
                        <label>{{ __('header.amount') }} *</label>
                        <input type="text" autocomplete="off" required value=""  name="amount" class="form-control" id="amount" placeholder="Amount" class="form-control">
                    </div>
                    <div class="form-group">                   
                        <button class="btn btn-hg btn-block btn-info" type="submit" id='bankrefbtnr'> {{ __('header.commit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.pay_single_number') }}</div>
            <div class="panel-body">
                <h3><small><i class="fa fa-bell" aria-hidden="true"></i> {{ __('header.file_under_maintenance') }}</small></h3>
                <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/singlenumber" method="post" accept-charset="utf-8" class="form-vertical" role="form">           
                
                <div class="form-group">
                    <label>{{ __('header.telephone_number') }} *</label>
                    <input type="text" autocomplete="off" required value="254" name="phone" class="form-control" id="ref" placeholder="Phone Number 2547XXXXXXXX" size="12" maxlength="12">
                    
                </div>
                <div class="form-group">
                    <label>{{ __('header.amount') }} *</label>
                    <input type="text" autocomplete="off" required value=""  name="amount" class="form-control" id="amount" placeholder="Amount" min="10" max="70000">
                </div>
                <div class="form-group">
                    <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }}</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.fund_bulk_pay_from_income') }}</div>
            <div class="panel-body">
                <!-- Start -->
                <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/movefrominbox" method="post" accept-charset="utf-8" class="form-vertical" role="form" id="movefrominboxform">                   
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row"><!-- bulk pay from inbox -->
                                <div class="col-md-6">
                                    <h3>{{ __('header.income') }}: -500.00 <small>KES</small></h3>
                                </div>
                                <div class="col-md-6">
                                    <h3>{{ __('header.partial_debt') }}: 500.00 <small>KES</small></h3>
                                </div>
                            </div>
                            <input type="hidden" name="inbox_amount" value="-500">
                            <input type="hidden" name="debt_amount" value="500.00">
                            <input type="hidden" name="inbox_curr" value="KES">
                        </div>
                    </div>
                   <div class="form-group">
                        <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }}</button>
                    </div>
                </form>
                                      
                <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/movepartial" method="post" accept-charset="utf-8" class="form-vertical" role="form" id="movepartialform">               
                    <input type="hidden" name="inbox_amount" value="-500">
                    <input type="hidden" name="inbox_curr" value="KES">
                    <div class="form-group">
                        <label>{{ __('header.amount') }} *</label>
                        <input type="text" autocomplete="off" required  name="partial_amount" class="form-control" id="partial_amount" placeholder="Amount" class="form-control"   disabled>
                    </div>
                    <input type="hidden" name="debt_amount" value="500.00">
                    <div class="form-group">
                        <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }} </button>
                    </div>
                </form>
                <!-- End -->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.bulk_pay_inbox_amount') }}</div>
            <div class="panel-body">
                <h2><span class="label label-info"><i class="icon-money"></i> KES -1,816.00</span></h2>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">{{ __('header.fund_bulk_pay_from_income') }}</div>
            <div class="panel-body">
                <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/movefrominbox" method="post" accept-charset="utf-8" class="form-vertical" role="form" id="moveusdform">                   <div class="panel">
                    <div class="panel-body">
                       
                        <div class="row"><!-- bulk pay from inbox -->
                            <div class="col-md-6">
                            <h3>{{ __('header.income') }}: 0.00 <small>USD</small></h3>
                        </div>
                        <div class="col-md-6">
                            <h3>{{ __('header.partial_debt') }}: 0.00 <small>USD</small></h3>
                        </div>
                    </div>

                    <!-- <h3><i class="icon-money"></i>  <small>USD</small></h3> -->
                    <input type="hidden" name="inbox_amount" value="0">
                    <input type="hidden" name="debt_amount" value="0">
                    <input type="hidden" name="inbox_curr" value="USD">
                </div>
            </div>
            <div class="form-group">
                <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }}</button>
            </div>
        </form>

        <form action="https://backoffice.ipayafrica.com/index.php/bulkpay/movepartialUSD" method="post" accept-charset="utf-8" class="form-vertical" role="form" id="movepartialusdform">                                      <input type="hidden" name="inbox_amount" value="0">
            <input type="hidden" name="inbox_curr" value="USD">
            <input type="hidden" name="debt_amount" value="0">
            <div class="form-group">
                <label>{{ __('header.amount') }} *</label>
                <input type="text" autocomplete="off" required  name="partial_amount" class="form-control" id="partial_amount" placeholder="Amount" class="form-control"  disabled>
            </div>
            <div class="form-group">
                <button disabled class="btn btn-hg btn-block btn-info" type="submit">{{ __('header.cannot_move_funds') }}</button>
            </div>
        </form>

            </div>
        </div>
    </div>
</div>

@endsection