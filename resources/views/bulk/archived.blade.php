@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.bulkpay') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.archived') }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.historical_data_bulkpay') }}</div>
    <div class="panel-body">
         
        <form class="form-inline" action ="{{ route('bulk_archive_data') }}" method="GET">
            <div class="form-group">
                <label class="sr-only"></label>
                <select class="form-control" name="month">
                    <option> {{ __('header.month_to_archive') }} </option>

                    @foreach($months as $key=>$month_name)
                        <option value="{{ ($key+1) }}" > {{$month_name}} </option>
                    @endforeach
                
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only"></label>
                <select class="form-control" name="duration">
                    <option> {{ __('header.year_to_archive') }} </option>
                    <option value="2017" > 2017 </option>
                    <option value="2016" > 2016 </option>
                    <option value="2015" > 2015 </option>
                    <option value="2014" > 2014 </option>
                    <option value="2013" > 2013 </option>
                    <option value="2012" > 2012 </option>
                    <option value="2011" > 2011 </option>
                    <option value="2010" > 2010 </option>
                    
                </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ __('header.search') }}</button>
            </form>
        </div>
    </div>

    <!-------------------------------------BEGIN------------------------------->

    <!---------------------------------------END------------------------------->

    <div class="panel panel-primary">
        <div class="panel-heading">{{ __('header.result') }}</div>
        
        <div class="panel-body">
            @if(!empty($query))
                {{ $query->links() }}
                <div class="table-responsive">
                        <table class="table table-hover table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>{{ __('header.date') }}</th>
                                    <th>Cr</th>
                                    <th>Dr</th>
                                    <th>{{ __('header.commission') }}</th>
                                    <th>{{ __('header.reference') }}</th>
                                    <th>{{ __('header.telephone_number') }}</th>
                                    <th>{{ __('header.mobile_code') }}</th>
                                    <th>{{ __('header.transaction_status') }}</th>
                                    <th>{{ 'Running Balance' }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach($query as $invoice)
                                    <tr>
                                        <td>{{$invoice->dttime}}</td>
                                        <td>{{$invoice->cr}}</td>
                                        <td>{{$invoice->dr}}</td>
                                        <td>{{$invoice->ipay_comm}}</td>
                                        <td>{{$invoice->ref}}</td>
                                        <td>{{$invoice->telephone}}</td>
                                        <td>{{$invoice->mmref}}</td>
                                        <td><span class="badge">
                                            @if($invoice->status == 1)
                                            {{ __('header.successful') }}
                                            @else
                                            {{ __('header.failed') }}
                                            @endif
                                        </span></td>
                                        <td><span class="badge">{{ 'upcoming' }}</span></td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                        {{ $query->links() }}
                    </div>
                @else
            <div class="alert alert-info" role="alert">{{ __('header.data_to_show') }}</div>
            @endif
        </div>
        
    </div>

@endsection