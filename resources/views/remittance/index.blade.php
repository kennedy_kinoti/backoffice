@extends('layouts.app')

@section('title', 'Bank Remittance')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.remmittance_reports') }}</li>
  </ol>
</nav>
@endsection

@section('content')

@if (session('successMsg'))
    <div class="alert alert-success">
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger">
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.bank_remittance') }}</div>
    <div class="panel-body">
        
        <div class="table-responsive">
            <table class="table table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th>{{ __('header.from_date') }}</th>
                        <th>{{ __('header.to_date') }}</th>
                        <th>{{ __('header.total_transactions') }}</th>
                        <th>{{ __('header.ipay_commission') }}</th>
                        <th>{{ __('header.transfer_charges') }}</th>
                        <th>{{ __('header.refunds') }}</th>
                        <th>{{ __('header.amount_transfered') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($logged_trans))
                        <td colspan="7" align="center">{{ __('header.data_to_display') }}</td>
                    @elseif($logged_trans->isEmpty())
                        <td colspan="7" align="center">{{ __('header.refunds_available') }}</td>
                    @else
                        @foreach($logged_trans as $trans)
                        <tr>
                            <td>{{$trans->initiate_date}}</td>
                            <td>{{$trans->reference_id}}</td>
                            <td>{{$trans->vendor_id}}</td>   
                            <td>{{$trans->reference_type}}</td>
                            <td>
                                <a href="{{url('/refund/process/'.$trans->reference_id)}}"><button type="button" class="btn btn-success">{{ __('header.refund') }}</button></a>
                            </td>
                            <td>
                                <a href="{{url('/refund/decline/'.$trans->reference_id)}}"><button type="button" class="btn btn-danger">{{ __('header.reject') }}</button></a>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @if(!empty($logged_trans))
            {{ $logged_trans->links() }}
        @endif
    </div>
</div>

@endsection