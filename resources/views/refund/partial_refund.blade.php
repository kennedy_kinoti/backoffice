@extends('layouts.app')

@section('title', 'Completed Refunds')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.refunds') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.partial_refund') }}</li>
  </ol>
</nav>
@endsection

@section('content')

@if (session('successMsg'))
    <div class="alert alert-success">
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger">
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.partial_refund') }}</div>
    <div class="panel-body">

        @foreach($logs_data as $trans)
        <form method ="POST" action="{{ url('refund/partial_submit/'.$trans->id)}}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="form-group ">
                    <label>TXN {{ __('header.code') }} *</label>
                    <input type="text" autocomplete="off" required="" name="txncode" class="form-control" placeholder="TXN Code" value="{{$trans->txncode}}" disabled>
                </div>

                <div class="form-group ">
                    <label>{{ __('header.channel') }} *</label>
                    <input type="text" autocomplete="off" required="" name="txnamt" class="form-control" placeholder="John" value="{{$trans->sendernumber}}" disabled>
                </div>
                
                <div class="form-group ">
                    <label>TXN {{ __('header.amount') }} *</label>
                    <input type="text" autocomplete="off" required="" name="txnamt" class="form-control" placeholder="John" value="{{$trans->txnamt}}" disabled>
                </div>

                <div class="form-group ">
                    <label>{{ __('header.partial_reversal_amount') }} *</label>
                    <input type="number" autocomplete="off" required="" name="revamt" class="form-control" placeholder="Reversal Amount Less Than {{$trans->txnamt}}" value="" max="{{$trans->txnamt}}">
                </div>
                <div class="form-group">
                    <button class="btn btn-hg btn-block btn-info" type="submit"> {{ __('header.completed') }}</button>
                </div>
            </form>
        @endforeach
        
    </div>
</div>

@endsection