@extends('layouts.app')

@section('title', 'Completed Refunds')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.refunds') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.completed') }}</li>
  </ol>
</nav>
@endsection

@section('content')

@if (session('successMsg'))
    <div class="alert alert-success">
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger">
        {{  session('failMsg') }}
    </div>
@endif

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.completed') }} {{ __('header.refunds') }}</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th>{{ __('header.initiate_date') }}</th>
                        <th>{{ __('header.complete_date') }}</th>
                        <th>{{ __('header.code') }}</th>
                        <th>{{ __('header.vendor_id') }}</th>
                        <th>{{ __('header.sender_number') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($refunded))
                        <td colspan="5" align="center">{{ __('header.data_to_display') }}</td>
                    @elseif($refunded->isEmpty())
                        <td colspan="5" align="center">{{ __('header.completed_refunds_available') }}</td>
                    @else
                        @foreach($refunded as $trans)
                        <tr>
                            <td>{{$trans->updatedindb}}</td>
                            <td>{{$trans->receivingdatetime}}</td>
                            <td>{{$trans->txncode}}</td>
                            <td>{{$trans->vendorid}}</td>   
                            <td>{{$trans->sendernumber}}</td>
                            <td>
                                <button name="refund" data-payid="{{$trans->id}}" data-toggle="modal" data-target="#detailsModal{{$trans->id}}" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-eye"></i></button>

                                <div class="modal fade" id="detailsModal{{$trans->id}}" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <!-- <form action="{{url('refund/initiate')}}" method="POST"> -->
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                    <h5 class="modal-title" id="detailsModalLabel">{{ __('header.transaction_details')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" data-modal="" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @if(empty($trans->id))
                                                    <tr><td>{{ __('header.response') }}</td></tr>
                                                @else
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><b>{{ __('header.transaction_code')}}</b>:</td>
                                                        <td>{{$trans->txncode}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.sender_number')}} </b>:</td>
                                                        <td>{{$trans->sendernumber}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.reversal_time') }} </b>:</td>
                                                        <td>{{$trans->updatedindb}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.initiated_time')}} </b>:</td>
                                                        <td>{{$trans->receivingdatetime}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.amount') }} </b>:</td>
                                                        <td>{{$trans->txnamt}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{{ __('header.vendor_id')}} </b>:</td>
                                                        <td>{{$trans->vendorid}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td hidden ><b>{{ __('header.card_mask')}} </b>:</td>
                                                        <td>3213 XXXX XXXX XXXX</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">{{ __('header.close') }}</button>
                                                <a href="{{url('/payments/export')}}"><button type="submit" class="btn btn-danger">{{ __('header.print') }}</button></a>
                                            </div>
                                            @endif
                                            <!-- </form> -->
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @if(!empty($refunded))
            {{ $refunded->links() }}
        @endif
    </div>
</div>

@endsection