@extends('layouts.app')

@section('title', 'Payments')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.refunds') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.initiate') }}</li>
  </ol>
</nav>
@endsection

@section('content')

@if(!empty($successMsg))
  <div class="alert alert-success"> {{ $successMsg }}</div>
@endif

<div class="panel panel-primary">
  <div class="panel-heading">{{ __('header.initiate_refund') }}</div>
  <div class="panel-body">
        <form method="post" action="{{url('refund/create')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="form-group col-md-12">
                <label for="initiate_id">{{ __('header.intiate_id') }}:</label>
                <input type="text" class="form-control" name="vendor_id" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                <label for="intiate_date">{{ __('header.intiate_date') }}:</label>
                <input type="text" class="form-control" name="intiate_date" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="comp_id">{{ __('header.completor_id') }}:</label>
                    <input type="text" class="form-control" name="comp_id" required>
                </div>
            </div>  
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="email">{{ __('header.email') }}:</label>
                    <input type="email" class="form-control" name="email" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                <label for="phone">{{ __('header.phone') }}</label>
                <input type="text" class="form-control" name="phone">    
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-8">
                <button type="submit" class="btn btn-success">{{ _('header.create') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection