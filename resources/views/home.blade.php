@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">{{__('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.status') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
<div class="panel-heading">{{ __('header.status_page') }}</div>
<div class="panel-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('header.iPay_africa_dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('header.logged_in') }}!
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
