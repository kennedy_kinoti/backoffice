@extends('layouts.app')

@section('title', 'Payments')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">B2B</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">Business to Business (B2B)</div>
    <div class="panel-body">

        <form>
            <div class="form-group ">
                <label>{{ __('header.account_type') }}</label>
                <select class="form-control" name="accounttype">
                    <option value="">{{ __('header.pick_type') }}</option>
                    <option value="mpesapaybill">Mpesa Paybill</option>
                    <option value="mpesatill">Mpesa Till/ Buy Goods</option>
                </select>
            </div>
            <div class="form-group ">
                <label>{{ __('header.sender_to') }} *</label>
                <input type="text" autocomplete="off" required="" name="sendernumber" class="form-control" id="ref" placeholder="eg 510800" size="12" maxlength="12" value="">
            </div>
            <div class="form-group ">
                <label>{{ __('header.narration') }}</label>
                <input type="text" autocomplete="off" required="" name="account" class="form-control" id="ref" placeholder="eg Paybill account number" disabled="" value="">
            </div>
            <div class="form-group ">
                <label>{{ __('header.amount') }} *</label>
                <input type="text" autocomplete="off" required="" name="amount" class="form-control" placeholder="Amount" value="">
            </div>
            <div class="form-group">
                <button disabled="" class="btn btn-hg btn-block btn-info" type="submit"> {{ __('header.send') }}</button>
            </div>
        </form>
        
    </div>
</div>
@endsection