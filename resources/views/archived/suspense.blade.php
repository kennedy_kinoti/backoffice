@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.archived') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.suspense_online') }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.historical_suspense') }}</div>
    <div class="panel-body">
         
        <form class="form-inline">
            <div class="form-group">
                <label class="sr-only"></label>
                <select class="form-control" name="month">
                    <option> {{ __('header.month_to_archive') }} </option>
                    <option value="1" > Jan. </option>
                        <option value="2" > Feb. </option>
                        <option value="3" > Mar. </option>
                        <option value="4" > Apr. </option>
                        <option value="5" > May </option>
                        <option value="6" > Jun. </option>
                        <option value="7" > Jul. </option>
                        <option value="8" > Aug. </option>
                        <option value="9" > Sep. </option>
                        <option value="10" > Oct. </option>
                        <option value="11" > Nov. </option>
                        <option value="12" > Dec. </option>
                
                </select>
            </div>
            <div class="form-group">
                <label class="sr-only"></label>
                <select class="form-control" name="duration">
                    <option> {{ __('header.year_to_archive') }} </option>
                    <option value="2017" > 2017 </option>
                    <option value="2016" > 2016 </option>
                    <option value="2015" > 2015 </option>
                    <option value="2014" > 2014 </option>
                    <option value="2013" > 2013 </option>
                    <option value="2012" > 2012 </option>
                    <option value="2011" > 2011 </option>
                    <option value="2010" > 2010 </option>
                    
                </select>
            </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> {{ __('header.search') }}</button>
            </form>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">{{ __('header.result') }}</div>
        <div class="panel-body">
            <div class="alert alert-info" role="alert">{{ __('header.data_to_show') }}</div>
        </div>
    </div>


@endsection