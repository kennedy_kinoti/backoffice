@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{__('header.suspense') }}</div>
    <div class="panel-body">
        
        <form class="form-inline">
            <div class="form-group">
                <label class="sr-only" for="from_date"></label>
                <input type="text" class="form-control datepicker" id="from_date" placeholder="{{__('header.from_date') }}" name="from_date">
            </div>
            <div class="form-group">
                <label class="sr-only" for="to_date"></label>
                <input type="text" class="form-control datepicker" id="to_date" placeholder="{{__('header.to_date') }}" value="<?php echo date('Y-M-d')?>" name="to_date">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputSearch">{{__('header.search') }}</label>
                <input type="text" name="search" class="form-control" id="exampleInputSearch" placeholder="{{__('header.search') }}">
            </div>

            <button type="submit" class="btn btn-default">{{__('header.filter') }}</button>
        </form>

        <table class="table table-striped table-condensed" id="list">
            <thead>
            <tr>
                <th>{{__('header.date') }}</th>
                <th>{{__('header.sender') }}</th>
                <th>{{__('header.vendor_id') }}</th>
                <th>{{__('header.code') }}</th>
                <th>{{__('header.telephone_number') }}</th>
                <th>{{__('header.channel') }}</th>
                <th>{{__('header.amount') }}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>2018-06-10 17:20:04</td>
                    <td>Maston Miriangamu</td>
                    <td>0212812371</td>
                    <td>66838396</td>
                    <td>254753743615</td>
                    <td>AIRTELMONEY</td>
                    <td>KES 100.00</td>
                    <td>
                        <form action="https://backoffice.ipayafrica.com/index.php/online/refund_pos" method="post" accept-charset="utf-8">       <input type="hidden" value="66838396" name="refund_details">            
                        <input type="hidden" value="KES" name="refund_curr">
                        <input type="hidden" value="100.00" name="refund_amt">
                        <button name="refund" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-success pending"><i class="fa fa-reply"></i> {{__('header.refund') }}</button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection