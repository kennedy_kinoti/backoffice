@extends('layouts.app')
@section('title', 'Page Title')

@section('menu', 'active')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.remmittance_account') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.account_details') }} </div>
    <div class="panel-body">
        <div class="table-responsive">
            @if(!empty($data))
            <table class="table table-hover">
                <tr class="info">
                    <td></td>
                    <td></td>
                    <td><b>{{ __('header.bank_details') }} </b></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>{{ __('header.bank_name') }} :</b></td>
                    <td>{{ $data[0]['mer_bankname']}}</td>
                    <td><b>{{ __('header.bank_branch') }} :</b></td>
                    <td>{{ ucfirst($data[0]['mer_bankbranch']) }}</td>
                </tr>
                <tr>
                    <td><b>{{ __('header.ac_number') }} :</b></td>
                    <td>{{ $data[0]['mer_bankaccnum'] }}</td>
                    <td><b>{{ __('header.ac_name') }} :</b></td>
                    <td>{{ ucwords($data[0]['mer_bankaccname']) }}</td>
                </tr>

                <tr class="info">
                    <td></td>
                    <td></td>
                    <td><b>{{ __('header.payment_options') }}</b></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="2">{{ __('header.pay_at') }}</td>
                    <td><input type="text" value="{{ $data[0]['release_level'] }}" readonly=""></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">{{ __('header.pay_me_using') }}</td>
                    <td>{{ $data[0]['mer_pay_mode'] }}</td>
                    <td></td>
                </tr>
                
            </table>
            @else
                <strong style="font-size:1.2em">Nothing to show</strong>
            @endif
        </div>
    </div>
</div>


<form action="{{ route('settle') }}" method="POST" accept-charset="utf-8" class="form-vertical" role="form">
    @csrf
    <div class="panel panel-primary">
        <div class="panel-heading">{{ __('header.frequency_details') }}</div>
        <div class="panel-body">
            <div class="table-responsive">
                <div class="col-md-12">
                    <b>{{ __('header.settle_me') }} {{ __('header.every') }}</b>
                    <ul class="list-inline">
                            
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox"  @if(!empty($data[0]['mon']) and $data[0]['mon'] != 0): checked @endif  name="monday" value="1" id = "mon" onchange="weekdays()">{{ __('header.monday') }}</label></li>
                        </li><label class="checkbox-inline"><input class="daily_check" type="checkbox" @if(!empty($data[0]['tue']) and $data[0]['tue'] != 0): checked @endif name="tuesday" value="1" id = "tue" onchange="weekdays()">{{ __('header.tuesday') }}</label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" @if(!empty($data[0]['wed']) and $data[0]['wed'] != 0): checked @endif name="wednesday" value="1" id = "wed" onchange="weekdays()">{{ __('header.wednesday') }}</label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" @if(!empty($data[0]['thur']) and $data[0]['thur'] != 0): checked @endif name="thursday" value="1" id = "thur" onchange="weekdays()">{{ __('header.thursday') }}</label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" @if(!empty($data[0]['fri']) and $data[0]['fri'] != 0): checked @endif name="friday" value="1" id = "fri" onchange="weekdays()">{{ __('header.friday') }}</label></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">                
                <div class="col-md-12">
                <b>{{ __('header.settle_me') }}</b>
                <ul class="list-inline">
                    <li><label class="checkbox-inline"><input class="settle_duration" id="everyday" type="checkbox" @if(!empty($data[0]['mon']) and $data[0]['mon'] != 0 and !empty($data[0]['tue']) and $data[0]['tue'] != 0 and !empty($data[0]['wed']) and $data[0]['wed'] != 0 and !empty($data[0]['thur']) and $data[0]['thur'] != 0) and !empty($data[0]['fri']) and $data[0]['fri'] != 0): checked @endif name="everyday" value="1" onclick="days()">{{ __('header.everyday') }}</label></li>  
                    <li><label class="checkbox-inline"><input class="settle_duration" id="fortnight" type="checkbox" @if(!empty($data[0]['midmnth']) and $data[0]['midmnth'] != 0): checked @endif  name="midmonth" value="1" onchange = "settle()">{{ __('header.fortnight') }}</label></li>  
                    <li><label class="checkbox-inline"><input class="settle_duration" id="monthly" type="checkbox" @if(!empty($data[0]['startmnth']) and $data[0]['startmnth'] != 0): checked @endif name="startmonth" value="1" onchange = "settles()">{{ __('header.monthly') }}</label></li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">                
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-sm">{{ __('header.save') }}</button>
            </div>
        </div>
        <div class="table-responsive">                
            <div class="col-md-12">
                {{ __('header.terms_and_conditions') }}
                <br/>
                <small>{{ __('header.t_c') }}</small>
            </div>
        
    </div>
</form>

@endsection
<script>
    function days()
    {
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');
        var mon = document.getElementById('mon');
        var tue = document.getElementById('tue');
        var wed = document.getElementById('wed');
        var thur= document.getElementById('thur');
        var fri = document.getElementById('fri');

    var all = [mon, tue, wed, thur, fri];

        if(everyday.checked == true){

            for(x = 0; x < all.length; x++)
            {
                all[x].checked = true;
            } 
            fortnight.checked = false;
            month.checked = false;
            }   else {
                for(x = 0; x < all.length; x++)
                    {
                        all[x].checked = false;
                    } 
    }
}
    function weekdays(){
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');
        var mon = document.getElementById('mon');
        var tue = document.getElementById('tue');
        var wed = document.getElementById('wed');
        var thur= document.getElementById('thur');
        var fri = document.getElementById('fri');

        var wkdays = [mon, tue, wed, thur, fri];
        var i;

        for (i = 0; i < wkdays.length; i++){
            if(wkdays[i].checked == false){
                everyday.checked = false;
            }
            else {
                fortnight.checked = false;
                month.checked = false;
            }
        }
    }
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');

        function settle(){
            document.getElementById('monthly').checked = false;
            document.getElementById('everyday').checked = false;
            document.getElementById('mon').checked = false;
            document.getElementById('tue').checked = false;
            document.getElementById('wed').checked = false;
            document.getElementById('thur').checked = false;
            document.getElementById('fri').checked = false;
        }
        
        function settles(){
            document.getElementById('everyday').checked = false;
            document.getElementById('fortnight').checked = false;
            document.getElementById('mon').checked = false;
            document.getElementById('tue').checked = false;
            document.getElementById('wed').checked = false;
            document.getElementById('thur').checked = false;
            document.getElementById('fri').checked = false;
        }
</script>