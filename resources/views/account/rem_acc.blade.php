@extends('layouts.app')

@section('title', 'Payments')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ 'Remittance Account' }}</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">{{ 'Remittance Account' }}</div>
    <div class="panel-body">

        <div class="row">
               
            <form method ="GET" action="{{ url('payments/search')}}">
               @csrf
                <div class="form-group col-md-4">
                    <!-- <label>Transaction code</label> -->
                    <input type="text" name="code" class="form-control" id="code" placeholder="{{ __('header.transaction_code') }}" autocomplete="off">
                </div>
                <div class="form-group col-md-4">
                <!-- <label>From</label> -->
                    <input type="text"  class="form-control" name="datefilter" value="" placeholder="{{ __('header.date_range') }}" autocomplete="off"/>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success">{{ __('header.search') }}</button>
                </div>

                @if (!empty($data))
                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"
                        
                    > {{ __('header.export') }}</a>
                </div>
                @endif
            </form>
        
            <div class="form-group col-md-1">
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">
                    <span class="glyphicon glyphicon-collapse-down"></span> {{ __('header.advanced') }}
                </button>
            </div>
            <div id="demo" class="collapse col-md-12">
                <div class="form-group col-md-3">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled>{{ __('header.channel') }}</option>
                        <option>MPESA</option>
                        <option>Airtel Money</option>
                        <option>Equitel</option>
                        <option>eLipa</option>
                        <option>Kenswitch</option>
                        <option>Visa</option>
                        <option>Mastercard</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="oid" placeholder="Order ID" name="oid"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="fname" placeholder="Client First Name" name="fname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="lname" placeholder="Client Last Name" name="lname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"/>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th>{{ __('header.code') }}</th>
                        <th>{{ __('header.channel') }}</th>
                        <th>{{ __('header.date') }}</th>
                        <th>{{ __('header.amount') }}</th>
                        <th>{{ __('header.name') }}</th>
                        <th>{{ __('header.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5" align="center">
                            {{ __('header.search_first') }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection