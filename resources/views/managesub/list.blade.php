@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage_sub') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.list_edit_sub_account') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.pos_sub_account') }}</div>
    <div class="panel-body">
        <table class="table table-striped table-condensed" id="list">
            <thead>
                <tr>
                    <th>{{ __('header.telephone_number') }}</th>
                    <th>{{ __('header.email') }}</th>
                    <th>{{ __('header.sub_account') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>254722123456</td>
                    <td>john@example.com</td>
                    <td>android</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection