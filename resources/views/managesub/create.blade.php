@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.manage_sub') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.add_sub_account') }}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="panel panel-primary">
  <div class="panel-heading">{{ __('header.pos_sub_account') }}</div>
  <div class="panel-body">
    <form method="post" action="{{url('register')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
      <div class="row">
        <div class="form-group col-md-12">
          <label for="vendor_id">{{ __('header.sub_account_vendor_id') }}:</label>
          <input type="text" class="form-control" name="vendor_id" required>
        </div>
      </div>
      <div class="row">
          <div class="form-group col-md-12">
            <label for="acc_name">{{ __('header.sub_account_name') }}:</label>
            <input type="text" class="form-control" name="acc_name" required>
          </div>
        </div>
      <div class="row">
          <div class="form-group col-md-12">
            <label for="email">{{ __('header.email') }}:</label>
            <input type="email" class="form-control" name="email" required>
          </div>
        </div>
      <div class="row">
        <div class="form-group col-md-12">
          <label for="phone">{{ __('header.phone') }}</label>
          <input type="text" class="form-control" name="phone">    
        </div>
      </div>
      <div class="row">
        <div class="form-group col-md-8">
          <button type="submit" class="btn btn-success">{{ __('header.create') }}</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
