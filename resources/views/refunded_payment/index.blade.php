@extends('layouts.app')

@section('title', 'Page Title')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.refunds') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.refunded') }}</li>
  </ol>
</nav>
@endsection


@section('content')
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.refund_list') }}</div>
    <div class="panel-body">

        <div class="row">
            
            <form method ="POST" action="{{ url('payments/search')}}">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="form-group col-md-4">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled>{{ __('header.status') }}</option>
                        <option>{{ __('header.success') }}</option>
                        <option>{{ __('header.rejected') }}</option>
                    </select>
                </div>
               
                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success">{{ __('header.search') }}</button>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"> {{ __('header.export') }}</a>
                </div>
            </form>
        </div>
        
        <div class="table-responsive">
            <table class="table table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th>{{ __('header.date') }}</th>
                        <th>{{ __('header.code') }}</th>
                        <th>{{ __('header.vendor_id') }}</th>
                        <th>{{ __('header.reference_type') }}</th>
                        <th>{{ __('header.telephone_number') }}</th>
                        <th>{{ __('header.channel') }}</th>
                        <th>{{ __('header.status') }}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($logged_trans))
                        <td colspan="5" align="center">{{ __('header.data_to_display') }}</td>
                    @elseif($logged_trans->isEmpty())
                        <td colspan="5" align="center">{{ __('header.refunds_available') }}</td>
                    @else
                        @foreach($logged_trans as $trans)
                            <tr>
                                <td>{{$trans->initiate_date}}</td>
                                <td>{{$trans->reference_id}}</td>
                                <td>{{$trans->vendor_id}}</td>   
                                <td>{{$trans->reference_type}}</td>
                                <td>{{$trans->vendor_id}}</td>   
                                <td>{{$trans->reference_type}}</td>
                                <td>
                                    @if($trans->state == 1)
                                        <span class="badge badge-pill badge-success">{{ __('header.success') }}</span>
                                    @elseif($trans->state == 2)
                                        <span class="badge badge-pill badge-danger">{{ __('header.rejected') }}</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        @if(!empty($logged_trans))
            {{ $logged_trans->links() }}
        @endif
    </div>
</div>
@endsection