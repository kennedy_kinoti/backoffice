@extends('layouts.app')
@section('title', 'Manage Users')

@section('breadcrumb')
<nav aria-label="breadcrumb">
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
      <li class="breadcrumb-item"><a href="#">{{ __('header.invoice') }}</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ __('header.create') }}</li>
   </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
      <div class="panel-heading">
         <h3 class="panel-title"><i class="icon-file-text-alt"></i> {{ __('header.create_invoice') }}
         </h3>
      </div>
      <div class="panel-body">
         <!-- The Form -->
         <form method="post" class="form-horizontal" action="{{url('invoice/initiate')}}" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <!-- Logo -->
            <div class="form-group">
               <label for="f_name" class="col-lg-3 control-label">{{ __('header.client_logo') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="file" value="" name="logo" class="form-control" id="logo" placeholder="Insert client's logo">                        
               </div>
            </div>
            <!-- Input First Name -->
            <div class="form-group">
               <label for="f_name" class="col-lg-3 control-label">{{ __('header.client_name') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" value="" name="name" class="form-control" id="f_name" placeholder="Insert client's name" required>                        
               </div>
            </div>
            <!-- Email Field -->
            <div class="form-group">
               <label for="email" class="col-lg-3 control-label">{{ __('header.email') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" value="" name="email" class="form-control" id="email" placeholder="Insert client's email" required>                        
               </div>
            </div>
            <!-- Phone Number Input -->
            <div class="form-group">
               <label for="tel" class="col-lg-3 control-label">{{ __('header.telephone_number') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" value="" name="tel_number" class="form-control" id="tel" placeholder="Insert client's telephone number" required>                        
               </div>
            </div>
            <!-- Itinerary Textarea -->
            <div class="form-group">
               <label class="col-lg-3 control-label">{{ __('header.payment_details') }}</label>                        
               <div class="col-lg-6">                        
                  <textarea value="" name="itinerary" class="form-control" rows="6" required></textarea>                        
               </div>
            </div>
            <?php 
                function random_str($length, $keyspace = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'){
                    $pieces = [];
                    $max = mb_strlen($keyspace, '8bit') - 1;
                    for ($i = 0; $i < $length; ++$i) {
                        $pieces []= $keyspace[random_int(0, $max)];
                    }
                    return implode('', $pieces);
                }

                $a = random_str(5, '0123456789');
                $b = random_str(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

            ?>
            <!-- Invoice Number -->
            <div class="form-group">
                <label for="inv_no" class="col-lg-3 control-label">{{ __('header.invoice_number') }}</label>                        
                <div class="col-lg-6"> 
                    <input type="text" name="inv_no" class="form-control" id="inv_no" placeholder="Generate a unique ID" value="{{strtoupper($a.'-'.$b)}}">                        
                </div>

            </div>
            <!-- Order Number -->
            <div class="form-group">
               <label for="inv_no" class="col-lg-3 control-label">{{ __('header.order_id') }}</label>                        
               <div class="col-lg-6">
                  <input type="text" name="order_id" class="form-control" id="order_id" placeholder="Generate a unique Order">                        
               </div>
            </div>
            <!-- Select currency checkbox -->
            <div class="form-group">
               <label class="col-lg-3 control-label">{{ __('header.select_currency') }}</label>                        
               <div class="col-lg-6">
                  <select class="form-control" name="curr" required>
                     <option value="">{{ __('header.pick_currency') }}</option>
                     <option value="KES"  selected="selected">KES</option>
                     <option value="USD"  selected="selected">USD</option>
                  </select>
               </div>
            </div>
            <!-- Amount -->
            <div class="form-group">
               <label for="ttl_amount" class="col-lg-3 control-label">{{ __('header.total_amount') }}</label>                        
               <div class="col-lg-6">                        
                  <input type="text" value="" class="form-control" id="ttl_amount" name="amount" placeholder="Amount" required autocomplete="off">                        
               </div>
            </div>
            <!-- Submit and Reset Buttons -->
            <div class="form-group">
               <label class="col-lg-3 control-label"></label>                    
               <div class="col-lg-6">                    
                  <button name="invoice" value="invoice" type="submit" class="btn btn-primary">{{ __('header.create_invoice') }}</button>
                  <button name="proformainvoice" value="proformainvoice" type="submit" class="btn btn-info">{{ __('header.pro_forma_invoice') }}</button>                    
                  <button type="reset" class="btn btn-default">{{ __('header.reset') }}</button>                    
               </div>
            </div>
         </form>
      </div>
      <!-- end of panel body -->                    
   </div>
   <!-- end of panel -->
</div>
@endsection