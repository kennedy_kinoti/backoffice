@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.invoice') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.view_pending') }}</li>
  </ol>
</nav>
@endsection

@section('content')
@if (session('successMsg'))
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('successMsg') }}
    </div>
@elseif (session('failMsg'))
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{  session('failMsg') }}
    </div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.pending_invoices') }}</div>
    <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed">
                <thead>
                    <tr>
                        <th>{{ __('header.invoice_number') }}</th>
                        <th>{{ __('header.name') }}</th>
                        <th>{{ __('header.initiator') }}</th>
                        <th>{{ __('header.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($pending_invoices))
                        <tr>
                            <td colspan="4" align="center">
                                {{ __('header.data_to_display') }}
                            </td>
                        </tr>
                    @else
                    @foreach($pending_invoices as $p_invoice)
                        <tr>
                            <td>{{$p_invoice->invoice_number}}</td>
                            <td>{{$p_invoice->names}}</td>
                            <td>{{'iPay'}}</td>
                            
                            <td>
                                <button name="refund" data-payid="{{$p_invoice->id}}" data-toggle="modal" data-target="#invoiceModal{{$p_invoice->id}}" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-eye"></i> {{ __('header.view') }}</button>

                                 <div class="modal fade" id="invoiceModal{{$p_invoice->id}}" tabindex="-1" role="dialog" aria-labelledby="invoiceModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    
                                                    <form action="{{url('invoice/confirm/'.$p_invoice->id)}}" method="POST">
                                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                        <button type="button" class="close" data-dismiss="modal" data-modal="" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h3 class="modal-title" id="detailsModalLabel">{{ __('header.invoice') }} <b>{{$p_invoice->invoice_number}}</b></h3>
                                                        
                                                </div>

                                                <div class="modal-body">
                                                    @if(empty($p_invoice->id))
                                                        <tr><td>{{ __('header.response') }}</td></tr>
                                                    @else
                                                    <table class="table table-bordered">
                                                        <tr >
                                                            <td><b>{{ __('header.logo') }} </b>:</td>
                                                            <td>
                                                                <img src='{{ asset('invoice_logos/'.$p_invoice->logo) }}'>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.name') }} </b>:</td>
                                                            <td>{{$p_invoice->names}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.email') }} </b>:</td>
                                                            <td>{{$p_invoice->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.telephone_number') }} </b>:</td>
                                                            <td>{{$p_invoice->phonenumber}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.payment_details') }} </b>:</td>
                                                            <td>{{$p_invoice->itinerary}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.amount') }} </b>:</td>
                                                            <td>{{$p_invoice->amount}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.status') }} </b>:</td>
                                                            <td> <span class="label label-danger">{{ __('header.awaiting_confirmation') }}</span></td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">{{ __('header.close') }}</button>
                                                    
                                                    <a href=""><button type="submit" name="submit" value="confirm" class="btn btn-success">{{ __('header.confirm') }}</button></a>

                                                    <a href=""><button type="submit" name="submit" value="reject" class="btn btn-danger">{{ __('header.reject') }}</button></a>
                                                </div>

                                                @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>    
        </div>
        @if(!empty($pending_invoices))
            {{ $pending_invoices->links() }}
        @endif
    </div>
</div>
@endsection