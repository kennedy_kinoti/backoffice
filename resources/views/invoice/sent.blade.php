@extends('layouts.app')

@section('title', 'View Invoices')

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">{{ __('header.dashboard') }}</a></li>
    <li class="breadcrumb-item"><a href="#">{{ __('header.invoice') }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ __('header.view_sent') }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">{{ __('header.sent_invoices') }}</div>
    <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed">
                <thead>
                    <tr>
                        <th>{{ __('header.date') }}</th>
                        <th>{{ __('header.invoice_number') }}</th>
                        <th>{{ __('header.status') }}</th>
                        <th>{{ __('header.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(empty($invoices))
                        <tr>
                            <td colspan="4" align="center">
                                {{ __('header.search_first') }}
                            </td>
                        </tr>
                    @else
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->date}}</td>
                            <td>{{$invoice->invoice_number}}</td>
                            <td>
                                @if($invoice->state == 0)
                                    <span class="label label-danger">{{ __('header.test') }}</span>
                                @elseif($invoice->state == 1)
                                    <span class="label label-warning">{{ __('header.pending') }}</span>
                                @elseif($invoice->state == 2)
                                    <span class="label label-success">{{ __('header.paid') }}</span>
                                @endif
                            </td>
                            <td>
                                <button name="refund" data-payid="{{$invoice->id}}" data-toggle="modal" data-target="#invoiceModal{{$invoice->id}}" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-eye"></i> {{ __('header.view') }}</button>

                                 <div class="modal fade" id="invoiceModal{{$invoice->id}}" tabindex="-1" role="dialog" aria-labelledby="invoiceModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <form action="{{url('invoice/resend_invoice/'.$invoice->id)}}" method="POST">
                                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                                        <button type="button" class="close" data-dismiss="modal" data-modal="" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h3 class="modal-title" id="detailsModalLabel">{{ __('header.invoice') }} <b>{{$invoice->invoice_number}}</b></h3>
                                                        
                                                </div>

                                                <div class="modal-body">
                                                    @if(empty($invoice->id))
                                                        <tr><td>{{ __('header.response') }}</td></tr>
                                                    @else
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <td><b>{{ __('header.name') }} </b>:</td>
                                                            <td>{{$invoice->names}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.email') }} </b>:</td>
                                                            <td>{{$invoice->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.telephone_number') }} </b>:</td>
                                                            <td>{{$invoice->phonenumber}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.payment_details') }} </b>:</td>
                                                            <td>{{$invoice->itinerary}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.amount') }} </b>:</td>
                                                            <td>{{$invoice->amount}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.amount') }} </b>:</td>
                                                            <td>{{$invoice->amount}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>{{ __('header.status') }} </b>:</td>
                                                            <td>
                                                                <span class="label label-success">{{ __('header.sent') }}</span>
                                                                <span class="label label-warning">{{ __('header.pending_payment') }}</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal">{{ __('header.close') }}</button>
                                                    
                                                    <a href=""><button type="submit" name="submit" class="btn btn-success"><i class="fa fa-retweet"> {{ __('header.resend') }}</i></button></a>
                                                </div>

                                                @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>    
        </div>
        @if(!empty($invoices))
            {{ $invoices->links() }}
        @endif
    </div>
</div>

@endsection