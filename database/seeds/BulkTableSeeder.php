<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BulkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory('App\Bulk', 50)->create();
    }
}
