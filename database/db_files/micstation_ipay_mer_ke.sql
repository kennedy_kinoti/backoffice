-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2018 at 02:41 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `micstation_ipay_mer_ke`
--

-- --------------------------------------------------------

--
-- Table structure for table `0000_bulk_inbox`
--

CREATE TABLE `0000_bulk_inbox` (
  `id` int(11) NOT NULL,
  `ref` varchar(70) NOT NULL,
  `cr` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dr` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dttime` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `mmref` varchar(15) NOT NULL,
  `ipay_comm` int(5) NOT NULL,
  `telephone` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `0000_cart_data`
--

CREATE TABLE `0000_cart_data` (
  `id` bigint(20) NOT NULL,
  `sender` varchar(15) NOT NULL,
  `code` varchar(25) NOT NULL,
  `cartcode` varchar(40) NOT NULL,
  `currentstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `afteruploadstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amt` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amt_diff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fverified` enum('passed','not passed') NOT NULL DEFAULT 'not passed',
  `used` enum('yes','no','more','less') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `order_id` varchar(30) NOT NULL,
  `idscomm_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `email` varchar(120) NOT NULL,
  `refund_status` enum('not refunded','refund pending','refund effected') NOT NULL DEFAULT 'not refunded',
  `refund_req_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p1` text NOT NULL,
  `p2` text NOT NULL,
  `p3` varchar(50) NOT NULL,
  `p4` varchar(50) NOT NULL,
  `qwh` int(11) NOT NULL DEFAULT '0',
  `afd` int(11) NOT NULL DEFAULT '0',
  `poi` int(11) NOT NULL DEFAULT '0',
  `uyt` int(11) NOT NULL DEFAULT '0',
  `ifd` int(11) NOT NULL DEFAULT '0',
  `ipn_status` tinyint(1) NOT NULL DEFAULT '0',
  `curr` varchar(3) NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Incoming Raw Data';

-- --------------------------------------------------------

--
-- Table structure for table `0000_cart_data_refunded`
--

CREATE TABLE `0000_cart_data_refunded` (
  `id` bigint(20) NOT NULL,
  `sender` varchar(15) NOT NULL,
  `code` varchar(25) NOT NULL,
  `cartcode` varchar(40) NOT NULL,
  `currentstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `afteruploadstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amt` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amt_diff` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fverified` enum('passed','not passed') NOT NULL DEFAULT 'not passed',
  `used` enum('yes','no','more','less') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `order_id` varchar(30) NOT NULL,
  `idscomm_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `email` varchar(120) NOT NULL,
  `refund_status` enum('not refunded','refund pending','refund effected') NOT NULL DEFAULT 'refund effected',
  `refund_req_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `p1` varchar(50) NOT NULL,
  `p2` varchar(50) NOT NULL,
  `p3` varchar(50) NOT NULL,
  `p4` varchar(50) NOT NULL,
  `qwh` int(11) NOT NULL DEFAULT '0',
  `afd` int(11) NOT NULL DEFAULT '0',
  `poi` int(11) NOT NULL DEFAULT '0',
  `uyt` int(11) NOT NULL DEFAULT '0',
  `ifd` int(11) NOT NULL DEFAULT '0',
  `ipn_status` tinyint(1) NOT NULL DEFAULT '0',
  `curr` varchar(3) NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Incoming Raw Data';

-- --------------------------------------------------------

--
-- Table structure for table `0000_inbox_new`
--

CREATE TABLE `0000_inbox_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `updatedindb` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `receivingdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sendernumber` varchar(20) NOT NULL DEFAULT '',
  `txncode` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `msisdn` varchar(13) NOT NULL,
  `vendorid` varchar(10) NOT NULL,
  `idscomm_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `txnamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `curr` varchar(3) NOT NULL DEFAULT 'KES',
  `runid` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `0000_inbox_new`
--

INSERT INTO `0000_inbox_new` (`id`, `updatedindb`, `receivingdatetime`, `sendernumber`, `txncode`, `fname`, `lname`, `msisdn`, `vendorid`, `idscomm_fee`, `txnamt`, `curr`, `runid`) VALUES
(57, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'TELKOM', 'AK47', 'Osama', 'Laden', '0715916968', '0000', '1.00', '45000.00', 'KES', 'no'),
(58, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', 'WWW1231', 'Andrew', 'Ouko', '0715916968', '0000', '1.00', '5600.00', 'KES', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `0000_inbox_refunded`
--

CREATE TABLE `0000_inbox_refunded` (
  `id` int(10) UNSIGNED NOT NULL,
  `updatedindb` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `receivingdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sendernumber` varchar(20) NOT NULL DEFAULT '',
  `txncode` varchar(20) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `msisdn` varchar(13) NOT NULL,
  `vendorid` varchar(10) NOT NULL,
  `idscomm_fee` decimal(10,2) NOT NULL DEFAULT '0.00',
  `txnamt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `curr` varchar(3) NOT NULL DEFAULT 'KES',
  `reconciled` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `0000_inbox_refunded`
--

INSERT INTO `0000_inbox_refunded` (`id`, `updatedindb`, `receivingdatetime`, `sendernumber`, `txncode`, `fname`, `lname`, `msisdn`, `vendorid`, `idscomm_fee`, `txnamt`, `curr`, `reconciled`) VALUES
(1, '2018-05-02 00:00:00', '2018-07-12 00:00:00', 'VISA', '12ertyusss12', 'joshua', 'kisee', '0705118708', '000', '3.00', '10000.00', 'USD', 'no'),
(2, '2018-05-02 00:00:00', '2018-05-01 00:00:00', 'MPESA', '12ertgsssa8', 'joshua', 'kisee', '0705118708', '000', '1.00', '20.00', 'KES', 'no'),
(3, '2018-05-02 00:00:00', '2018-05-01 00:00:00', 'Mpesa', 'rsdrrrhhhlg4', 'joshua', 'kisee', '0705118708', '000', '1.00', '20.00', 'KES', 'no'),
(4, '2018-07-12 00:00:00', '2018-07-12 00:00:00', 'MPESA', '12ertyuss9', 'joshua', 'kisee', '0705118708', '000', '1.00', '20.00', 'KES', 'no'),
(5, '2018-07-04 12:26:39', '2018-07-04 12:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(6, '2018-07-04 12:26:39', '2018-07-04 12:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(7, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(8, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(9, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(10, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(11, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no'),
(12, '2018-07-04 09:26:39', '2018-07-04 09:26:39', 'MPESA', '12yees12', 'Kennedy', 'Kinoti', '0715916968', '000', '0.00', '0.00', 'KES', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `0000_payment_status`
--

CREATE TABLE `0000_payment_status` (
  `id` int(11) NOT NULL,
  `txndate_frm` datetime NOT NULL,
  `txndate_to` datetime NOT NULL,
  `txn_amount` decimal(10,2) NOT NULL,
  `ipay_comm` decimal(10,2) NOT NULL,
  `ipay_refund` decimal(10,2) NOT NULL DEFAULT '0.00',
  `txnstatus` enum('Pending','Paid') NOT NULL DEFAULT 'Pending',
  `txnpaymode` enum('Pesalink','EFT','RTGS','MPESA','Airtelmoney','eLipa','BULKPAY') NOT NULL DEFAULT 'EFT',
  `txncost` int(6) NOT NULL DEFAULT '200',
  `curr` enum('KES','UGX','USD') NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `0000_payment_status`
--

INSERT INTO `0000_payment_status` (`id`, `txndate_frm`, `txndate_to`, `txn_amount`, `ipay_comm`, `ipay_refund`, `txnstatus`, `txnpaymode`, `txncost`, `curr`) VALUES
(1, '2018-01-01 00:00:00', '2018-07-09 00:00:00', '1000.00', '200.00', '0.00', 'Paid', 'EFT', 200, 'KES'),
(2, '2018-01-01 00:00:00', '2018-07-09 00:00:00', '1000.00', '200.00', '0.00', 'Paid', 'EFT', 200, 'KES'),
(3, '2018-01-01 00:00:00', '2018-07-09 00:00:00', '1000.00', '200.00', '0.00', 'Paid', 'EFT', 200, 'KES'),
(4, '2018-01-01 00:00:00', '2018-07-09 00:00:00', '1000.00', '200.00', '0.00', 'Paid', 'EFT', 200, 'KES'),
(5, '2018-01-01 00:00:00', '2018-07-09 00:00:00', '1000.00', '200.00', '0.00', 'Paid', 'EFT', 200, 'KES');

-- --------------------------------------------------------

--
-- Table structure for table `demo_bulk_inbox`
--

CREATE TABLE `demo_bulk_inbox` (
  `id` int(11) NOT NULL,
  `ref` varchar(70) NOT NULL,
  `cr` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dr` decimal(10,2) NOT NULL DEFAULT '0.00',
  `dttime` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `mmref` varchar(15) NOT NULL,
  `ipay_comm` int(5) NOT NULL,
  `telephone` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `demo_bulk_inbox`
--

INSERT INTO `demo_bulk_inbox` (`id`, `ref`, `cr`, `dr`, `dttime`, `status`, `mmref`, `ipay_comm`, `telephone`) VALUES
(1, 'REF1', '10000.00', '0.00', '2018-04-27 00:00:00', 1, '', 0, ''),
(2, 'B2BDEMO3144C152484390345628427', '0.00', '10.00', '2018-04-27 18:45:03', 1, 'MDR21H4ETW', 0, ''),
(3, 'B2BDEMO3144D1525953564187780039', '0.00', '1000.00', '2018-05-10 14:59:24', 1, '12345678', 0, ''),
(4, 'B2BDEMO3144E1528284844126737909', '0.00', '10.00', '2018-06-06 14:34:04', -2, 'MF60000000', 0, ''),
(5, 'B2BDEMO3144E1528285253222735053', '0.00', '10.00', '2018-06-06 14:40:53', -2, 'MF671H5865', 0, ''),
(6, 'B2BDEMO3144E1528376734157637403', '0.00', '10.00', '2018-06-07 16:05:33', 1, 'bar', 0, ''),
(7, 'B2BDEMO3144E152838176738675833', '0.00', '10.00', '2018-06-07 17:29:27', -2, 'MF721H58H4', 0, ''),
(8, 'B2BDEMO3144E152839407775099911', '0.00', '10.00', '2018-06-07 20:54:37', -2, 'MF771H58KL', 0, ''),
(9, 'B2BDEMO3144E152839415528171548', '0.00', '10.00', '2018-06-07 20:55:55', 1, 'REF2', 0, ''),
(10, 'B2BDEMO3144E1528397467171686260', '0.00', '10.00', '2018-06-07 21:51:07', 1, 'REF3', 0, ''),
(11, 'B2BDEMO3144E152839750123565340', '0.00', '10.00', '2018-06-07 21:51:41', 1, 'REF4', 0, ''),
(12, 'B2BDEMO3144E1528398362227207075', '0.00', '10.00', '2018-06-07 22:06:02', 1, 'REF5', 0, ''),
(13, 'B2BDEMO3144E1528398443116266247', '0.00', '10.00', '2018-06-07 22:07:23', 1, 'REF6', 0, ''),
(14, 'B2BDEMO3144E1528398572139205009', '0.00', '10.00', '2018-06-07 22:09:32', 1, 'REF7', 0, ''),
(15, 'B2BDEMO3144E152839863861227830', '0.00', '10.00', '2018-06-07 22:10:38', -2, 'MF70000000', 0, ''),
(16, 'B2BDEMO3144E1528401010162461775', '0.00', '10.00', '2018-06-07 22:50:10', -2, 'MF791H58L7', 0, ''),
(17, 'B2BDEMO3144E1528899872232968249', '0.00', '10.00', '2018-06-13 17:24:32', -2, 'MFD31H5A0F', 0, ''),
(18, 'B2BDEMO3144E1528900603153344705', '0.00', '10.00', '2018-06-13 17:36:43', 1, 'REF11', 0, ''),
(19, 'B2BDEMO3144E1528900987112752615', '0.00', '10.00', '2018-06-13 17:43:07', 1, 'REF17', 0, ''),
(20, 'B2BDEMO3144E152890143833635612', '0.00', '10.00', '2018-06-13 17:50:38', 1, 'REF18', 0, ''),
(21, 'B2BDEMO3144E1528901872152457882', '0.00', '10.00', '2018-06-13 17:57:52', 1, 'REF19', 0, ''),
(22, 'B2BDEMO3144E1528901969201952800', '0.00', '10.00', '2018-06-13 17:59:29', 1, 'REF20', 0, ''),
(23, 'B2BDEMO3144E1528902073182405591', '0.00', '10.00', '2018-06-13 18:01:13', -2, 'MFD0000000', 0, ''),
(24, 'B2BDEMO3144E1528902120165110151', '0.00', '10.00', '2018-06-13 18:02:00', -2, 'MFD41H5A1U', 0, ''),
(25, 'B2BDEMO3144E1528902437257171468', '0.00', '10.00', '2018-06-13 18:07:17', -2, 'MFD11H5A21', 0, ''),
(26, 'B2BDEMO3144E1528902493234991978', '0.00', '10.00', '2018-06-13 18:08:13', 1, 'REF21', 0, ''),
(27, 'B2BDEMO3144E1528902629150608989', '0.00', '10.00', '2018-06-13 18:10:29', 1, 'REF22', 0, ''),
(28, 'B2BDEMO3144E152890318271550310', '0.00', '10.00', '2018-06-13 18:19:42', -2, 'MFD71H5A2H', 0, ''),
(29, 'B2BDEMO3144E1528903273166845154', '0.00', '10.00', '2018-06-13 18:21:13', -2, 'MFD0000000', 0, ''),
(30, 'B2BDEMO3144E1528903318168403419', '0.00', '10.00', '2018-06-13 18:21:58', -2, 'MFD91H5A2J', 0, ''),
(31, 'B2BDEMO3144E1528903933192520629', '0.00', '10.00', '2018-06-13 18:32:13', 1, 'REF23', 0, ''),
(32, 'B2BDEMO3144E1529579002230145912', '0.00', '10.00', '2018-06-21 14:03:22', 1, 'REF24', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `0000_bulk_inbox`
--
ALTER TABLE `0000_bulk_inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0000_cart_data`
--
ALTER TABLE `0000_cart_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `0000_cart_data_refunded`
--
ALTER TABLE `0000_cart_data_refunded`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `0000_inbox_new`
--
ALTER TABLE `0000_inbox_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0000_inbox_refunded`
--
ALTER TABLE `0000_inbox_refunded`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `0000_payment_status`
--
ALTER TABLE `0000_payment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demo_bulk_inbox`
--
ALTER TABLE `demo_bulk_inbox`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `0000_bulk_inbox`
--
ALTER TABLE `0000_bulk_inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `0000_cart_data`
--
ALTER TABLE `0000_cart_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `0000_cart_data_refunded`
--
ALTER TABLE `0000_cart_data_refunded`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `0000_inbox_new`
--
ALTER TABLE `0000_inbox_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `0000_inbox_refunded`
--
ALTER TABLE `0000_inbox_refunded`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `0000_payment_status`
--
ALTER TABLE `0000_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `demo_bulk_inbox`
--
ALTER TABLE `demo_bulk_inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
