-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2018 at 02:41 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `micstation_conf_ipay_ke`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `autopay_transactions`
-- (See below for the actual view)
--
CREATE TABLE `autopay_transactions` (
`id` int(11)
,`sid` varchar(100)
,`online_amount` decimal(20,2)
,`session_amount` decimal(20,2)
,`receivingdatetime` timestamp
,`sessdatetime` timestamp
,`tid` varchar(15)
,`vid` varchar(15)
,`tcode` varchar(20)
,`ipay_comm` decimal(10,2)
,`curr` varchar(3)
,`msisdn` varchar(15)
,`oid` varchar(34)
,`p1` varchar(20)
,`p2` varchar(20)
,`p3` varchar(20)
,`p4` varchar(20)
,`callback` varchar(255)
,`channel` varchar(15)
,`phone` varchar(20)
,`email` varchar(150)
,`fname` varchar(20)
,`lname` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `autopay_transactions_new`
-- (See below for the actual view)
--
CREATE TABLE `autopay_transactions_new` (
`id` int(11)
,`sid` varchar(100)
,`online_amount` decimal(20,2)
,`session_amount` decimal(20,2)
,`receivingdatetime` timestamp
,`sessdatetime` timestamp
,`tid` varchar(15)
,`vid` varchar(15)
,`tcode` varchar(20)
,`ipay_comm` decimal(10,2)
,`curr` varchar(3)
,`msisdn` varchar(15)
,`oid` varchar(34)
,`p1` varchar(20)
,`p2` varchar(20)
,`p3` varchar(20)
,`p4` varchar(20)
,`callback` varchar(255)
,`channel` varchar(15)
,`phone` varchar(20)
,`email` varchar(150)
,`fname` varchar(20)
,`lname` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `autopay_transactions_old`
-- (See below for the actual view)
--
CREATE TABLE `autopay_transactions_old` (
`id` int(11)
,`sid` varchar(100)
,`online_amount` decimal(20,2)
,`session_amount` decimal(20,2)
,`receivingdatetime` timestamp
,`sessdatetime` timestamp
,`tid` varchar(15)
,`vid` varchar(15)
,`tcode` varchar(20)
,`ipay_comm` decimal(10,2)
,`curr` varchar(3)
,`msisdn` varchar(15)
,`oid` varchar(34)
,`p1` varchar(20)
,`p2` varchar(20)
,`p3` varchar(20)
,`p4` varchar(20)
,`callback` varchar(255)
,`channel` varchar(15)
,`phone` varchar(20)
,`email` varchar(150)
,`fname` varchar(20)
,`lname` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `autopay_transactions_test`
-- (See below for the actual view)
--
CREATE TABLE `autopay_transactions_test` (
`id` int(11)
,`sid` varchar(100)
,`online_amount` decimal(20,2)
,`session_amount` decimal(20,2)
,`receivingdatetime` timestamp
,`sessdatetime` timestamp
,`tid` varchar(15)
,`vid` varchar(15)
,`tcode` varchar(20)
,`ipay_comm` decimal(10,2)
,`curr` varchar(3)
,`msisdn` varchar(15)
,`oid` varchar(34)
,`p1` varchar(20)
,`p2` varchar(20)
,`p3` varchar(20)
,`p4` varchar(20)
,`callback` varchar(255)
,`channel` varchar(15)
,`phone` varchar(20)
,`email` varchar(150)
,`fname` varchar(20)
,`lname` varchar(20)
);

-- --------------------------------------------------------

--
-- Table structure for table `ipay_b2b_ke`
--

CREATE TABLE `ipay_b2b_ke` (
  `id` int(11) NOT NULL,
  `ref` varchar(70) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txncost` decimal(10,2) NOT NULL,
  `curr` varchar(10) NOT NULL,
  `dttime` datetime NOT NULL,
  `mmref` varchar(50) NOT NULL,
  `account` varchar(15) NOT NULL COMMENT 'the paybill or account number or till',
  `narration` varchar(25) NOT NULL COMMENT 'other account details tied to the account',
  `status` int(11) NOT NULL DEFAULT '0',
  `vendor_id` varchar(15) NOT NULL,
  `hash` varchar(65) NOT NULL,
  `request_method` enum('BOF','API','RCN') NOT NULL,
  `merchant_reference` varchar(50) NOT NULL COMMENT 'This is a unique reference sent by the merchant it should be unique',
  `api_response` text NOT NULL,
  `api_response_text` varchar(100) NOT NULL,
  `channelcost` double(10,2) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `api_response_time` datetime NOT NULL,
  `third_party_conversation_id` varchar(70) NOT NULL,
  `channel_metadata` varchar(255) NOT NULL,
  `receipient_metadata` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipay_b2b_ke`
--

INSERT INTO `ipay_b2b_ke` (`id`, `ref`, `amount`, `txncost`, `curr`, `dttime`, `mmref`, `account`, `narration`, `status`, `vendor_id`, `hash`, `request_method`, `merchant_reference`, `api_response`, `api_response_text`, `channelcost`, `channel`, `api_response_time`, `third_party_conversation_id`, `channel_metadata`, `receipient_metadata`) VALUES
(18, 'B2BDEMO3144C152484390345628427', '10.00', '0.00', 'kes', '2018-04-27 18:45:03', 'MDR21H4ETW', '600000', '', 1, 'demo', '', 'API', 'reference18', 'SUCCESS', 'The service request is processed successfully.', 0.00, 'MPESAPAYBILL', '2018-04-27 18:45:37', 'AG_20180427_000041e18d94075f56e7', 'Working Account|KES|599860.00|599860.00|0.00|0.00', '600000 - saf test org'),
(19, 'B2BDEMO3144D1525953564187780039', '1000.00', '0.00', 'kes', '2018-05-10 14:59:24', '', 'mpesa', '', 1, 'demo', '', 'API', '12345678', 'FAILED', 'Bad Request - Invalid PartyB', 0.00, 'MPESAPAYBILL', '0000-00-00 00:00:00', '', '400.002.02', ''),
(20, 'B2BDEMO3144E1528284844126737909', '10.00', '0.00', 'kes', '2018-06-06 14:34:04', 'MF60000000', '174379', 'test narration another', 1, 'demo', '', 'API', 'reference25', 'FAILED', 'The ReceiverParty information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-06 14:34:40', 'AG_20180606_0000468e6095038fa793', '', ''),
(21, 'B2BDEMO3144E1528285253222735053', '10.00', '0.00', 'kes', '2018-06-06 14:40:53', 'MF671H5865', '600000', 'test narration another', 1, 'demo', '', 'API', 'reference26', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-06 14:41:29', 'AG_20180606_00006263315c30897b92', '', ''),
(22, 'B2BDEMO3144E1528376734157637403', '10.00', '0.00', 'kes', '2018-06-07 16:05:33', '', '10', '', 0, 'demo', '', 'API', 'bar', '', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '', ''),
(23, 'B2BDEMO3144E152838176738675833', '10.00', '0.00', 'kes', '2018-06-07 17:29:27', 'MF721H58H4', '600000', 'test narration another', 1, 'demo', '', 'API', 'reference27', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-07 17:30:07', 'AG_20180607_00004b0bbbd8ce74e407', '', ''),
(24, 'B2BDEMO3144E152839407775099911', '10.00', '0.00', 'kes', '2018-06-07 20:54:37', 'MF771H58KL', '600000', '1', 1, 'demo', '', 'API', 'REF1', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-07 20:55:12', 'AG_20180607_0000646b2b01b02f5acd', '', ''),
(25, 'B2BDEMO3144E152839415528171548', '10.00', '0.00', 'kes', '2018-06-07 20:55:55', '', '600000', '1', 0, 'demo', '', 'API', 'REF2', '', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '', ''),
(26, 'B2BDEMO3144E1528397467171686260', '10.00', '0.00', 'kes', '2018-06-07 21:51:07', '', '510800', '1', 0, 'demo', '', 'API', 'REF3', '', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '', ''),
(27, 'B2BDEMO3144E152839750123565340', '10.00', '0.00', 'kes', '2018-06-07 21:51:41', '', '510800', '1', 0, 'demo', '', 'API', 'REF4', '', 'Accept the service request successfully.', 0.00, 'MPESAPAYBILL', '0000-00-00 00:00:00', 'AG_20180607_00004032a501f04142b1', '', ''),
(28, 'B2BDEMO3144E1528398362227207075', '10.00', '0.00', 'kes', '2018-06-07 22:06:02', '', '510800', '1', 1, 'demo', '', 'API', 'REF5', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(29, 'B2BDEMO3144E1528398443116266247', '10.00', '0.00', 'kes', '2018-06-07 22:07:23', '', '174379', '1', 1, 'demo', '', 'API', 'REF6', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(30, 'B2BDEMO3144E1528398572139205009', '10.00', '0.00', 'kes', '2018-06-07 22:09:32', '', '123456', '1', 1, 'demo', '', 'API', 'REF7', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(31, 'B2BDEMO3144E152839863861227830', '10.00', '0.00', 'kes', '2018-06-07 22:10:38', 'MF70000000', '844844', '1', 1, 'demo', '', 'API', 'REF8', 'FAILED', 'The ReceiverParty information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-07 22:11:15', 'AG_20180607_00006f45bdfcf7e2b6a1', '', ''),
(32, 'B2BDEMO3144E1528401010162461775', '10.00', '0.00', 'kes', '2018-06-07 22:50:10', 'MF791H58L7', '600000', 'B2BDEMO3144E1528398638612', 1, 'demo', '', 'API', 'REF9', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-07 22:50:46', 'AG_20180607_000075ce49b72858988b', '', ''),
(33, 'B2BDEMO3144E1528899872232968249', '10.00', '0.00', 'kes', '2018-06-13 17:24:32', 'MFD31H5A0F', '600000', 'test1', 1, 'demo', '', 'API', 'REF10', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-13 17:25:08', 'AG_20180613_000047cba2d23d06e478', '', ''),
(34, 'B2BDEMO3144E1528900603153344705', '10.00', '0.00', 'kes', '2018-06-13 17:36:43', '', '510800', 'test1', 0, 'demo', '', 'API', 'REF11', '', 'Accept the service request successfully.', 0.00, 'MPESAPAYBILL', '0000-00-00 00:00:00', 'AG_20180613_000060df486155536418', '', ''),
(35, 'B2BDEMO3144E1528900987112752615', '10.00', '0.00', 'kes', '2018-06-13 17:43:07', '', '123456', '1', 1, 'demo', '', 'API', 'REF17', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(36, 'B2BDEMO3144E152890143833635612', '10.00', '0.00', 'kes', '2018-06-13 17:50:38', '', '174379', '1', 1, 'demo', '', 'API', 'REF18', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(37, 'B2BDEMO3144E1528901872152457882', '10.00', '0.00', 'kes', '2018-06-13 17:57:52', '', '174379', '1', 1, 'demo', '', 'API', 'REF19', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(38, 'B2BDEMO3144E1528901969201952800', '10.00', '0.00', 'kes', '2018-06-13 17:59:29', '', '174379', '1', 1, 'demo', '', 'API', 'REF20', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(39, 'B2BDEMO3144E1528902073182405591', '10.00', '0.00', 'kes', '2018-06-13 18:01:13', 'MFD0000000', '510800', 'test1', 1, 'demo', '', 'API', 'REF12', 'FAILED', 'The ReceiverParty information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:01:48', 'AG_20180613_0000476c2f57e1d50665', '', ''),
(40, 'B2BDEMO3144E1528902120165110151', '10.00', '0.00', 'kes', '2018-06-13 18:02:00', 'MFD41H5A1U', '600000', 'test1', 1, 'demo', '', 'API', 'REF13', 'INVALID_INITIATOR_INFORMATION', 'The initiator information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:02:33', 'AG_20180613_000079084084ae9a7f06', '', ''),
(41, 'B2BDEMO3144E1528902437257171468', '10.00', '0.00', 'kes', '2018-06-13 18:07:17', 'MFD11H5A21', '600000', 'test1', 1, 'demo', '', 'API', 'REF14', 'INVALID_INITIATOR_INFORMATION', 'The initiator information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:07:56', 'AG_20180613_00007e5c4a5a45577678', '', ''),
(42, 'B2BDEMO3144E1528902493234991978', '10.00', '0.00', 'kes', '2018-06-13 18:08:13', '', '174379', '1', 1, 'demo', '', 'API', 'REF21', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(43, 'B2BDEMO3144E1528902629150608989', '10.00', '0.00', 'kes', '2018-06-13 18:10:29', '', '600000', '1', 1, 'demo', '', 'API', 'REF22', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(44, 'B2BDEMO3144E152890318271550310', '10.00', '0.00', 'kes', '2018-06-13 18:19:42', 'MFD71H5A2H', '600000', 'test1', 1, 'demo', '', 'API', 'REF31', 'INVALID_INITIATOR_INFORMATION', 'The initiator information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:20:17', 'AG_20180613_00004b88d5d4d66a4221', '', ''),
(45, 'B2BDEMO3144E1528903273166845154', '10.00', '0.00', 'kes', '2018-06-13 18:21:13', 'MFD0000000', '600000', 'test1', 1, 'demo', '', 'API', 'REF32', 'INVALID_INITIATOR_INFORMATION', 'The initiator information is invalid.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:21:46', 'AG_20180613_00004deda5907781ba6d', '', ''),
(46, 'B2BDEMO3144E1528903318168403419', '10.00', '0.00', 'kes', '2018-06-13 18:21:58', 'MFD91H5A2J', '600000', 'test1', 1, 'demo', '', 'API', 'REF33', 'FAILED', 'External validation failed for the C2B transaction.', 0.00, 'MPESAPAYBILL', '2018-06-13 18:22:33', 'AG_20180613_00007cb1f27045e92aaf', '', ''),
(47, 'B2BDEMO3144E1528903933192520629', '10.00', '0.00', 'kes', '2018-06-13 18:32:13', '', '600000', '1', 1, 'demo', '', 'API', 'REF23', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', ''),
(48, 'B2BDEMO3144E1529579002230145912', '10.00', '0.00', 'kes', '2018-06-21 14:03:22', '', '600000', '1', 1, 'demo', '', 'API', 'REF24', 'FAILED', '', 0.00, 'MPESATILL', '0000-00-00 00:00:00', '', '500.001.1001', '');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_billing_mer_reg_ke`
--

CREATE TABLE `ipay_billing_mer_reg_ke` (
  `id` int(11) NOT NULL,
  `merprefix` varchar(25) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `safaricom_post` varchar(100) NOT NULL,
  `safaricom_pre` text NOT NULL,
  `airtel` varchar(100) NOT NULL,
  `kplc_prepaid` varchar(100) NOT NULL,
  `startimes` varchar(100) NOT NULL,
  `nairobi_water` varchar(100) NOT NULL,
  `gotv` varchar(100) NOT NULL,
  `dstv` varchar(100) NOT NULL,
  `zuku` varchar(100) NOT NULL,
  `orange` varchar(100) NOT NULL,
  `kplc_postpaid` varchar(100) NOT NULL,
  `zuku_phone` varchar(100) NOT NULL,
  `zuku_sat` varchar(100) NOT NULL,
  `telkom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_blacklist_iin_ke`
--

CREATE TABLE `ipay_blacklist_iin_ke` (
  `id` int(11) NOT NULL,
  `iin` varchar(6) CHARACTER SET latin1 NOT NULL,
  `vendor` varchar(15) CHARACTER SET latin1 NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_blacklist_ip_ke`
--

CREATE TABLE `ipay_blacklist_ip_ke` (
  `id` int(11) NOT NULL,
  `ip` varchar(50) CHARACTER SET latin1 NOT NULL,
  `vendor` varchar(15) CHARACTER SET latin1 NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_blacklist_msisdn_ke`
--

CREATE TABLE `ipay_blacklist_msisdn_ke` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(15) CHARACTER SET latin1 NOT NULL,
  `vendor` varchar(15) CHARACTER SET latin1 NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_bulkpay_bo_ke`
--

CREATE TABLE `ipay_bulkpay_bo_ke` (
  `id` int(11) NOT NULL,
  `ref` varchar(70) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txncost` decimal(10,2) NOT NULL,
  `dttime` datetime NOT NULL,
  `mmref` varchar(50) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `vendor_id` varchar(15) NOT NULL,
  `hash` varchar(65) NOT NULL,
  `request_method` enum('BOF','API','RCN') NOT NULL,
  `merchant_reference` varchar(50) NOT NULL COMMENT 'This is a unique reference sent by the merchant it should be unique',
  `api_response` text NOT NULL,
  `channelcost` double(10,2) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `api_response_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_bulkpay_bo_ke_test`
--

CREATE TABLE `ipay_bulkpay_bo_ke_test` (
  `id` int(11) NOT NULL,
  `ref` varchar(70) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txncost` decimal(10,2) NOT NULL,
  `dttime` datetime NOT NULL,
  `mmref` varchar(15) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `vendor_id` varchar(10) NOT NULL,
  `hash` varchar(65) NOT NULL,
  `request_method` enum('BOF','API','RCN','') NOT NULL,
  `merchant_reference` varchar(30) NOT NULL COMMENT 'This is a unique reference sent by the merchant it should be unique',
  `api_response` text NOT NULL,
  `channelcost` double(10,2) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `api_response_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_cc_blacklist_usr_ke`
--

CREATE TABLE `ipay_cc_blacklist_usr_ke` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `cardnum` varchar(20) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vendor` varchar(15) NOT NULL DEFAULT 'jumia'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_cc_failed_txn_usr_details_ke`
--

CREATE TABLE `ipay_cc_failed_txn_usr_details_ke` (
  `id` int(11) NOT NULL,
  `vendorid` varchar(15) NOT NULL,
  `email` varchar(120) NOT NULL,
  `cardnum` varchar(20) NOT NULL,
  `attempt` tinyint(4) NOT NULL,
  `txndate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `clientip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_cc_kcb_txn_tbl_ke`
--

CREATE TABLE `ipay_cc_kcb_txn_tbl_ke` (
  `id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txn_ref` varchar(20) NOT NULL,
  `resp_code` varchar(5) NOT NULL DEFAULT '255',
  `txn_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ipay_id` varchar(15) NOT NULL,
  `cc_type` varchar(10) NOT NULL,
  `client_ip` varchar(50) NOT NULL,
  `cc_partial_num` varchar(20) NOT NULL,
  `cc_fraud_score` varchar(10) NOT NULL,
  `txn_message` varchar(200) NOT NULL,
  `txn_receipt_no` varchar(30) NOT NULL,
  `txn_risk` varchar(10) NOT NULL,
  `client_email` varchar(150) NOT NULL,
  `client_tel` varchar(15) NOT NULL,
  `cc_distance` int(6) NOT NULL,
  `cc_countrymatch` varchar(5) NOT NULL,
  `cc_proxyscore` double(3,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_cc_txn_tbl_ke`
--

CREATE TABLE `ipay_cc_txn_tbl_ke` (
  `id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txn_ref` varchar(20) NOT NULL,
  `resp_code` varchar(5) NOT NULL DEFAULT '255',
  `txn_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ipay_id` varchar(10) NOT NULL,
  `cc_type` varchar(20) NOT NULL,
  `client_ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_cost_per_channel_ke`
--

CREATE TABLE `ipay_cost_per_channel_ke` (
  `id` int(11) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `type` enum('fixed','percent') NOT NULL DEFAULT 'percent'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_kcb_invoice_keys_ke`
--

CREATE TABLE `ipay_kcb_invoice_keys_ke` (
  `vid` varchar(15) NOT NULL,
  `id` tinyint(1) NOT NULL,
  `migid` varchar(15) NOT NULL,
  `acc_code` varchar(8) NOT NULL,
  `hsh` varchar(32) NOT NULL,
  `hshtype` varchar(10) NOT NULL DEFAULT 'SHA256',
  `curr` varchar(3) NOT NULL,
  `live` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_kcb_key_ke`
--

CREATE TABLE `ipay_kcb_key_ke` (
  `id` tinyint(1) NOT NULL,
  `migid` varchar(8) NOT NULL,
  `acc_code` varchar(8) NOT NULL,
  `hsh` varchar(32) NOT NULL,
  `hshtype` varchar(10) NOT NULL,
  `curr` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_kcb_key_test_ke`
--

CREATE TABLE `ipay_kcb_key_test_ke` (
  `id` tinyint(1) NOT NULL,
  `migid` varchar(8) NOT NULL,
  `acc_code` varchar(8) NOT NULL,
  `hsh` varchar(32) NOT NULL,
  `hshtype` varchar(10) NOT NULL,
  `curr` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_ksw_active_orders_ke`
--

CREATE TABLE `ipay_ksw_active_orders_ke` (
  `id` bigint(20) NOT NULL,
  `refnum` varchar(20) NOT NULL,
  `order_id` varchar(20) NOT NULL,
  `ipay_id` varchar(10) NOT NULL,
  `cllbk` varchar(200) NOT NULL,
  `cstmail` varchar(1) NOT NULL,
  `txndatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_ksw_txn_tbl_ke`
--

CREATE TABLE `ipay_ksw_txn_tbl_ke` (
  `id` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `txn_ref` varchar(20) NOT NULL,
  `resp_code` varchar(5) NOT NULL,
  `txn_date` datetime NOT NULL,
  `ipay_id` varchar(10) NOT NULL,
  `std_fee` decimal(10,2) NOT NULL DEFAULT '30.00',
  `systemAuditNr` varchar(25) NOT NULL,
  `raw_Data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_malipo_ke`
--

CREATE TABLE `ipay_malipo_ke` (
  `id` int(11) NOT NULL,
  `mer_bankaccname` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankbranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccnum` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `curr` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES',
  `mer_email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_vid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_recon_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mer_bankswift` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mer_pay_mode` enum('Pesalink','EFT','RTGS','MPESA','Airtelmoney','eLipa') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EFT',
  `mer_status` int(1) NOT NULL DEFAULT '0' COMMENT 'merchant status',
  `mer_payment_frm_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'relates to the payment status table entry of that vendor',
  `b2cstatus` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `mobileref` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `b2cipay_reference` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_malipo_mail_ke`
--

CREATE TABLE `ipay_malipo_mail_ke` (
  `id` int(11) NOT NULL,
  `vid` varchar(20) NOT NULL,
  `target` varchar(120) NOT NULL,
  `subj` varchar(50) NOT NULL,
  `msg` text NOT NULL,
  `hash` varchar(64) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT '0',
  `mer_recon_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='iPay Merchants Recon Email Messages';

-- --------------------------------------------------------

--
-- Table structure for table `ipay_merchant_reg_ke`
--

CREATE TABLE `ipay_merchant_reg_ke` (
  `id` int(11) NOT NULL,
  `mername` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `merrefno` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `merprefix` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mercity` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `mertel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `meremail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `merpobox` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `notifyurl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `merhost` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `mccid` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mccidus` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `parentdb` varchar(22) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'micstation_ipay_mer_ke',
  `seckey` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ipaykey',
  `paybillchannel` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '510800',
  `hashidseckey` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mobileappipyseckkey',
  `largedata` tinyint(1) NOT NULL DEFAULT '0',
  `mmoney` decimal(10,2) NOT NULL DEFAULT '3.50',
  `kcbmbanking` decimal(10,2) NOT NULL DEFAULT '3.50',
  `eblmbanking` decimal(10,2) NOT NULL DEFAULT '1.00',
  `dcard` decimal(10,2) NOT NULL DEFAULT '3.80',
  `mcard` decimal(10,2) DEFAULT '3.50',
  `ccard` decimal(10,2) NOT NULL DEFAULT '3.50',
  `mpos` decimal(10,2) NOT NULL DEFAULT '3.50',
  `ipaywallet` decimal(10,2) NOT NULL DEFAULT '1.00',
  `saccomfi` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bulkpay` decimal(10,2) NOT NULL DEFAULT '100.00',
  `brandname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bulkpaylim` int(11) NOT NULL DEFAULT '50000',
  `xelipa` tinyint(4) NOT NULL DEFAULT '1',
  `xmpesa` tinyint(4) NOT NULL DEFAULT '1',
  `xairtel` tinyint(4) NOT NULL DEFAULT '1',
  `xequity` tinyint(4) NOT NULL DEFAULT '1',
  `xkcb` tinyint(4) NOT NULL DEFAULT '1',
  `xdcard` tinyint(4) NOT NULL DEFAULT '1',
  `xccard` tinyint(4) NOT NULL DEFAULT '1',
  `frdCountryMatch` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes,No',
  `frdBinMatch` varchar(18) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes,No,NotFound,NA',
  `frdRiskScore` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT ',1.00',
  `frdHighRiskCountry` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT ',Yes,No',
  `frdCarderEmail` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `frdAnonymousProxy` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `frdCurrency` varchar(19) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES,USD,EUR',
  `frdMaxTotal` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '200000,3000,2000',
  `frdDistance` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '500,500,500',
  `frdSpecialCase` tinyint(1) NOT NULL DEFAULT '0',
  `to_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bulkpaynew` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"mpesa":"100","airtelmoney":"100","eLipa":"0","pesalink":"50"}',
  `cantokenize` tinyint(1) NOT NULL DEFAULT '1',
  `cardretries` int(2) NOT NULL DEFAULT '3',
  `created_at` datetime NOT NULL,
  `salesperson` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='iPay: Merchant Details';

--
-- Dumping data for table `ipay_merchant_reg_ke`
--

INSERT INTO `ipay_merchant_reg_ke` (`id`, `mername`, `merrefno`, `merprefix`, `mercity`, `mertel`, `meremail`, `merpobox`, `notifyurl`, `merhost`, `active`, `mccid`, `mccidus`, `mfi`, `parentdb`, `seckey`, `paybillchannel`, `hashidseckey`, `largedata`, `mmoney`, `kcbmbanking`, `eblmbanking`, `dcard`, `mcard`, `ccard`, `mpos`, `ipaywallet`, `saccomfi`, `bulkpay`, `brandname`, `bulkpaylim`, `xelipa`, `xmpesa`, `xairtel`, `xequity`, `xkcb`, `xdcard`, `xccard`, `frdCountryMatch`, `frdBinMatch`, `frdRiskScore`, `frdHighRiskCountry`, `frdCarderEmail`, `frdAnonymousProxy`, `frdCurrency`, `frdMaxTotal`, `frdDistance`, `frdSpecialCase`, `to_email`, `cc_email`, `bulkpaynew`, `cantokenize`, `cardretries`, `created_at`, `salesperson`) VALUES
(4419, 'Demo', '8d6e212da20620ca26445f88e767f305', '0000', '', '', 'ipay@ipayafrica.com', '', '', '', '1', NULL, NULL, '0', 'micstation_ipay_mer_ke', '00003', '510800', 'mobileappipyseckkey', 0, '3.50', '3.50', '1.00', '3.80', '3.50', '3.50', '3.50', '1.00', '0.00', '100.00', '', 50000, 1, 1, 1, 1, 1, 1, 1, 'Yes,No', 'Yes,No,NotFound,NA', ',1.00', ',Yes,No', 'No', 'No', 'KES,USD,EUR', '200000,3000,2000', '500,500,500', 0, '', '', '{\"mpesa\":\"100\",\"airtelmoney\":\"100\",\"eLipa\":\"0\",\"pesalink\":\"50\"}', 1, 3, '2018-04-27 00:00:00', ''),
(4420, 'Merchant Test 1', 'fe01ce2a7fbac8fafaed7c982a04e229', 'DEMO', 'Nairobi', '2547XXXXX', 'example@example.com', '', '', '', '1', NULL, NULL, '0', 'micstation_ipay_mer_ke', 'SECURITYKEY', '510800', 'mobileappipyseckkey', 0, '3.50', '3.50', '1.00', '3.80', '3.50', '3.50', '3.50', '1.00', '0.00', '100.00', '', 50000, 1, 1, 1, 1, 1, 1, 1, 'Yes,No', 'Yes,No,NotFound,NA', ',1.00', ',Yes,No', 'No', 'No', 'KES,USD,EUR', '200000,3000,2000', '500,500,500', 0, '', '', '{\"mpesa\":\"100\",\"airtelmoney\":\"100\",\"eLipa\":\"0\",\"pesalink\":\"50\"}', 1, 3, '2018-05-31 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_merchant_reg_ke_backup`
--

CREATE TABLE `ipay_merchant_reg_ke_backup` (
  `id` int(11) NOT NULL,
  `mername` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `merrefno` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `merprefix` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mercity` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `mertel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `meremail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `merpobox` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `notifyurl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `merhost` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `mccid` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mccidus` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mfi` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `parentdb` varchar(22) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'micstation_ipay_mer_ke',
  `seckey` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ipaykey',
  `paybillchannel` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '510800',
  `hashidseckey` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'mobileappipyseckkey',
  `largedata` tinyint(1) NOT NULL DEFAULT '0',
  `mmoney` decimal(10,2) NOT NULL DEFAULT '3.50',
  `kcbmbanking` decimal(10,2) NOT NULL DEFAULT '3.50',
  `eblmbanking` decimal(10,2) NOT NULL DEFAULT '1.00',
  `dcard` decimal(10,2) NOT NULL DEFAULT '3.80',
  `ccard` decimal(10,2) NOT NULL DEFAULT '3.50',
  `mpos` decimal(10,2) NOT NULL DEFAULT '3.50',
  `ipaywallet` decimal(10,2) NOT NULL DEFAULT '1.00',
  `saccomfi` decimal(10,2) NOT NULL DEFAULT '0.00',
  `bulkpay` decimal(10,2) NOT NULL DEFAULT '100.00',
  `brandname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bulkpaylim` int(11) NOT NULL DEFAULT '50000',
  `xelipa` tinyint(4) NOT NULL DEFAULT '1',
  `xmpesa` tinyint(4) NOT NULL DEFAULT '1',
  `xairtel` tinyint(4) NOT NULL DEFAULT '1',
  `xequity` tinyint(4) NOT NULL DEFAULT '1',
  `xkcb` tinyint(4) NOT NULL DEFAULT '1',
  `xdcard` tinyint(4) NOT NULL DEFAULT '1',
  `xccard` tinyint(4) NOT NULL DEFAULT '1',
  `frdCountryMatch` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes,No',
  `frdBinMatch` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes,No',
  `frdRiskScore` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT ',2.00',
  `frdHighRiskCountry` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT ',Yes,No',
  `frdCarderEmail` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `frdAnonymousProxy` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `frdCurrency` varchar(19) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES,USD,EUR',
  `frdMaxTotal` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '200000,3000,2000',
  `frdDistance` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '500,500,500',
  `frdSpecialCase` tinyint(1) NOT NULL DEFAULT '0',
  `to_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cc_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='iPay: Merchant Details';

-- --------------------------------------------------------

--
-- Table structure for table `ipay_merchant_reports`
--

CREATE TABLE `ipay_merchant_reports` (
  `id` int(11) NOT NULL,
  `vid` varchar(50) NOT NULL,
  `vid_created_date` datetime NOT NULL,
  `number_of_transactions` bigint(20) NOT NULL,
  `transaction_amount` decimal(10,2) NOT NULL,
  `period_year` varchar(10) NOT NULL,
  `period_month` varchar(10) NOT NULL,
  `sales_id` varchar(25) NOT NULL,
  `account_type` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_online_callback_log_ke`
--

CREATE TABLE `ipay_online_callback_log_ke` (
  `id` int(11) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `callback` text NOT NULL,
  `saangapi` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='iPay callback log';

-- --------------------------------------------------------

--
-- Table structure for table `ipay_orders`
--

CREATE TABLE `ipay_orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(60) NOT NULL,
  `ipay_id` varchar(15) NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `vid` varchar(10) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_paybill_tarrif`
--

CREATE TABLE `ipay_paybill_tarrif` (
  `id` tinyint(4) NOT NULL,
  `min` varchar(10) CHARACTER SET latin1 NOT NULL,
  `max` varchar(10) CHARACTER SET latin1 NOT NULL,
  `client_pays_saf` varchar(5) CHARACTER SET latin1 NOT NULL,
  `ids_pays_saf` varchar(5) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='MPESA Paybill Payment Tarrif';

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pos_comm_txn_tbl`
--

CREATE TABLE `ipay_pos_comm_txn_tbl` (
  `id` int(11) NOT NULL,
  `terminal` varchar(20) CHARACTER SET latin1 NOT NULL,
  `comm` decimal(5,2) NOT NULL DEFAULT '3.00',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `merchant_name` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pos_txn_tbl`
--

CREATE TABLE `ipay_pos_txn_tbl` (
  `id` bigint(20) NOT NULL,
  `amount` int(11) NOT NULL,
  `saf_comm` decimal(5,2) NOT NULL,
  `ipay` decimal(10,2) NOT NULL,
  `paystream` decimal(10,2) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `terminal` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pymnt_tbl_ke`
--

CREATE TABLE `ipay_pymnt_tbl_ke` (
  `id` int(11) NOT NULL,
  `mer_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mer_prfx` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_txn_id_date` varchar(19) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ReceivingDateTime',
  `mer_status` int(1) NOT NULL DEFAULT '0' COMMENT 'merchant status',
  `release_level` decimal(12,2) NOT NULL DEFAULT '5000.00',
  `mer_email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_tel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankbranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccname` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccnum` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankswift` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mer_pay_mode` enum('EFT','RTGS','MPESA','Airtelmoney','eLipa','Pesalink') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EFT',
  `mer_pay_cost` decimal(10,2) NOT NULL DEFAULT '110.00',
  `sat` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sun` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mon` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `tue` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `wed` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `thur` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fri` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `startmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `midmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `curr` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ipay_pymnt_tbl_ke`
--

INSERT INTO `ipay_pymnt_tbl_ke` (`id`, `mer_id`, `mer_prfx`, `mer_txn_id_date`, `mer_status`, `release_level`, `mer_email`, `mer_tel`, `mer_bankname`, `mer_bankbranch`, `mer_bankaccname`, `mer_bankaccnum`, `mer_bankswift`, `mer_pay_mode`, `mer_pay_cost`, `sat`, `sun`, `mon`, `tue`, `wed`, `thur`, `fri`, `startmnth`, `midmnth`, `curr`) VALUES
(1, '0000', '0000', '2018-07-01 00:00:00', 0, '5000.00', 'kiseej@gmail.com', '0705118708', 'boa', 'gong', 'ipay biz', '23456789876543', '00', 'EFT', '110.00', '0', '0', '1', '', '', '', '', '', '', 'KES');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pymnt_tbl_ke_backup`
--

CREATE TABLE `ipay_pymnt_tbl_ke_backup` (
  `id` int(11) NOT NULL,
  `mer_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mer_prfx` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_txn_id_date` varchar(19) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ReceivingDateTime',
  `mer_status` int(1) NOT NULL DEFAULT '0' COMMENT 'merchant status',
  `release_level` decimal(12,2) NOT NULL DEFAULT '5000.00',
  `mer_email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_tel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankbranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccname` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccnum` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankswift` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mer_pay_mode` enum('EFT','RTGS','MPESA','Airtel Money','iPay Wallet') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EFT',
  `mer_pay_cost` smallint(6) NOT NULL DEFAULT '110',
  `sat` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sun` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mon` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `tue` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `wed` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `thur` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fri` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `startmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `midmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `curr` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pymnt_tbl_ke_new_backup`
--

CREATE TABLE `ipay_pymnt_tbl_ke_new_backup` (
  `id` int(11) NOT NULL,
  `mer_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mer_prfx` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_txn_id_date` varchar(19) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ReceivingDateTime',
  `mer_status` int(1) NOT NULL DEFAULT '0' COMMENT 'merchant status',
  `release_level` decimal(12,2) NOT NULL DEFAULT '5000.00',
  `mer_email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_tel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankbranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccname` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccnum` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankswift` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `mer_pay_mode` enum('EFT','RTGS','MPESA','Airtelmoney','eLipa','Pesalink') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EFT',
  `mer_pay_cost` smallint(6) NOT NULL DEFAULT '110',
  `sat` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sun` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `mon` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `tue` varchar(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `wed` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `thur` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fri` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `startmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `midmnth` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `curr` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'KES'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_pymnt_tbl_mbg`
--

CREATE TABLE `ipay_pymnt_tbl_mbg` (
  `id` mediumint(9) NOT NULL,
  `mer_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `mer_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mer_prfx` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_txn_id_date` timestamp NOT NULL DEFAULT '2013-01-01 05:00:00' COMMENT 'ReceivingDateTime',
  `mer_status` int(1) NOT NULL DEFAULT '0' COMMENT 'merchant status',
  `release_level` decimal(12,2) NOT NULL DEFAULT '5000.00',
  `mer_email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_tel` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankbranch` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccname` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankaccnum` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mer_bankswift` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mer_eft_pay` int(1) NOT NULL DEFAULT '1',
  `mer_pay_mode` enum('EFT','RTGS','MPESA') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'EFT',
  `mer_pay_cost` smallint(6) NOT NULL DEFAULT '100',
  `mer_pay_freq` enum('M,W,F','Daily','Weekly','BiWeekly','Monthly') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Weekly',
  `parentdb` enum('micstation_mbg','micstation_mbg2','micstation_mbg3') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'micstation_mbg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_special_calc_ke`
--

CREATE TABLE `ipay_special_calc_ke` (
  `id` int(11) NOT NULL,
  `vid` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Special Formula Vendors that don''t do straight through Commission calculations';

-- --------------------------------------------------------

--
-- Table structure for table `ipay_stanbic_key_ke`
--

CREATE TABLE `ipay_stanbic_key_ke` (
  `id` tinyint(1) NOT NULL,
  `vid` varchar(20) NOT NULL,
  `migid` varchar(8) NOT NULL,
  `acc_code` varchar(8) NOT NULL,
  `hsh` varchar(32) NOT NULL,
  `hshtype` varchar(10) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `midtype` enum('GOK','iPay','Corporate') NOT NULL,
  `securitytype` enum('2d','3d') NOT NULL DEFAULT '2d'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_supported_currencies_ke`
--

CREATE TABLE `ipay_supported_currencies_ke` (
  `id` tinyint(1) NOT NULL,
  `curr` varchar(30) NOT NULL,
  `cardattempts` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_supported_currencies_test_ke`
--

CREATE TABLE `ipay_supported_currencies_test_ke` (
  `id` tinyint(1) NOT NULL,
  `curr` varchar(30) NOT NULL,
  `cardattempts` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_temp_bulkpay_vals_ke`
--

CREATE TABLE `ipay_temp_bulkpay_vals_ke` (
  `id` int(11) NOT NULL,
  `vid` varchar(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_thirdparty_loans`
--

CREATE TABLE `ipay_thirdparty_loans` (
  `id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `sid` varchar(50) NOT NULL,
  `vid` varchar(25) NOT NULL,
  `thirdparty_name` varchar(50) NOT NULL,
  `loan_amount` decimal(10,2) NOT NULL,
  `thirdparty_reference` varchar(60) NOT NULL,
  `status` varchar(25) NOT NULL,
  `fname` varchar(25) NOT NULL,
  `lname` varchar(25) NOT NULL,
  `reference_hash` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_txnlog`
--

CREATE TABLE `ipay_txnlog` (
  `id` int(11) NOT NULL,
  `txndatetime` datetime NOT NULL,
  `txnid` varchar(15) NOT NULL,
  `status` varchar(15) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `diff` double(10,2) NOT NULL DEFAULT '0.00',
  `vendorid` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_autopay_ke`
--

CREATE TABLE `online_autopay_ke` (
  `id` int(11) NOT NULL,
  `tid` varchar(20) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `receivingdatetime` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `online_autopay_ke_orig`
--

CREATE TABLE `online_autopay_ke_orig` (
  `id` int(11) NOT NULL,
  `tid` varchar(20) NOT NULL,
  `sid` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `online_ipw_txns_ke`
--

CREATE TABLE `online_ipw_txns_ke` (
  `id` int(11) NOT NULL,
  `txndatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `msisdn` varchar(15) NOT NULL,
  `tid` varchar(20) NOT NULL,
  `cr` decimal(15,2) NOT NULL DEFAULT '0.00',
  `dr` decimal(15,2) NOT NULL DEFAULT '0.00',
  `vid` varchar(15) NOT NULL,
  `curr` varchar(4) NOT NULL,
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  `channel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_ke`
--

CREATE TABLE `online_ke` (
  `id` int(11) NOT NULL,
  `receivingdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txndatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `ipay_comm` decimal(10,2) NOT NULL DEFAULT '0.00',
  `msisdn` varchar(15) NOT NULL,
  `tcode` varchar(20) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `paybill` varchar(6) NOT NULL DEFAULT '510800',
  `status` varchar(7) NOT NULL DEFAULT 'offline',
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  `curr` varchar(3) NOT NULL DEFAULT 'KES',
  `txncost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `processed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `online_ke`
--

INSERT INTO `online_ke` (`id`, `receivingdatetime`, `txndatetime`, `fname`, `lname`, `amount`, `ipay_comm`, `msisdn`, `tcode`, `tid`, `vid`, `channel`, `paybill`, `status`, `refunded`, `curr`, `txncost`, `processed`) VALUES
(1, '0000-00-00 00:00:00', '2018-07-13 07:06:38', 'JOHN', 'DOE', '10.00', '0.00', '2547XXXXX', '', '', '', 'AIRTELMONEY', '510800', 'offline', 0, 'KES', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `online_ke_06_17_backup`
--

CREATE TABLE `online_ke_06_17_backup` (
  `id` int(11) NOT NULL,
  `receivingdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txndatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `ipay_comm` decimal(10,2) NOT NULL DEFAULT '0.00',
  `msisdn` varchar(15) NOT NULL,
  `tcode` varchar(20) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `paybill` varchar(6) NOT NULL DEFAULT '510800',
  `status` varchar(7) NOT NULL DEFAULT 'offline',
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  `curr` varchar(3) NOT NULL DEFAULT 'KES',
  `txncost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `processed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_ke_backup`
--

CREATE TABLE `online_ke_backup` (
  `id` int(11) NOT NULL,
  `receivingdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `txndatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `amount` decimal(20,2) NOT NULL,
  `ipay_comm` decimal(10,2) NOT NULL DEFAULT '0.00',
  `msisdn` varchar(15) NOT NULL,
  `tcode` varchar(20) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `channel` varchar(15) NOT NULL,
  `paybill` varchar(6) NOT NULL DEFAULT '510800',
  `status` varchar(7) NOT NULL DEFAULT 'offline',
  `refunded` tinyint(1) NOT NULL DEFAULT '0',
  `curr` varchar(3) NOT NULL DEFAULT 'KES',
  `txncost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `processed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_qr_ke`
--

CREATE TABLE `online_qr_ke` (
  `id` int(11) NOT NULL,
  `sid` varchar(100) CHARACTER SET latin1 NOT NULL,
  `tid` varchar(15) CHARACTER SET latin1 NOT NULL,
  `qresponse` blob NOT NULL,
  `posted_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1') CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_deletelog_ke`
--

CREATE TABLE `online_sessions_deletelog_ke` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(70) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(25) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `deleted_time` datetime NOT NULL,
  `triggered_by` varchar(20) NOT NULL,
  `isdeleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_ke`
--

CREATE TABLE `online_sessions_ke` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(100) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(34) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `sent_time` datetime NOT NULL,
  `no_attempt` int(11) NOT NULL,
  `response_txt` varchar(200) NOT NULL,
  `eazzypay` tinyint(1) NOT NULL DEFAULT '1',
  `autopay` int(1) NOT NULL DEFAULT '0',
  `email_reminder` int(11) NOT NULL DEFAULT '0',
  `pesalink` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_ke_07_05_backup`
--

CREATE TABLE `online_sessions_ke_07_05_backup` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(100) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(34) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `sent_time` datetime NOT NULL,
  `no_attempt` int(11) NOT NULL,
  `response_txt` varchar(20) NOT NULL,
  `eazzypay` tinyint(1) NOT NULL DEFAULT '1',
  `autopay` int(1) NOT NULL DEFAULT '0',
  `email_reminder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_ke_2017`
--

CREATE TABLE `online_sessions_ke_2017` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(100) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(34) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `sent_time` datetime NOT NULL,
  `no_attempt` int(11) NOT NULL,
  `response_txt` varchar(200) NOT NULL,
  `eazzypay` tinyint(1) NOT NULL DEFAULT '1',
  `autopay` int(1) NOT NULL DEFAULT '0',
  `email_reminder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_ke_backup`
--

CREATE TABLE `online_sessions_ke_backup` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(100) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(25) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `sent_time` datetime NOT NULL,
  `no_attempt` int(11) NOT NULL,
  `response_txt` varchar(20) NOT NULL,
  `eazzypay` tinyint(1) NOT NULL DEFAULT '1',
  `autopay` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `online_sessions_ke_test`
--

CREATE TABLE `online_sessions_ke_test` (
  `id` int(11) NOT NULL,
  `sessdatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(100) NOT NULL,
  `vid` varchar(15) NOT NULL,
  `oid` varchar(34) NOT NULL,
  `tid` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curr` varchar(3) NOT NULL,
  `tcode` varchar(15) NOT NULL,
  `live` tinyint(1) NOT NULL DEFAULT '1',
  `mpesa` tinyint(1) NOT NULL DEFAULT '1',
  `airtel` tinyint(1) NOT NULL DEFAULT '1',
  `debitcard` tinyint(1) NOT NULL DEFAULT '1',
  `mobilebanking` tinyint(1) NOT NULL DEFAULT '1',
  `creditcard` tinyint(1) NOT NULL DEFAULT '1',
  `mkoporahisi` tinyint(1) NOT NULL DEFAULT '1',
  `saida` tinyint(1) NOT NULL DEFAULT '1',
  `amount` decimal(20,2) NOT NULL,
  `p1` varchar(20) NOT NULL,
  `p2` varchar(20) NOT NULL,
  `p3` varchar(20) NOT NULL,
  `p4` varchar(20) NOT NULL,
  `callback` varchar(255) NOT NULL,
  `cartcallback` varchar(150) NOT NULL,
  `cst` int(11) NOT NULL,
  `crl` int(11) NOT NULL,
  `numeric_sid` bigint(20) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `elipa` int(11) NOT NULL DEFAULT '1',
  `rest` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `sent_time` datetime NOT NULL,
  `no_attempt` int(11) NOT NULL,
  `response_txt` varchar(20) NOT NULL,
  `eazzypay` tinyint(1) NOT NULL DEFAULT '1',
  `autopay` int(1) NOT NULL DEFAULT '0',
  `email_reminder` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `operatesttbl`
--

CREATE TABLE `operatesttbl` (
  `txnid` varchar(34) DEFAULT NULL,
  `amount` varchar(7) DEFAULT NULL,
  `wallet_type` varchar(7) DEFAULT NULL,
  `recepient` varchar(14) DEFAULT NULL,
  `sentstatus` int(11) NOT NULL,
  `failure_reason` varchar(15) DEFAULT NULL,
  `originaloid` varchar(50) NOT NULL,
  `updatestatus` varchar(50) NOT NULL,
  `ipay_reference` varchar(50) NOT NULL,
  `header_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `operatesttbl_old`
--

CREATE TABLE `operatesttbl_old` (
  `txnid` varchar(35) NOT NULL DEFAULT '',
  `amount` varchar(9) DEFAULT NULL,
  `recepient` varchar(14) DEFAULT NULL,
  `sentstatus` int(11) NOT NULL DEFAULT '0',
  `originaloid` varchar(50) NOT NULL,
  `updatestatus` varchar(30) NOT NULL,
  `ipay_reference` varchar(60) NOT NULL,
  `header_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `autopay_transactions`
--
DROP TABLE IF EXISTS `autopay_transactions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`micstation`@`%` SQL SECURITY DEFINER VIEW `autopay_transactions`  AS  select `b`.`id` AS `id`,`a`.`sid` AS `sid`,`b`.`amount` AS `online_amount`,`a`.`amount` AS `session_amount`,`b`.`receivingdatetime` AS `receivingdatetime`,`a`.`sessdatetime` AS `sessdatetime`,`a`.`tid` AS `tid`,`a`.`vid` AS `vid`,`b`.`tcode` AS `tcode`,`b`.`ipay_comm` AS `ipay_comm`,`b`.`curr` AS `curr`,`b`.`msisdn` AS `msisdn`,`a`.`oid` AS `oid`,`a`.`p1` AS `p1`,`a`.`p2` AS `p2`,`a`.`p3` AS `p3`,`a`.`p4` AS `p4`,`a`.`callback` AS `callback`,`b`.`channel` AS `channel`,`a`.`phone` AS `phone`,`a`.`email` AS `email`,`b`.`fname` AS `fname`,`b`.`lname` AS `lname` from (`online_ke` `b` join `online_sessions_ke` `a` on((`b`.`tid` = `a`.`tid`))) where ((`a`.`vid` <> 'demo') and (`a`.`live` = 1) and (`a`.`autopay` = 1) and (`b`.`tid` = `a`.`tid`) and (`b`.`vid` = `a`.`vid`) and (`a`.`tid` > '') and (`a`.`sent` = 0) and (`b`.`receivingdatetime` >= (now() + interval ((6 * 60) + 30) minute)) and (not((`b`.`channel` like 'visa%'))) and (`a`.`vid` = `b`.`vid`) and (`b`.`status` like 'online')) ;

-- --------------------------------------------------------

--
-- Structure for view `autopay_transactions_new`
--
DROP TABLE IF EXISTS `autopay_transactions_new`;

CREATE ALGORITHM=UNDEFINED DEFINER=`micstation`@`%` SQL SECURITY DEFINER VIEW `autopay_transactions_new`  AS  select `b`.`id` AS `id`,`a`.`sid` AS `sid`,`b`.`amount` AS `online_amount`,`a`.`amount` AS `session_amount`,`b`.`receivingdatetime` AS `receivingdatetime`,`a`.`sessdatetime` AS `sessdatetime`,`a`.`tid` AS `tid`,`a`.`vid` AS `vid`,`b`.`tcode` AS `tcode`,`b`.`ipay_comm` AS `ipay_comm`,`b`.`curr` AS `curr`,`b`.`msisdn` AS `msisdn`,`a`.`oid` AS `oid`,`a`.`p1` AS `p1`,`a`.`p2` AS `p2`,`a`.`p3` AS `p3`,`a`.`p4` AS `p4`,`a`.`callback` AS `callback`,`b`.`channel` AS `channel`,`a`.`phone` AS `phone`,`a`.`email` AS `email`,`b`.`fname` AS `fname`,`b`.`lname` AS `lname` from (`online_ke` `b` join `online_sessions_ke` `a` on((`b`.`tid` = `a`.`tid`))) where ((`a`.`vid` = 'opera') and (`a`.`live` = 1) and (`a`.`autopay` = 1) and (`a`.`tid` > '') and (`a`.`sent` = 0) and (`b`.`receivingdatetime` <= (now() + interval ((6 * 60) + 30) minute)) and (not((`b`.`channel` like 'visa%'))) and (`b`.`status` like 'online') and (`a`.`vid` = `b`.`vid`)) order by `b`.`receivingdatetime` desc ;

-- --------------------------------------------------------

--
-- Structure for view `autopay_transactions_old`
--
DROP TABLE IF EXISTS `autopay_transactions_old`;

CREATE ALGORITHM=UNDEFINED DEFINER=`micstation`@`%` SQL SECURITY DEFINER VIEW `autopay_transactions_old`  AS  select `b`.`id` AS `id`,`a`.`sid` AS `sid`,`b`.`amount` AS `online_amount`,`a`.`amount` AS `session_amount`,`b`.`receivingdatetime` AS `receivingdatetime`,`a`.`sessdatetime` AS `sessdatetime`,`a`.`tid` AS `tid`,`a`.`vid` AS `vid`,`b`.`tcode` AS `tcode`,`b`.`ipay_comm` AS `ipay_comm`,`b`.`curr` AS `curr`,`b`.`msisdn` AS `msisdn`,`a`.`oid` AS `oid`,`a`.`p1` AS `p1`,`a`.`p2` AS `p2`,`a`.`p3` AS `p3`,`a`.`p4` AS `p4`,`a`.`callback` AS `callback`,`b`.`channel` AS `channel`,`a`.`phone` AS `phone`,`a`.`email` AS `email`,`b`.`fname` AS `fname`,`b`.`lname` AS `lname` from (`online_ke` `b` join `online_sessions_ke` `a` on((`b`.`tid` = `a`.`tid`))) where ((`a`.`vid` <> 'demo') and (`a`.`vid` <> 'opera') and (`a`.`live` = 1) and (`a`.`autopay` = 1) and (`a`.`tid` > '') and (`a`.`sent` = 0) and (`b`.`receivingdatetime` <= (now() + interval ((6 * 60) + 30) minute)) and (not((`b`.`channel` like 'visa%'))) and (`b`.`status` like 'online') and (`a`.`vid` = `b`.`vid`)) order by `b`.`receivingdatetime` ;

-- --------------------------------------------------------

--
-- Structure for view `autopay_transactions_test`
--
DROP TABLE IF EXISTS `autopay_transactions_test`;

CREATE ALGORITHM=UNDEFINED DEFINER=`micstation`@`%` SQL SECURITY DEFINER VIEW `autopay_transactions_test`  AS  select `b`.`id` AS `id`,`a`.`sid` AS `sid`,`b`.`amount` AS `online_amount`,`a`.`amount` AS `session_amount`,`b`.`receivingdatetime` AS `receivingdatetime`,`a`.`sessdatetime` AS `sessdatetime`,`a`.`tid` AS `tid`,`a`.`vid` AS `vid`,`b`.`tcode` AS `tcode`,`b`.`ipay_comm` AS `ipay_comm`,`b`.`curr` AS `curr`,`b`.`msisdn` AS `msisdn`,`a`.`oid` AS `oid`,`a`.`p1` AS `p1`,`a`.`p2` AS `p2`,`a`.`p3` AS `p3`,`a`.`p4` AS `p4`,`a`.`callback` AS `callback`,`b`.`channel` AS `channel`,`a`.`phone` AS `phone`,`a`.`email` AS `email`,`b`.`fname` AS `fname`,`b`.`lname` AS `lname` from (`online_ke` `b` join `online_sessions_ke` `a`) where ((`b`.`tid` = `a`.`tid`) and (`a`.`vid` <> 'demo') and (`a`.`live` = 1) and (`a`.`autopay` = 1) and (`a`.`tid` <> '') and (`a`.`sent` = 1)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ipay_b2b_ke`
--
ALTER TABLE `ipay_b2b_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref` (`ref`),
  ADD KEY `mmref` (`mmref`),
  ADD KEY `merchant_reference` (`merchant_reference`);

--
-- Indexes for table `ipay_billing_mer_reg_ke`
--
ALTER TABLE `ipay_billing_mer_reg_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `merprefix` (`merprefix`);

--
-- Indexes for table `ipay_blacklist_iin_ke`
--
ALTER TABLE `ipay_blacklist_iin_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_blacklist_ip_ke`
--
ALTER TABLE `ipay_blacklist_ip_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_blacklist_msisdn_ke`
--
ALTER TABLE `ipay_blacklist_msisdn_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_bulkpay_bo_ke`
--
ALTER TABLE `ipay_bulkpay_bo_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref` (`ref`);

--
-- Indexes for table `ipay_bulkpay_bo_ke_test`
--
ALTER TABLE `ipay_bulkpay_bo_ke_test`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref` (`ref`),
  ADD KEY `ref_2` (`ref`),
  ADD KEY `amount` (`amount`);

--
-- Indexes for table `ipay_cc_blacklist_usr_ke`
--
ALTER TABLE `ipay_cc_blacklist_usr_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_cc_failed_txn_usr_details_ke`
--
ALTER TABLE `ipay_cc_failed_txn_usr_details_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_cc_kcb_txn_tbl_ke`
--
ALTER TABLE `ipay_cc_kcb_txn_tbl_ke`
  ADD PRIMARY KEY (`id`),
  ADD KEY `txn_ref` (`txn_ref`),
  ADD KEY `ipay_id` (`ipay_id`),
  ADD KEY `ipay_id_2` (`ipay_id`,`txn_message`,`cc_type`);

--
-- Indexes for table `ipay_cc_txn_tbl_ke`
--
ALTER TABLE `ipay_cc_txn_tbl_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_cost_per_channel_ke`
--
ALTER TABLE `ipay_cost_per_channel_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_kcb_invoice_keys_ke`
--
ALTER TABLE `ipay_kcb_invoice_keys_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_kcb_key_ke`
--
ALTER TABLE `ipay_kcb_key_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr` (`curr`);

--
-- Indexes for table `ipay_kcb_key_test_ke`
--
ALTER TABLE `ipay_kcb_key_test_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr` (`curr`);

--
-- Indexes for table `ipay_ksw_active_orders_ke`
--
ALTER TABLE `ipay_ksw_active_orders_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_ksw_txn_tbl_ke`
--
ALTER TABLE `ipay_ksw_txn_tbl_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_malipo_ke`
--
ALTER TABLE `ipay_malipo_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_malipo_mail_ke`
--
ALTER TABLE `ipay_malipo_mail_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash` (`hash`);

--
-- Indexes for table `ipay_merchant_reg_ke`
--
ALTER TABLE `ipay_merchant_reg_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prefix_channel` (`merprefix`,`paybillchannel`),
  ADD KEY `merprefix` (`merprefix`),
  ADD KEY `salesperson` (`salesperson`),
  ADD KEY `created_at` (`created_at`);

--
-- Indexes for table `ipay_merchant_reg_ke_backup`
--
ALTER TABLE `ipay_merchant_reg_ke_backup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prefix_channel` (`merprefix`,`paybillchannel`);

--
-- Indexes for table `ipay_merchant_reports`
--
ALTER TABLE `ipay_merchant_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_online_callback_log_ke`
--
ALTER TABLE `ipay_online_callback_log_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_orders`
--
ALTER TABLE `ipay_orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Indexes for table `ipay_paybill_tarrif`
--
ALTER TABLE `ipay_paybill_tarrif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_pos_comm_txn_tbl`
--
ALTER TABLE `ipay_pos_comm_txn_tbl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `terminal` (`terminal`);

--
-- Indexes for table `ipay_pos_txn_tbl`
--
ALTER TABLE `ipay_pos_txn_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_pymnt_tbl_ke`
--
ALTER TABLE `ipay_pymnt_tbl_ke`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mer_prfx` (`mer_prfx`,`curr`);

--
-- Indexes for table `ipay_pymnt_tbl_ke_backup`
--
ALTER TABLE `ipay_pymnt_tbl_ke_backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mer_prfx` (`mer_prfx`,`curr`);

--
-- Indexes for table `ipay_pymnt_tbl_ke_new_backup`
--
ALTER TABLE `ipay_pymnt_tbl_ke_new_backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mer_prfx` (`mer_prfx`,`curr`);

--
-- Indexes for table `ipay_pymnt_tbl_mbg`
--
ALTER TABLE `ipay_pymnt_tbl_mbg`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mer_prfx` (`mer_prfx`),
  ADD UNIQUE KEY `mer_id` (`mer_id`);

--
-- Indexes for table `ipay_special_calc_ke`
--
ALTER TABLE `ipay_special_calc_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_stanbic_key_ke`
--
ALTER TABLE `ipay_stanbic_key_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr` (`curr`);

--
-- Indexes for table `ipay_supported_currencies_ke`
--
ALTER TABLE `ipay_supported_currencies_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr` (`curr`);

--
-- Indexes for table `ipay_supported_currencies_test_ke`
--
ALTER TABLE `ipay_supported_currencies_test_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curr` (`curr`);

--
-- Indexes for table `ipay_temp_bulkpay_vals_ke`
--
ALTER TABLE `ipay_temp_bulkpay_vals_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_thirdparty_loans`
--
ALTER TABLE `ipay_thirdparty_loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_txnlog`
--
ALTER TABLE `ipay_txnlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_autopay_ke`
--
ALTER TABLE `online_autopay_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_autopay_ke_orig`
--
ALTER TABLE `online_autopay_ke_orig`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_ipw_txns_ke`
--
ALTER TABLE `online_ipw_txns_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_ke`
--
ALTER TABLE `online_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tcode` (`tcode`),
  ADD KEY `receivingdatetime` (`receivingdatetime`),
  ADD KEY `vid` (`vid`),
  ADD KEY `tid` (`tid`);

--
-- Indexes for table `online_ke_06_17_backup`
--
ALTER TABLE `online_ke_06_17_backup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tcode` (`tcode`),
  ADD KEY `tid` (`tid`);

--
-- Indexes for table `online_ke_backup`
--
ALTER TABLE `online_ke_backup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tcode` (`tcode`);

--
-- Indexes for table `online_qr_ke`
--
ALTER TABLE `online_qr_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tid` (`tid`),
  ADD KEY `sid` (`sid`);

--
-- Indexes for table `online_sessions_deletelog_ke`
--
ALTER TABLE `online_sessions_deletelog_ke`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_sessions_ke`
--
ALTER TABLE `online_sessions_ke`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sid` (`sid`),
  ADD KEY `vid` (`vid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `oid` (`oid`,`phone`,`live`,`amount`);

--
-- Indexes for table `online_sessions_ke_07_05_backup`
--
ALTER TABLE `online_sessions_ke_07_05_backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sid` (`sid`),
  ADD KEY `tid` (`tid`);

--
-- Indexes for table `online_sessions_ke_2017`
--
ALTER TABLE `online_sessions_ke_2017`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sid` (`sid`),
  ADD KEY `vid` (`vid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `tcode` (`tcode`),
  ADD KEY `phone` (`phone`,`live`,`amount`);

--
-- Indexes for table `online_sessions_ke_backup`
--
ALTER TABLE `online_sessions_ke_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_sessions_ke_test`
--
ALTER TABLE `online_sessions_ke_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operatesttbl`
--
ALTER TABLE `operatesttbl`
  ADD UNIQUE KEY `txnid` (`txnid`);

--
-- Indexes for table `operatesttbl_old`
--
ALTER TABLE `operatesttbl_old`
  ADD PRIMARY KEY (`txnid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ipay_b2b_ke`
--
ALTER TABLE `ipay_b2b_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `ipay_billing_mer_reg_ke`
--
ALTER TABLE `ipay_billing_mer_reg_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_blacklist_iin_ke`
--
ALTER TABLE `ipay_blacklist_iin_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_blacklist_ip_ke`
--
ALTER TABLE `ipay_blacklist_ip_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_blacklist_msisdn_ke`
--
ALTER TABLE `ipay_blacklist_msisdn_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_bulkpay_bo_ke`
--
ALTER TABLE `ipay_bulkpay_bo_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_bulkpay_bo_ke_test`
--
ALTER TABLE `ipay_bulkpay_bo_ke_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_cc_blacklist_usr_ke`
--
ALTER TABLE `ipay_cc_blacklist_usr_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1137;
--
-- AUTO_INCREMENT for table `ipay_cc_failed_txn_usr_details_ke`
--
ALTER TABLE `ipay_cc_failed_txn_usr_details_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_cc_kcb_txn_tbl_ke`
--
ALTER TABLE `ipay_cc_kcb_txn_tbl_ke`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_cc_txn_tbl_ke`
--
ALTER TABLE `ipay_cc_txn_tbl_ke`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_cost_per_channel_ke`
--
ALTER TABLE `ipay_cost_per_channel_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ipay_kcb_invoice_keys_ke`
--
ALTER TABLE `ipay_kcb_invoice_keys_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ipay_kcb_key_ke`
--
ALTER TABLE `ipay_kcb_key_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ipay_kcb_key_test_ke`
--
ALTER TABLE `ipay_kcb_key_test_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ipay_ksw_active_orders_ke`
--
ALTER TABLE `ipay_ksw_active_orders_ke`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_ksw_txn_tbl_ke`
--
ALTER TABLE `ipay_ksw_txn_tbl_ke`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_malipo_ke`
--
ALTER TABLE `ipay_malipo_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_malipo_mail_ke`
--
ALTER TABLE `ipay_malipo_mail_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_merchant_reg_ke`
--
ALTER TABLE `ipay_merchant_reg_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4422;
--
-- AUTO_INCREMENT for table `ipay_merchant_reg_ke_backup`
--
ALTER TABLE `ipay_merchant_reg_ke_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3570;
--
-- AUTO_INCREMENT for table `ipay_merchant_reports`
--
ALTER TABLE `ipay_merchant_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_online_callback_log_ke`
--
ALTER TABLE `ipay_online_callback_log_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=712822;
--
-- AUTO_INCREMENT for table `ipay_orders`
--
ALTER TABLE `ipay_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=584763;
--
-- AUTO_INCREMENT for table `ipay_paybill_tarrif`
--
ALTER TABLE `ipay_paybill_tarrif`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ipay_pos_comm_txn_tbl`
--
ALTER TABLE `ipay_pos_comm_txn_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_pos_txn_tbl`
--
ALTER TABLE `ipay_pos_txn_tbl`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_pymnt_tbl_ke`
--
ALTER TABLE `ipay_pymnt_tbl_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ipay_pymnt_tbl_ke_backup`
--
ALTER TABLE `ipay_pymnt_tbl_ke_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_pymnt_tbl_ke_new_backup`
--
ALTER TABLE `ipay_pymnt_tbl_ke_new_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_pymnt_tbl_mbg`
--
ALTER TABLE `ipay_pymnt_tbl_mbg`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_special_calc_ke`
--
ALTER TABLE `ipay_special_calc_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ipay_stanbic_key_ke`
--
ALTER TABLE `ipay_stanbic_key_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_supported_currencies_ke`
--
ALTER TABLE `ipay_supported_currencies_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ipay_supported_currencies_test_ke`
--
ALTER TABLE `ipay_supported_currencies_test_ke`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ipay_temp_bulkpay_vals_ke`
--
ALTER TABLE `ipay_temp_bulkpay_vals_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `ipay_thirdparty_loans`
--
ALTER TABLE `ipay_thirdparty_loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_txnlog`
--
ALTER TABLE `ipay_txnlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_autopay_ke`
--
ALTER TABLE `online_autopay_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_autopay_ke_orig`
--
ALTER TABLE `online_autopay_ke_orig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_ipw_txns_ke`
--
ALTER TABLE `online_ipw_txns_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_ke`
--
ALTER TABLE `online_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `online_ke_06_17_backup`
--
ALTER TABLE `online_ke_06_17_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_ke_backup`
--
ALTER TABLE `online_ke_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_qr_ke`
--
ALTER TABLE `online_qr_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_deletelog_ke`
--
ALTER TABLE `online_sessions_deletelog_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_ke`
--
ALTER TABLE `online_sessions_ke`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_ke_07_05_backup`
--
ALTER TABLE `online_sessions_ke_07_05_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_ke_2017`
--
ALTER TABLE `online_sessions_ke_2017`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_ke_backup`
--
ALTER TABLE `online_sessions_ke_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `online_sessions_ke_test`
--
ALTER TABLE `online_sessions_ke_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
