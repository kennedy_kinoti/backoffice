-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 25, 2018 at 02:41 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `micstation_ipaybackoffice`
--

-- --------------------------------------------------------

--
-- Table structure for table `guzzle_posts`
--

CREATE TABLE `guzzle_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guzzle_posts`
--

INSERT INTO `guzzle_posts` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ken', '2018-09-30 06:44:35', '2018-09-30 06:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_bulkpay_status`
--

CREATE TABLE `ipay_bulkpay_status` (
  `id` int(11) NOT NULL,
  `ref` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL,
  `vid` varchar(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_country`
--

CREATE TABLE `ipay_country` (
  `id` int(11) NOT NULL,
  `country_name` varchar(50) NOT NULL,
  `currency_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_invoices`
--

CREATE TABLE `ipay_invoices` (
  `id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `names` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phonenumber` varchar(50) NOT NULL,
  `invoice_number` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `amount` int(11) NOT NULL,
  `itinerary` varchar(500) NOT NULL,
  `state` int(11) NOT NULL COMMENT '0 for pending 1 confirm 2 reject',
  `token` varchar(500) NOT NULL,
  `proformainvoice` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipay_invoices`
--

INSERT INTO `ipay_invoices` (`id`, `date`, `vendor_id`, `names`, `email`, `phonenumber`, `invoice_number`, `order_id`, `currency`, `amount`, `itinerary`, `state`, `token`, `proformainvoice`) VALUES
(1, '2018-07-12 19:38:13', '0000', 'joshua', 'kiseej@gmail.com', '0705118708', '2pqy31', 'IN2753DB', 'USD', 100, 'demo payment', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNDc4NDExNDY5Y2QiLCJpYXQiOjE1MzE0MTM1MjEsIm5iZiI6MTUzMTQxMzUyMSwiZXhwIjoxNTMxNDE3MTIxLCJkYXRhIjp7InJlZiI6IklOMjc1M0RCIiwidmlkIjoiMDAwMCJ9fQ.f-qZXPWra_GnczSQlBdzt2pS1KxE5zgLnmJ_AEDU8JA', 0),
(2, '2018-07-17 07:16:21', '0000', 'moses', 'kiseej@gmail.com', '0705118708', 'xwc7an', 'IN33859C', 'KES', 1000, 'pay without delay', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNGQ5MWMyNmU2MWUiLCJpYXQiOjE1MzE4MTAyNDIsIm5iZiI6MTUzMTgxMDI0MiwiZXhwIjoxNTMxODEzODQyLCJkYXRhIjp7InJlZiI6IklOMzM4NTlDIiwidmlkIjoiMDAwMCJ9fQ.fcQTBeC39rjErB13bLTMNK61X41GrK4B3ipowvouG6k', 0),
(3, '2018-07-17 09:49:23', '0000', 'moses', 'kiseej@gmail.com', '0705118708', 'xwc7an', 'IN5F9F32', 'KES', 1000, 'pay without delay', 2, '0', 0),
(4, '2018-07-17 11:39:48', '0000', 'Peter njuguna', 'peter@gmail.c0m', '0725049683', 'goq26l', 'INDD811F', 'KES', 200, 'yttytt', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNGRhYmJkMWNlNGUiLCJpYXQiOjE1MzE4MTY4OTMsIm5iZiI6MTUzMTgxNjg5MywiZXhwIjoxNTMxODIwNDkzLCJkYXRhIjp7InJlZiI6IklOREQ4MTFGIiwidmlkIjoiMDAwMCJ9fQ.gyPM1dD6trwRtV99KnV51DsIXAvnh0D6TcxRHbVYDLM', 0),
(5, '2018-07-17 12:00:14', '0000', 'dddd', 'ttttt@ttt.tt', '0708254213', '5ar2tj', 'IN08BB1B', 'KES', 10, 'tttttt', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5ODE0YzU5MWQiLCJpYXQiOjE1MzIwNzQwMDQsIm5iZiI6MTUzMjA3NDAwNCwiZXhwIjoxNTMyMDc3NjA0LCJkYXRhIjp7InJlZiI6IklOMDhCQjFCIiwidmlkIjoiMDAwMCJ9fQ.viy1llGZ8vyYpqQRfTheCTdb8ZnV4wUO1zc51ypR47M', 0),
(6, '2018-07-20 10:50:25', '0000', 'joshua', 'kiseej@gmail.com', '0705118708', '0705118708', 'IN57FCC3', 'KES', 100, 'weeer', 2, '0', 0),
(7, '2018-07-20 11:08:45', '0000', 'Joshua testing var', 'kiseej@gmail.com', '0705118708', '3w04ca', 'INA1A220', 'KES', 1000, 'demo payment', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OGJkMzE1NzUiLCJpYXQiOjE1MzIwNzQxNzMsIm5iZiI6MTUzMjA3NDE3MywiZXhwIjoxNTMyMDc3NzczLCJkYXRhIjp7InJlZiI6IklOQTFBMjIwIiwidmlkIjoiMDAwMCJ9fQ.qu7TsSvrRiQmWOd-TTNABjOImybFWURaRKEfhsbApho', 0),
(8, '2018-07-20 11:12:01', '0000', 'moses', 'kisee@ipayafrica.com', '0705118708', 'qes3uc', 'IN000A02', 'KES', 123, 'demo payment now', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OTc5ZDMyMWMiLCJpYXQiOjE1MzIwNzQzNjEsIm5iZiI6MTUzMjA3NDM2MSwiZXhwIjoxNTMyMDc3OTYxLCJkYXRhIjp7InJlZiI6IklOMDAwQTAyIiwidmlkIjoiMDAwMCJ9fQ.q5BuocYVKSz2UMthBsgp2GWvk1z_ei9aqHM3o2yT3lY', 0),
(9, '2018-07-04 12:26:39', '0000', 'Kennedy Kinoti Mbabu', 'kinotikennedy@gmail.com', '0715916968', '11874-37B659', '000A', 'usd', 2350, 'Test 1', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OTc5ZDMyMWMiLCJpYXQiOjE1MzIwNzQzNjEsIm5iZiI6MTUzMjA3NDM2MSwiZXhwIjoxNTMyMDc3OTYxLCJkYXRhIjp7InJlZiI6IklOMDAwQTAyIiwidmlkIjoiMDAwMCJ9fQ.q5BuocYVKSz2UMthBsgp2GWvk1z_ei9aqHM3o2yT3lY', 0),
(10, '2018-07-04 12:26:39', '0000', 'Kennedy Kinoti Mbabu', 'kinotikennedy@gmail.com', '0715916968', '11874-37B659', 'IN000A03', 'usd', 2350, 'sample', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OTc5ZDMyMWMiLCJpYXQiOjE1MzIwNzQzNjEsIm5iZiI6MTUzMjA3NDM2MSwiZXhwIjoxNTMyMDc3OTYxLCJkYXRhIjp7InJlZiI6IklOMDAwQTAyIiwidmlkIjoiMDAwMCJ9fQ.q5BuocYVKSz2UMthBsgp2GWvk1z_ei9aqHM3o2yT3lY', 0),
(11, '2018-07-04 12:26:39', '0000', 'James Wathiga', 'andrew.ouko.254@gmail.com', '0715916968', '11874-37B659', 'IN000A03', 'USD', 2150, 'Pending project deployment', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OTc5ZDMyMWMiLCJpYXQiOjE1MzIwNzQzNjEsIm5iZiI6MTUzMjA3NDM2MSwiZXhwIjoxNTMyMDc3OTYxLCJkYXRhIjp7InJlZiI6IklOMDAwQTAyIiwidmlkIjoiMDAwMCJ9fQ.q5BuocYVKSz2UMthBsgp2GWvk1z_ei9aqHM3o2yT3lY', 0),
(12, '2018-07-04 12:26:39', '0000', 'Kennedy Kinoti Mbabu', 'kinotikennedy@gmail.com', '0715916968', '11874-37B658', 'IN000A03', 'KES', 5000, 'Sample mail', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOlsiaXBheS1zdGFnaW5nLmlwYXlhZnJpY2EuY29tIl0sImp0aSI6IjViNTE5OTc5ZDMyMWMiLCJpYXQiOjE1MzIwNzQzNjEsIm5iZiI6MTUzMjA3NDM2MSwiZXhwIjoxNTMyMDc3OTYxLCJkYXRhIjp7InJlZiI6IklOMDAwQTAyIiwidmlkIjoiMDAwMCJ9fQ.q5BuocYVKSz2UMthBsgp2GWvk1z_ei9aqHM3o2yT3lY', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ipay_invoice_storeprofile`
--

CREATE TABLE `ipay_invoice_storeprofile` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `slogan` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_website` varchar(100) NOT NULL,
  `company_telno` varchar(100) NOT NULL,
  `company_postal_add` varchar(100) NOT NULL,
  `company_physical_add` varchar(100) NOT NULL,
  `logo_path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_login_log`
--

CREATE TABLE `ipay_login_log` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `vendor_id` varchar(35) NOT NULL,
  `attempt_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipay_login_log`
--

INSERT INTO `ipay_login_log` (`id`, `ip_address`, `vendor_id`, `attempt_time`) VALUES
(1, '41.212.60.154', 'android', '2018-07-11 10:34:31'),
(2, '41.212.60.154', 'android', '2018-07-11 12:49:18'),
(3, '41.212.60.154', 'android', '2018-07-11 12:50:22');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_logs`
--

CREATE TABLE `ipay_logs` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` datetime NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` datetime NOT NULL,
  `state` int(11) NOT NULL COMMENT '0 for pending 1 confirm 2 reject',
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(100) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `reference_details` varchar(255) NOT NULL,
  `country` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipay_logs`
--

INSERT INTO `ipay_logs` (`id`, `initiate_id`, `initiate_date`, `completor_id`, `complete_date`, `state`, `reference_type`, `reference_id`, `vendor_id`, `reference_details`, `country`) VALUES
(1, 3, '2018-07-12 11:13:01', 98, '2018-07-01 12:26:39', 1, 'refund', '12ertgsssa8', '0000', '{\"id\":\"41\",\"fname\":\"joshua\",\"lname\":\"kisee\",\"txnamt\":\"20.00\",\"sendernumber\":\"MPESA\",\"curr\":\"KES\",\"receivingdatetime\":\"2018-05-01 00:00:00\"}', 'KE'),
(2, 3, '2018-07-12 11:26:23', 4, '2018-07-12 00:00:00', 1, 'refund', '12ertyusss12', '0000', '{\"id\":\"45\",\"fname\":\"joshua\",\"lname\":\"kisee\",\"txnamt\":\"10000.00\",\"sendernumber\":\"VISA\",\"curr\":\"USD\",\"receivingdatetime\":\"2018-07-12 00:00:00\"}', 'KE'),
(3, 3, '2018-07-12 11:27:27', 98, '2018-07-01 12:26:39', 1, 'refund', 'rsdrrrhhh13', '0000', '{\"id\":\"46\",\"fname\":\"joshua\",\"lname\":\"kisee\",\"txnamt\":\"30.00\",\"sendernumber\":\"VISA\",\"curr\":\"USD\",\"receivingdatetime\":\"2018-07-11 00:00:00\"}', 'KE'),
(4, 2, '2018-07-12 12:53:06', 4, '2018-07-12 00:00:00', 1, 'refund', 'rsdrrrhhhlg4', '0000', '{\"id\":\"37\",\"fname\":\"joshua\",\"lname\":\"kisee\",\"txnamt\":\"20.00\",\"sendernumber\":\"Mpesa\",\"curr\":\"KES\",\"receivingdatetime\":\"2018-05-01 00:00:00\"}', 'KE'),
(5, 4, '2018-07-12 19:38:13', 2, '2018-07-12 00:00:00', 1, 'invoice', 'IN2753DB', '0000', '', 'KE'),
(6, 2, '2018-07-17 07:16:21', 3, '2018-07-17 00:00:00', 1, 'invoice', 'IN33859C', '0000', '', 'KE'),
(7, 2, '2018-07-17 09:49:23', 3, '2018-07-17 00:00:00', 2, 'invoice', 'IN5F9F32', '0000', '', 'KE'),
(8, 2, '2018-07-17 11:39:48', 3, '2018-07-17 00:00:00', 1, 'invoice', 'INDD811F', '0000', '', 'KE'),
(9, 3, '2018-07-17 12:00:14', 2, '2018-07-20 00:00:00', 1, 'invoice', 'IN08BB1B', '0000', '', 'KE'),
(10, 3, '2018-07-17 12:29:17', 2, '2018-07-17 00:00:00', 1, 'refund', '12ertyuss9', '0000', '{\"id\":\"42\",\"fname\":\"joshua\",\"lname\":\"kisee\",\"txnamt\":\"20.00\",\"sendernumber\":\"MPESA\",\"curr\":\"KES\",\"receivingdatetime\":\"2018-07-12 00:00:00\"}', 'KE'),
(11, 2, '2018-07-20 10:50:25', 3, '2018-07-20 00:00:00', 2, 'invoice', 'IN57FCC3', '0000', '', 'KE'),
(12, 2, '2018-07-20 11:08:45', 3, '2018-07-20 00:00:00', 1, 'invoice', 'INA1A220', '0000', '', 'KE'),
(13, 2, '2018-07-20 11:12:01', 3, '2018-07-20 00:00:00', 1, 'invoice', 'IN000A02', '0000', '', 'KE'),
(14, 0, '2018-08-27 14:27:17', 98, '2018-07-01 12:26:39', 1, 'refund', 'rsdrrrhhh13', '000', '', 'KE'),
(15, 0, '2018-09-11 13:38:37', 98, '2018-07-01 12:26:39', 1, 'refund', 'WY56346HD', '', '', 'KE'),
(16, 0, '2018-09-27 12:05:03', 98, '2018-07-01 12:26:39', 1, 'refund', 'MFQ41H5XW6', '', '', 'KE'),
(17, 0, '2018-09-27 12:07:12', 98, '2018-07-01 12:26:39', 1, 'refund', 'WY56346HD', '', '', 'KE'),
(18, 0, '2018-09-28 16:19:08', 98, '2018-07-01 12:26:39', 1, 'refund', 'WY56346HD', '', '', 'KE'),
(19, 0, '2018-10-03 11:24:32', 98, '2018-07-01 12:26:39', 1, 'refund', 'KK123456', '0000', '', 'KE'),
(20, 0, '2018-10-04 14:35:07', 98, '2018-07-01 12:26:39', 1, 'refund', 'MK123456', '0000', '', 'KE'),
(21, 0, '2018-10-04 14:36:22', 98, '2018-07-01 12:26:39', 1, 'refund', 'MK123456', '0000', '', 'KE'),
(22, 0, '2018-10-17 12:11:36', 98, '2018-07-01 12:26:39', 1, 'refund', 'MK123456', '0000', '', 'KE'),
(23, 0, '2018-10-17 12:11:45', 98, '2018-07-01 12:26:39', 1, 'refund', 'MK123456', '0000', '', 'KE'),
(24, 0, '2018-10-17 12:13:34', 98, '2018-07-01 12:26:39', 1, 'refund', 'MK123456', '0000', '', 'KE'),
(25, 0, '2018-10-17 12:14:19', 98, '2018-07-01 12:26:39', 1, 'refund', '12yees12', '000', '', 'KE'),
(26, 0, '2018-10-17 12:15:00', 98, '2018-07-01 12:26:39', 1, 'refund', '12ertyusssaj6', '000', '', 'KE'),
(27, 0, '2018-10-17 12:16:08', 98, '2018-07-01 12:26:39', 1, 'refund', '12ertyusssaj6', '000', '', 'KE'),
(28, 0, '2018-10-17 12:16:16', 98, '2018-07-01 12:26:39', 1, 'refund', '12yees12', '000', '', 'KE'),
(29, 0, '2018-10-17 12:48:50', 98, '2018-07-01 12:26:39', 1, 'refund', '12ertyusssaj6', '000', '', 'KE'),
(30, 0, '2018-10-17 13:07:46', 98, '2018-07-01 12:26:39', 2, 'refund', 'MTJ123456', '0000', '', 'KE'),
(31, 0, '2018-10-17 13:07:58', 98, '2018-07-01 12:26:39', 2, 'refund', 'MTJ123456', '0000', '', 'KE'),
(32, 0, '2018-10-17 14:31:32', 98, '2018-07-01 12:26:39', 2, 'refund', 'MTJ123456', '0000', '', 'KE'),
(33, 0, '2018-10-17 14:34:07', 98, '2018-07-01 12:26:39', 2, 'refund', 'MTJ123456', '0000', '', 'KE'),
(34, 0, '2018-10-17 14:39:29', 98, '2018-07-01 12:26:39', 2, 'refund', 'MTJ123456', '0000', '', 'KE'),
(35, 0, '2018-10-17 14:57:57', 98, '2018-07-01 12:26:39', 2, 'refund', 'MK123456A', '0000', '', 'KE'),
(36, 0, '2018-10-17 16:57:21', 0, '2018-07-01 12:26:39', 0, 'refund', 'WWW1231', '0000', '', 'KE'),
(37, 0, '2018-10-17 16:57:37', 0, '2018-07-01 12:26:39', 0, 'refund', 'AK47', '0000', '', 'KE');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_logs_new`
--

CREATE TABLE `ipay_logs_new` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` datetime NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` datetime NOT NULL,
  `state` int(11) NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(100) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `reference_details` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_logs_tests`
--

CREATE TABLE `ipay_logs_tests` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` datetime NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` datetime NOT NULL,
  `state` int(11) NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(100) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `reference_details` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_logs_tz`
--

CREATE TABLE `ipay_logs_tz` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` date NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` date NOT NULL,
  `state` int(11) NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(70) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `reference_details` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_optout`
--

CREATE TABLE `ipay_optout` (
  `id` int(11) NOT NULL,
  `phonenumber` varchar(16) NOT NULL,
  `senderid` varchar(16) NOT NULL,
  `updatedindb` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ipay_users`
--

CREATE TABLE `ipay_users` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `sub_account` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `views_parameters` varchar(1000) NOT NULL,
  `login_device` int(10) NOT NULL DEFAULT '3' COMMENT '0-none 1-web 2-app 3-all',
  `app_trans_view` int(10) NOT NULL DEFAULT '1' COMMENT '1-see_all 2-see_mine',
  `online_state` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `main_account` int(11) NOT NULL,
  `user_database` varchar(50) NOT NULL,
  `currency` varchar(10) NOT NULL DEFAULT 'kes',
  `country_code` varchar(5) NOT NULL,
  `currentstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastloggedin` datetime NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipay_users`
--

INSERT INTO `ipay_users` (`id`, `vendor_id`, `sub_account`, `name`, `email`, `username`, `password`, `views_parameters`, `login_device`, `app_trans_view`, `online_state`, `state`, `main_account`, `user_database`, `currency`, `country_code`, `currentstamp`, `lastloggedin`, `remember_token`) VALUES
(1, '0000', '0000', 'Technical', 'technical@ipayafrica.com', 'technical', '2840180493ff3073dc8ed330c972437d', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":\"1\",\"add_users\":\"1\",\"manage_users\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":\"1\",\"online_initiate_ref\":\"1\",\"online_complete_ref\":\"1\",\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":\"1\",\"confirm_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"fund_bulkpay\":\"1\",\"bank_remittance\":\"1\",\"device_web\":\"1\",\"device_app\":\"1\",\"device_all\":null}', 3, 1, 1, 1, 1, 'micstation_ipay_mer_ke', 'kes', 'ke', '2018-05-14 13:02:32', '2018-10-11 17:18:49', NULL),
(2, '0000', '', 'Demo', 'kiseej@gmail.com', 'demo', '908f8d317c5369215a993d749d645232', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":\"1\",\"add_users\":\"1\",\"manage_users\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":\"1\",\"online_initiate_ref\":\"1\",\"online_complete_ref\":\"1\",\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":\"1\",\"confirm_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"fund_bulkpay\":\"1\",\"bank_remittance\":\"1\",\"device_web\":\"1\",\"device_app\":null,\"device_all\":null}', 3, 2, 1, 0, 0, 'micstation_ipay_mer_ke', 'kes', 'ke', '2018-05-18 09:07:33', '2018-10-12 13:38:49', NULL),
(3, '0000', '0000', 'moses', 'moseskariuki2119@gmail.com ', 'moses', 'fe01ce2a7fbac8fafaed7c982a04e229', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":\"1\",\"initiate_invoices\":\"\",\"add_users\":\"1\",\"manage_users\":\"1\",\"fund_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"confirm_bulkpay\":\"1\",\"bank_remittance\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":\"1\",\"online_initiate_ref\":\"1\",\"online_complete_ref\":\"1\",\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":\"1\"}', 3, 1, 1, 1, 0, 'micstation_ipay_mer_ke', 'kes', 'ke', '2018-05-18 09:07:33', '2018-05-02 00:00:00', NULL),
(4, '0000', '001', 'joshua', 'kisee@ipayafrica.com', 'android', 'fe01ce2a7fbac8fafaed7c982a04e229', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":\"1\",\"initiate_invoices\":\"\",\"add_users\":\"1\",\"manage_users\":\"1\",\"fund_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"confirm_bulkpay\":\"1\",\"bank_remittance\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":\"1\",\"online_initiate_ref\":\"1\",\"online_complete_ref\":\"1\",\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":\"1\"}', 3, 1, 1, 1, 0, 'micstation_ipay_mer_ke', 'kes', 'ke', '2018-05-18 09:07:33', '2018-05-02 00:00:00', NULL),
(5, '0000', '001', 'Kennedy Kinoti', 'kinotikennedy@gmail.com', 'android', '$2y$10$30vqjF354Xweox221rEx6unldADGsYFKxupGFi9MYG/cunSK/bL9a', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":\"1\",\"add_users\":\"1\",\"manage_users\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":\"1\",\"online_initiate_ref\":\"1\",\"online_complete_ref\":\"1\",\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":\"1\",\"confirm_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"fund_bulkpay\":\"1\",\"bank_remittance\":\"1\",\"device_web\":null,\"device_app\":null,\"device_all\":\"1\"}', 3, 1, 1, 1, 0, 'micstation_ipay_mer_ke', 'kes', 'ke', '2018-05-18 09:07:33', '2018-10-11 16:35:54', 'TnfqpwR8Vijwvg0SbMZ4jfBptJ4zGIdDywqtMzIm7rTmOO8524ugoowdt62X'),
(6, '0000', '001', 'Lucy', 'lucy@ipayafrica.com', 'lucy', 'lucy123', '{\"create_invoices\":\"1\",\"manage_invoices\":\"1\",\"confirm_invoices\":null,\"add_users\":\"1\",\"manage_users\":\"1\",\"online_success\":\"1\",\"online_suspense\":\"1\",\"online_failed\":null,\"online_initiate_ref\":null,\"online_complete_ref\":null,\"pos_success\":\"1\",\"pos_initiate_ref\":\"1\",\"pos_complete_ref\":null,\"confirm_bulkpay\":\"1\",\"init_bulkpay\":\"1\",\"fund_bulkpay\":null,\"bank_remittance\":\"1\",\"device_web\":\"1\",\"device_app\":null,\"device_all\":null}', 3, 1, 1, 1, 0, 'micstation_ipay_mer_ke', 'KES', 'KE', '2018-10-12 10:04:42', '2018-10-17 14:41:55', ''),
(7, '0000', '001', 'Arnold', 'kennedy@ipayafrica.com', 'arnold', '$2y$10$d4gv9Y0PzRdSA.fE3gAh3eKArSxOJ5.ZWoDzJ5CTSI2DdYLE8WMF.', '{\"create_invoices\":\"1\",\"manage_invoices\":null,\"confirm_invoices\":null,\"add_users\":null,\"manage_users\":\"1\",\"online_success\":null,\"online_suspense\":null,\"online_failed\":null,\"online_initiate_ref\":\"1\",\"online_complete_ref\":null,\"pos_success\":\"1\",\"pos_initiate_ref\":null,\"pos_complete_ref\":null,\"confirm_bulkpay\":null,\"init_bulkpay\":null,\"fund_bulkpay\":null,\"bank_remittance\":\"1\",\"device_web\":null,\"device_app\":null,\"device_all\":null}', 3, 1, 1, 0, 0, 'micstation_ipay_mer_ke', 'KES', 'KE', '2018-10-17 07:23:18', '2018-10-17 10:47:40', '1NtCvEQrIVx6Bfc8d9hGlqHPAJnMeNQAzX07CnrmW0bUXjKmK02mXIE8btGa');

-- --------------------------------------------------------

--
-- Table structure for table `ipay_users_back`
--

CREATE TABLE `ipay_users_back` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `views_parameters` varchar(1000) NOT NULL,
  `online_state` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `main_account` int(11) NOT NULL,
  `user_database` varchar(50) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `currentstamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastloggedin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_invoices`
--

CREATE TABLE `kcb_invoices` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `payment_details` text NOT NULL,
  `primary_notification_address` varchar(50) NOT NULL,
  `secondary_notification_address` varchar(50) NOT NULL,
  `phonenumber` varchar(20) NOT NULL,
  `reference_number` varchar(50) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `paid_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `token` varchar(40) NOT NULL,
  `vendor` varchar(20) NOT NULL,
  `date_of_travel` date NOT NULL,
  `txn_reference` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_invoice_storeprofile`
--

CREATE TABLE `kcb_invoice_storeprofile` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `slogan` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_website` varchar(100) NOT NULL,
  `company_telno` varchar(100) NOT NULL,
  `company_postal_add` varchar(100) NOT NULL,
  `company_physical_add` varchar(100) NOT NULL,
  `logo_path` varchar(100) NOT NULL,
  `successful_email` varchar(150) NOT NULL,
  `failed_email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_login_log`
--

CREATE TABLE `kcb_login_log` (
  `id` int(11) NOT NULL,
  `attempt_time` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `vendor_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_logs`
--

CREATE TABLE `kcb_logs` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` date NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` date NOT NULL,
  `state` int(11) NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(70) NOT NULL,
  `vendor_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_sweeptbl`
--

CREATE TABLE `kcb_sweeptbl` (
  `id` int(11) NOT NULL,
  `vid` varchar(20) NOT NULL,
  `serial_number` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_templates`
--

CREATE TABLE `kcb_templates` (
  `id` int(11) NOT NULL,
  `vendor` varchar(15) NOT NULL,
  `template_type` int(11) NOT NULL,
  `template_body` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kcb_users`
--

CREATE TABLE `kcb_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `names` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `params` text NOT NULL,
  `state` int(11) NOT NULL,
  `main_account` int(11) NOT NULL,
  `vendor` varchar(20) NOT NULL,
  `database` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2018_09_12_123457_create_guzzle_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('kinotikennedy@gmail.com', '$2y$10$S8FaQBbYnmqyMxHUMrvyNeCtN6OWL2HFRE0op8IWLTyh3lrQJ6nBa', '2018-10-12 07:03:47'),
('lucy@ipayafrica.com', '$2y$10$gIQm.oyR9AY/1/wg1cy3N.5y6Kx6NCVq7hr2iuK9ZHck2HxQYontm', '2018-10-17 11:43:42'),
('kennedy@ipayafrica.com', '$2y$10$lDWIKAx0UcOc4wm85YxYsOmnrGuGUU8uwtS/r0ChTEXlGTQPZlKl.', '2018-10-17 11:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_invoices`
--

CREATE TABLE `stanbic_invoices` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `payment_details` text NOT NULL,
  `primary_notification_address` varchar(50) NOT NULL,
  `secondary_notification_address` varchar(50) NOT NULL,
  `phonenumber` varchar(20) NOT NULL,
  `reference_number` varchar(50) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `paid_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `token` varchar(40) NOT NULL,
  `vendor` varchar(20) NOT NULL,
  `date_of_travel` date NOT NULL,
  `txn_reference` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_invoice_storeprofile`
--

CREATE TABLE `stanbic_invoice_storeprofile` (
  `id` int(11) NOT NULL,
  `vendor_id` varchar(20) NOT NULL,
  `slogan` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_website` varchar(100) NOT NULL,
  `company_telno` varchar(100) NOT NULL,
  `company_postal_add` varchar(100) NOT NULL,
  `company_physical_add` varchar(100) NOT NULL,
  `logo_path` varchar(100) NOT NULL,
  `successful_email` varchar(150) NOT NULL,
  `failed_email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_login_log`
--

CREATE TABLE `stanbic_login_log` (
  `id` int(11) NOT NULL,
  `attempt_time` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `vendor_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_logs`
--

CREATE TABLE `stanbic_logs` (
  `id` int(11) NOT NULL,
  `initiate_id` int(11) NOT NULL,
  `initiate_date` date NOT NULL,
  `completor_id` int(11) NOT NULL,
  `complete_date` date NOT NULL,
  `state` int(11) NOT NULL,
  `reference_type` varchar(20) NOT NULL,
  `reference_id` varchar(70) NOT NULL,
  `vendor_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_templates`
--

CREATE TABLE `stanbic_templates` (
  `id` int(11) NOT NULL,
  `vendor` varchar(10) NOT NULL,
  `template_type` int(11) NOT NULL,
  `template_body` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stanbic_users`
--

CREATE TABLE `stanbic_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `names` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `params` text NOT NULL,
  `state` int(11) NOT NULL,
  `main_account` int(11) NOT NULL,
  `vendor` varchar(20) NOT NULL,
  `database` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guzzle_posts`
--
ALTER TABLE `guzzle_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_bulkpay_status`
--
ALTER TABLE `ipay_bulkpay_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_country`
--
ALTER TABLE `ipay_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_invoices`
--
ALTER TABLE `ipay_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_invoice_storeprofile`
--
ALTER TABLE `ipay_invoice_storeprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_login_log`
--
ALTER TABLE `ipay_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_logs`
--
ALTER TABLE `ipay_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `reference_id` (`reference_id`),
  ADD KEY `initiate_id` (`initiate_id`),
  ADD KEY `completor_id` (`completor_id`);

--
-- Indexes for table `ipay_logs_new`
--
ALTER TABLE `ipay_logs_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_logs_tests`
--
ALTER TABLE `ipay_logs_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_logs_tz`
--
ALTER TABLE `ipay_logs_tz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_optout`
--
ALTER TABLE `ipay_optout`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phonenumber` (`phonenumber`);

--
-- Indexes for table `ipay_users`
--
ALTER TABLE `ipay_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipay_users_back`
--
ALTER TABLE `ipay_users_back`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_invoices`
--
ALTER TABLE `kcb_invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference_number` (`reference_number`);

--
-- Indexes for table `kcb_invoice_storeprofile`
--
ALTER TABLE `kcb_invoice_storeprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_login_log`
--
ALTER TABLE `kcb_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_logs`
--
ALTER TABLE `kcb_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_sweeptbl`
--
ALTER TABLE `kcb_sweeptbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_templates`
--
ALTER TABLE `kcb_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kcb_users`
--
ALTER TABLE `kcb_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `stanbic_invoices`
--
ALTER TABLE `stanbic_invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reference_number` (`reference_number`);

--
-- Indexes for table `stanbic_invoice_storeprofile`
--
ALTER TABLE `stanbic_invoice_storeprofile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stanbic_login_log`
--
ALTER TABLE `stanbic_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stanbic_logs`
--
ALTER TABLE `stanbic_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stanbic_templates`
--
ALTER TABLE `stanbic_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stanbic_users`
--
ALTER TABLE `stanbic_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guzzle_posts`
--
ALTER TABLE `guzzle_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ipay_bulkpay_status`
--
ALTER TABLE `ipay_bulkpay_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_country`
--
ALTER TABLE `ipay_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_invoices`
--
ALTER TABLE `ipay_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ipay_invoice_storeprofile`
--
ALTER TABLE `ipay_invoice_storeprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_login_log`
--
ALTER TABLE `ipay_login_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ipay_logs`
--
ALTER TABLE `ipay_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `ipay_logs_new`
--
ALTER TABLE `ipay_logs_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_logs_tests`
--
ALTER TABLE `ipay_logs_tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_logs_tz`
--
ALTER TABLE `ipay_logs_tz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ipay_optout`
--
ALTER TABLE `ipay_optout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT for table `ipay_users`
--
ALTER TABLE `ipay_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ipay_users_back`
--
ALTER TABLE `ipay_users_back`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kcb_invoices`
--
ALTER TABLE `kcb_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kcb_invoice_storeprofile`
--
ALTER TABLE `kcb_invoice_storeprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kcb_login_log`
--
ALTER TABLE `kcb_login_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kcb_logs`
--
ALTER TABLE `kcb_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kcb_sweeptbl`
--
ALTER TABLE `kcb_sweeptbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kcb_templates`
--
ALTER TABLE `kcb_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `kcb_users`
--
ALTER TABLE `kcb_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `stanbic_invoices`
--
ALTER TABLE `stanbic_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stanbic_invoice_storeprofile`
--
ALTER TABLE `stanbic_invoice_storeprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stanbic_login_log`
--
ALTER TABLE `stanbic_login_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stanbic_logs`
--
ALTER TABLE `stanbic_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stanbic_templates`
--
ALTER TABLE `stanbic_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `stanbic_users`
--
ALTER TABLE `stanbic_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
