<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Bulk::class, function (Faker $faker) {
    return [
        'ref' => $faker->swiftBicNumber,
		'cr' => $faker->numberBetween(180, 11000),
		'dr' => $faker->numberBetween(50, 10000),
		'dttime' => $faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now', $timezone = 'Africa/Nairobi'),
		'status' => $faker->numberBetween(1,1),
		'mmref' => $faker->swiftBicNumber,
		'ipay_comm' => $faker->numberBetween(100,1000),
		//'telephone' => $faker->numberBetween(254700000000, 254790000000),
		'telephone' => $faker->e164PhoneNumber,
		'rbalance' => $faker->numberBetween(200, 8000),
    ];
});
