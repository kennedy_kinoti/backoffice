<?php $__env->startSection('title', 'Payments'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/"><?php echo e(__('header.dashboard')); ?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo e(__('header.payments')); ?></li>
  </ol>
</nav>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('successMsg')): ?>
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('successMsg')); ?>

    </div>
<?php elseif(session('failMsg')): ?>
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('failMsg')); ?>

    </div>
<?php endif; ?>
<div class="panel panel-primary">
    <div class="panel-heading"><?php echo e(__('header.payments')); ?></div>
    <div class="panel-body">

        <div class="row">
               
            <form method ="GET" action="<?php echo e(url('payments/search')); ?>">
               <?php echo csrf_field(); ?>
                <div class="form-group col-md-4">
                    <!-- <label>Transaction code</label> -->
                    <input type="text" name="code" class="form-control" id="code" placeholder="<?php echo e(__('header.transaction_code')); ?>" autocomplete="off">
                </div>
                <!-- <div class="form-group col-md-2"> -->
                    <!-- <label>From</label> -->
                    <!-- <input type="text"  value="" class="form-control datepicker" id="date_from" placeholder="From" name="date_from" autocomplete="off"> -->
                <!-- </div> -->
                <!-- <div class="form-group col-md-2"> -->
                    <!-- <label>To</label> -->
                    <!-- <input type="text"  value="" class="form-control datepicker" id="date_to" placeholder="To"  name="date_to" autocomplete="off"> -->
                <!-- </div> -->
                <div class="form-group col-md-4">
                <!-- <label>From</label> -->
                    <input type="text"  class="form-control" name="datefilter" value="" placeholder="<?php echo e(__('header.date_range')); ?>" autocomplete="off"/>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success"><?php echo e(__('header.search')); ?></button>
                </div>

                <?php if(!empty($data)): ?>
                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"
                        
                    > <?php echo e(__('header.export')); ?></a>
                </div>
                <?php endif; ?>
            </form>
            
            <!-- <div class="form-group col-md-1">
                <a href="<?php echo e(url('/payments/export')); ?>" name="export" class="btn btn-danger"> PDF</a>
            </div> -->
            <div class="form-group col-md-1">
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">
                    <span class="glyphicon glyphicon-collapse-down"></span> <?php echo e(__('header.advanced')); ?>

                </button>
            </div>
            <div id="demo" class="collapse col-md-12">
                <div class="form-group col-md-3">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled><?php echo e(__('header.channel')); ?></option>
                        <option>MPESA</option>
                        <option>Airtel Money</option>
                        <option>Equitel</option>
                        <option>eLipa</option>
                        <option>Kenswitch</option>
                        <option>Visa</option>
                        <option>Mastercard</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="oid" placeholder="Order ID" name="oid"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="fname" placeholder="Client First Name" name="fname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="lname" placeholder="Client Last Name" name="lname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"/>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th><?php echo e(__('header.code')); ?></th>
                        <th><?php echo e(__('header.channel')); ?></th>
                        <th><?php echo e(__('header.date')); ?></th>
                        <th><?php echo e(__('header.amount')); ?></th>
                        <th><?php echo e(__('header.name')); ?></th>
                        <th><?php echo e(__('header.action')); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(empty($data)): ?>
                        <tr>
                            <td colspan="5" align="center">
                                <?php echo e(__('header.search_first')); ?>

                            </td>
                        </tr>
                    <?php else: ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($payment->txncode); ?></td>
                            <td><?php echo e($payment->sendernumber); ?></td>
                            <td><?php echo e($payment->receivingdatetime); ?></td>  
                            <td><?php echo e($payment->txnamt); ?></td>
                            <td><?php echo e($payment->fname); ?> <?php echo e($payment->lname); ?></td>
                            <td>
                                <button name="refund" data-payid="<?php echo e($payment->id); ?>" data-toggle="modal" data-target="#detailsModal<?php echo e($payment->id); ?>" value="refund" type="submit" data-placement="top" data-trigger="hover" data-toggle="popover" data-content="Refund" class="btn btn-warning pending"><i class="fa fa-eye"></i> <?php echo e(__('header.view')); ?></button>

                                <div class="modal fade" id="detailsModal<?php echo e($payment->id); ?>" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <input name="_token" type="hidden" value="<?php echo e(csrf_token()); ?>"/>
                                                
                                                <h5 class="modal-title" id="detailsModalLabel"><?php echo e(__('header.transaction_details')); ?></h5>
                                                
                                                <button type="button" class="close" data-dismiss="modal" data-modal="" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>   
                                            </div>

                                            <div class="modal-body">
                                                <?php if(empty($payment->id)): ?>
                                                    <tr><td><?php echo e(__('header.response')); ?></td></tr>
                                                <?php else: ?>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><b><?php echo e(__('header.client_name')); ?> </b>:</td>
                                                        <td><?php echo e($payment->fname); ?> <?php echo e($payment->lname); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.transaction_code')); ?></b>:</td>
                                                        <td><?php echo e($payment->txncode); ?></td>
                                                        <input name="email" value="<?php echo e($payment->txncode); ?>" placeholder="email" hidden/>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.order_id')); ?> </b>:</td>
                                                        <td><?php echo e($payment->runid); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.date_time')); ?> </b>:</td>
                                                        <td><?php echo e($payment->receivingdatetime); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.channel')); ?> </b>:</td>
                                                        <td><?php echo e($payment->sendernumber); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.amount')); ?> </b>:</td>
                                                        <td><?php echo e($payment->txnamt); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b><?php echo e(__('header.type')); ?> </b>:</td>
                                                        <td><?php echo e($payment->curr); ?></td>
                                                    </tr>
                                    
                                                    <tr hidden>
                                                        <td><b><?php echo e(__('header.card_mask')); ?> </b>:</td>
                                                        <td>3213 XXXX XXXX XXXX</td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary pull-left" data-dismiss="modal"><?php echo e(__('header.close')); ?></button>
                                                <a href="<?php echo e(url('/payments/export')); ?>"><button type="submit" class="btn btn-danger"><?php echo e(__('header.export')); ?></button></a>

                                                <a hidden href="<?php echo e(url('/refund/partial_initiate/'.$payment->id)); ?>"><button type="button" class="btn btn-primary"><?php echo e(__('header.partial_refund')); ?></button></a>
            
                                                <a href="<?php echo e(url('/refund/full_initiate/'.$payment->id)); ?>"><button type="button" class="btn btn-success"><?php echo e(__('header.full_refund')); ?></button></a>
                                            </div>
                                            
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                    <?php endif; ?> 
                </tbody>
            </table>
        </div>
        <?php if(!empty($data)): ?>
            <?php echo e($data); ?>

        <?php endif; ?>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>