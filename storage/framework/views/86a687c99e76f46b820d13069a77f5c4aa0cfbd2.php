<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="theme-color" content="#337ab7">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <meta name="description" content="iPay Africa Backoffice"/>
        <link rel="shortcut icon" href="<?php echo e(asset('images/favicon.png')); ?>">

        <title><?php echo e(config('app.name')); ?></title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="manifest" href="<?php echo e(asset('manifest.json')); ?>" />

    </head>
    <body>

        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar" class="open">
            
                <ul class="list-unstyled components">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn animate open" style="margin-left:10px;">
                        <i class="glyphicon glyphicon-align-left"></i>
                    </button>
                    <br><br>
                    <!-- B2B -->
                    <!--  <li <?php echo e((Request::is('pos/form') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/pos/form')); ?>"> <span class="glyphicon glyphicon-ok-circle"  data-toggle="tooltip" data-placement="top" title="Form"></span> <span id="lipa"> POS Form </span> </a>
                    </li> -->

                    <!-- Payments -->
                    <!-- <li <?php echo e((Request::is('vue-payment') ? 'class = active' : '')); ?> >
                        <a href="<?php echo e(url('/vue-payment')); ?>" data-toggle="tooltip" data-placement="top" title="Payments"> <span class="fa fa-barcode" ></span> <span id="lipa" >Payments - (Vue)</span></a>
                    </li> -->
                    <li <?php echo e((Request::is('payments') || Request::is('payments/search') ? 'class = active' : '')); ?> >
                        <a href="<?php echo e(url('payments')); ?>" data-toggle="tooltip" data-placement="top" title="Payments"> <span class="fa fa-blind" ></span> <span id="lipa" ><?php echo e(__('header.payments')); ?> </span></a>
                    </li>


                    <!-- Refunds -->
                    
                    <?php if(Gate::check('is_checked', 'initiated') || Gate::check('is_checked', 'initiated') || Gate::check('is_checked', 'refunded_payments')): ?>
                    <li>
                        <a href="#refunds" data-toggle="collapse" aria-expanded="false"> <span class="glyphicon glyphicon-repeat"  data-toggle="tooltip" data-placement="top" title="Refunds"></span> <span id="lipa"> <?php echo e(__('header.refunds')); ?> </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('refund/view') || Request::is('refund/complete') || Request::is('refunded') ? 'in' : '')); ?>" id="refunds">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'initiated')): ?>
                            <li <?php echo e((Request::is('refund/view') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/refund/view')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.initiated')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'completed')): ?>
                            <li <?php echo e((Request::is('refund/complete') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/refund/complete')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.completed')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'refunded_payments')): ?>
                            <li <?php echo e((Request::is('refunded') ? 'class = active' : '')); ?>> <a href="<?php echo e(url('/refunded')); ?>" ><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.refunded_payments')); ?> </a></li>
                            <?php endif; ?>
                            <!-- <li <?php echo e((Request::is('refund/view') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/refund/view')); ?>"> Vue Refunds </a></li> -->
                            
                        </ul>
                    </li>
                    <?php endif; ?>

                    <!-- User Account Management -->
                    <li>
                        <a href="#settings" data-toggle="collapse" aria-expanded="false" ><span class="glyphicon glyphicon-user"  data-toggle="tooltip" data-placement="top" title="Manage Users"></span> <span id="lipa"> <?php echo e(__('header.manage_users')); ?> </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('user/profile') || Request::is('user/create') || Request::is('user/manage') ? 'in' : '')); ?>" id="settings">
                            <li <?php echo e((Request::is('user/profile') ? 'class = active' : '')); ?>><a href="<?php echo e(url('user/profile')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.profile')); ?></a></li>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'add_users')): ?>
                            <li <?php echo e((Request::is('user/create') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/user/create')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.create_user')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'manage_users')): ?>
                            <li <?php echo e((Request::is('user/manage') || Request::is('user/manage/{id?}/edit') ? 'class = active' : '')); ?>><a href="<?php echo e(url('user/manage')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.manage_user')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    
                    <!-- Manage Account -->
                    <?php if(Gate::check('is_checked', 'add_sub_accounts') || Gate::check('is_checked', 'sub_accounts_action')): ?>
                    <li>
                        <a href="#manageSub" data-toggle="collapse" aria-expanded="false"><span class="glyphicon glyphicon-cog"  data-toggle="tooltip" data-placement="top" title="Manage Sub Accounts"></span> <span id="lipa"> <?php echo e(__('header.manage_sub_accounts')); ?> </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('add-sub') || Request::is('list-sub') ? 'in' : '')); ?>" id="manageSub">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'add_sub_accounts')): ?>
                            <li <?php echo e((Request::is('add-sub') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/add-sub')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.sub_accounts')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'sub_accounts_action')): ?>
                            <li <?php echo e((Request::is('list-sub') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/list-sub')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.sub_accounts_action')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <!-- Account -->
                    <li <?php echo e((Request::is('account') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/account')); ?>"> <span class="fa fa-cogs"  data-toggle="tooltip" data-placement="top" title="Remittance Details"></span> <span id="lipa"> <?php echo e(__('header.remittance_details')); ?> </span> </a>
                    </li>

                    <li <?php echo e((Request::is('rem_account') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/rem_account')); ?>"> <span class="fa fa-cogs"  data-toggle="tooltip" data-placement="top" title="Remittance"></span> <span id="elipa"> <?php echo e('Remittance Account'); ?> </span> </a>
                    </li>
                    <!-- Bank Remittance -->
                    <!-- <li <?php echo e((Request::is('remittance') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/remittance')); ?>"> <span class="fa fa-institution"  data-toggle="tooltip" data-placement="top" title="Remittance Reports"></span> <span id="lipa"> <?php echo e(__('header.remittance_reports')); ?> </span> </a>
                    </li> -->
                    <!-- Invoicing -->

                    <?php if(Gate::check('is_checked', 'create_invoices') || Gate::check('is_checked', 'manage_invoices') || Gate::check('is_checked', 'confirm_invoices')): ?>
                    <li>
                        <a href="#invoicing" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-file-text" data-toggle="tooltip" data-placement="top" title="Invoicing"></span>  <span id="lipa"> <?php echo e(__('header.invoicing')); ?> </span> </a>

                        <ul class="collapse list-unstyled <?php echo e((Request::is('invoice/create') || Request::is('invoice') || Request::is('invoice/list')? 'in' : '')); ?>" id="invoicing">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'create_invoices')): ?>
                             <li <?php echo e((Request::is('invoice/create') ? 'class = active' : '')); ?>><a href="<?php echo e(url('invoice/create')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.create_invoice')); ?> </a></li> <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'manage_invoices')): ?>  
                            <li <?php echo e((Request::is('invoice') ? 'class = active' : '')); ?>><a href="<?php echo e(url('invoice')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.view_invoice')); ?> </a></li><?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'confirm_invoices')): ?>
                            <li <?php echo e((Request::is('invoice/list') ? 'class = active' : '')); ?>><a href="<?php echo e(url('invoice/list')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.view_sent')); ?></a></li><?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    
                    <!-- Disbursed Payments -->
                    <?php if(Gate::check('is_checked', 'manage_bulk_pay') || Gate::check('is_checked', 'view_bp_reports') || Gate::check('is_checked', 'view_bp_archived')): ?>
                    <li>
                        <a href="#bulkpay" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-money"  data-toggle="tooltip" data-placement="top" title="Bulk Pay"></span> <span id="lipa"> <?php echo e(__('header.disburse')); ?> </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('bulk/manage') || Request::is('bulk') || Request::is('bulk/archived') || Request::is('b2b')? 'in' : '')); ?>" id="bulkpay">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'manage_bulk_pay')): ?>
                            <li <?php echo e((Request::is('bulk/manage') ? 'class = active' : '')); ?>><a href="<?php echo e(url('bulk/manage')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.manage_bulk_pay')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'view_bp_reports')): ?>
                            <li <?php echo e((Request::is('bulk') ? 'class = active' : '')); ?>><a href="<?php echo e(url('bulk')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.view_bulk_pay_reports')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'view_bp_archived')): ?>
                            <li <?php echo e((Request::is('bulk/archived') ? 'class = active' : '')); ?>><a href="<?php echo e(url('bulk/archived')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo e(__('header.view_bulk_pay_archived')); ?></a></li>
                            <?php endif; ?>
                            <!-- <li <?php echo e((Request::is('b2b') ? 'class = active' : '')); ?>>
                                <a href="<?php echo e(url('/b2b')); ?>"> <span class="glyphicon glyphicon-share"  data-toggle="tooltip" data-placement="top" title="B2B"></span> <span id="lipa"> B2B </span> </a>
                            </li> -->
                        </ul>
                    </li>
                    <?php endif; ?>

                    <!-- B2B -->
                    <li <?php echo e((Request::is('b2b') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/b2b')); ?>"> <span class="glyphicon glyphicon-share"  data-toggle="tooltip" data-placement="top" title="B2B"></span> <span id="lipa"> B2B </span> </a>
                    </li>

                    <!-- POS -->
                    <?php if(Gate::check('is_checked', 'pos_pdq_service') || Gate::check('is_checked', 'virtual_pos')): ?>
                    <li>
                        <a href="#pos" data-toggle="collapse" aria-expanded="false"> <span class="glyphicon glyphicon-object-align-vertical"  data-toggle="tooltip" data-placement="top" title="POS"></span> <span id="lipa"> POS </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('pos') || Request::is('pos/virtual')? 'in' : '')); ?>" id="pos">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'pos_pdq_service')): ?>
                            <li <?php echo e((Request::is('pos') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/pos')); ?>"> <i class="fa fa-chevron-right" aria-hidden="true" title="POS"></i><?php echo e(__('header.pos_pdq_service')); ?></a></li>
                            <?php endif; ?>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('is_checked', 'virtual_pos')): ?>
                            <li <?php echo e((Request::is('pos/virtual') ? 'class = active' : '')); ?>><a href="<?php echo e(url('/pos/virtual')); ?>"> <i class="fa fa-chevron-right" aria-hidden="true" title="POS"></i><?php echo e(__('header.virtual_pos')); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <!-- Archived Online Data -->
                    <!-- <li>
                        <a href="#archived" data-toggle="collapse" aria-expanded="false"> <span class="fa fa-briefcase"  data-toggle="tooltip" data-placement="top" title="Archived Data"></span> <span id="lipa"> <?php echo e(__('header.archived_online_data')); ?> </span></a>
                        <ul class="collapse list-unstyled <?php echo e((Request::is('archived/success') || Request::is('archived/suspense') || Request::is('archived/pos')? 'in' : '')); ?>" id="archived">
                            <li <?php echo e((Request::is('archived/success') ? 'class = active' : '')); ?>><a href="<?php echo e(url('archived/success')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.success')); ?></a></li>
                            <li <?php echo e((Request::is('archived/suspense') ? 'class = active' : '')); ?>><a href="<?php echo e(url('archived/suspense')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.suspense')); ?></a></li>
                            <li <?php echo e((Request::is('archived/pos') ? 'class = active' : '')); ?>><a href="<?php echo e(url('archived/pos')); ?>"><i class="fa fa-chevron-right" aria-hidden="true"></i> <?php echo e(__('header.pos')); ?></a></li>
                        </ul>
                    </li> -->

                    <!-- Analytics Menus -->
                    <li <?php echo e((Request::is('dashboard') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/dashboard')); ?>"> <span class="glyphicon glyphicon-dashboard"  data-toggle="tooltip" data-placement="top" title="Analytics"></span> <span id="lipa"> <?php echo e(__('header.analytics')); ?> </span> </a>
                    </li>

                    <!-- Mail Menus -->
                    <!-- <li <?php echo e((Request::is('basicemail') ? 'class = active' : '')); ?>>
                        <a href="<?php echo e(url('/basicemail')); ?>"> <span class="glyphicon glyphicon-envelope"  data-toggle="tooltip" data-placement="top" title="Mail *"></span> <span id="lipa"> <?php echo e(__('header.mail')); ?> </span> </a>
                    </li> -->
                    
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="<?php echo e(url('/logout')); ?>" class="download"> <span class="fa fa-power-off"></span> <span id="lipa"> <?php echo e(__('header.log_out')); ?></span></a></li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            
                            <a href="<?php echo e(url('/')); ?>" class="navbar-brand"><span class="text-light"><?php echo e(config('app.name')); ?></span></a>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            
                            <ul class="nav navbar-nav navbar-right">
                            
                                <li class="nav-item dropdown" >
                                    <button class="btn btn-info dropdown-toggle" href="#" data-toggle="dropdown" type="button" aria-expanded="false" role="button">
                                        <!-- <i class="fa fa-language text-dark"  data-toggle="tooltip" title="Language Switcher" ></i> -->
                                        <i class="material-icons" style="font-size: 20px">language</i>
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" id="languageSwitcher">
                                        <li><a href="<?php echo e(route('lang', 'en')); ?>">English</a></li>
                                        <li><a href="<?php echo e(route('lang', 'swa')); ?>">Swahili</a></li>
                                        <li><a href="<?php echo e(route('lang', 'fr')); ?>">French</a></li>
                                    </ul>
                                    &nbsp
                                    
                                    <!-- <button class="btn btn-info"> -->
                                        <!-- <span class="glyphicon glyphicon-bell text-default"></span> -->
                                        <!-- <i class="material-icons" style="font-size: 20px">notification_important</i>
                                        <span class="badge badge-dark">5</span>
                                    </button> -->
                                        
                                    &nbsp
                                    <!-- Authentication Links -->
                                    <?php if(auth()->guard()->guest()): ?>
                                        <li>
                                            <button class="btn btn-info" onclick="return window.location='<?php echo e(route('login')); ?>'">
                                                <?php echo e(__('Login')); ?>

                                            </button>
                                        </li>
                                        <!-- <li><a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a></li> -->
                                    <?php else: ?>
                                        <li class="nav-item dropdown">
                                            <button id="navbarDropdown" class="btn btn-info dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <!-- <span class="glyphicon glyphicon-user"></span> -->
                                            <i class="material-icons" style="font-size: 20px">face</i>
                                               
                                                <span class="caret"></span>
                                            </button>

                                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="languageSwitcher">
                                                <li><a class="dropdown-item" href="" > <?php echo e(Auth::user()->name); ?>'s Account </a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                                onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                    <?php echo e(__('Logout')); ?>

                                                </a></li>

                                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                                    <?php echo csrf_field(); ?>
                                                </form>
                                            </ul>

                                        </li>
                                    <?php endif; ?>
                              
                            </ul>
                        </div>
                    </div>
                </nav>
                <?php echo $__env->yieldContent('breadcrumb'); ?>

                <!-- Append content -->
                <?php echo $__env->yieldContent('content'); ?>
                <!-- End of append content -->

                <!-- Footer -->

            </div>
        
        <style>
            .badge-notify{
                background:red;
                position:relative;
                transform: (-100%, -100%);
            }
        </style>

        <script type="text/javascript" src="<?php echo e(asset('js/app.js')); ?>"></script>
        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function (event) {
                    console.log(event.target)
                    $('#sidebar, #content, #lipa').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    event.preventDefault();
                });
            });
        </script>

        <script>
        if ('serviceWorker' in navigator ) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('../../service-worker.js',{
                    scope: '.'
                }).then(function(registration) {
                    // Registration was successful
                    console.log('Service Worker registration successful with scope: ', registration.scope);
                }, function(err) {
                    // registration failed :(
                    console.log('Service Worker registration failed: ', err);
                });
            });
        }
        </script>

        <script type="text/javascript">
            $(function() {

                $('input[name="datefilter"]').daterangepicker({
                    showDropdowns: true,
                    autoUpdateInput: false,
                });

                $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' / ' + picker.endDate.format('YYYY-MM-DD'));

                });

                $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });

            });
        </script>
    </body>
</html>
