<?php $__env->startSection('title', 'Page Title'); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <p>
                <?php echo e(__('header.currency')); ?>

                <a href="https://backoffice.ipayafrica.com/index.php?curr=kes" class="btn btn-info btn-xs">KES</a>
                <a href="https://backoffice.ipayafrica.com/index.php?curr=usd" class="btn btn-info btn-xs">USD</a>
            </p>  

        </div>

        <div class="col-md-6">
            <p>
                <a href="https://backoffice.ipayafrica.com/index.php" class="btn btn-default btn-xs"><?php echo e(__('header.today')); ?></a>
                <a href="https://backoffice.ipayafrica.com/index.php?time=week" class="btn btn-default btn-xs"><?php echo e(__('header.this_week')); ?></a>
                <a href="https://backoffice.ipayafrica.com/index.php?time=month" class="btn btn-default btn-xs"><?php echo e(__('header.this_month')); ?></a>
            </p>    
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="row">

        <div class="col-md-4"  style="background-color: #337ab7; border-left: 4px solid #F5D313; color: #fff;">
            <div class="transactions-count">
                <div class="pull-left">
                    <a href="#"><h1>0 </h1></a>

                    <span class="description"> <?php echo e(__('header.transactions_today')); ?></span>
                </div>
                <div class="icon"><i class="fa fa-fw  fa-shopping-cart"></i> </div>
                <span class="pull-right  mini-graph warning"></span>
            </div>
        </div>

        <div class="col-md-4" style="background-color: #f56954; border-left: 4px solid #F5D313; color: #fff;" >
            <div class="transactions-count">
                <div class="pull-left">
                    <a href="#"><h1>0.00 </h1></a>

                    <span class="description"> <?php echo e(__('header.total_amount_today')); ?></span>
                </div>
                <div class="icon"><i class="fa fa-fw  fa-shopping-cart"></i> </div>
                <span class="pull-right  mini-graph warning"></span>
            </div>
        </div>

        <div class="col-md-4" style="background-color: #f39c12; border-left: 4px solid #F5D313; color: #fff;">
            <div class="transactions-count">
                <div class="pull-left">
                    <a href="#"><h1>0.00 </h1></a>

                    <span class="description"> <?php echo e(__('header.average')); ?></span>
                </div>
                <div class="icon"><i class="fa fa-fw  fa-shopping-cart"></i> </div>
                <span class="pull-right  mini-graph warning"></span>
            </div>
        </div>
    </div>
</div>

<br/>

<div class="panel panel-primary">
    <div class="panel-heading"><?=date('Y');?> <?php echo e(__('header.bar_graph')); ?></div>
    <div class="panel-body">
        <div id="chart" style="width:100%; margin:0 auto;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">   
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo e(__('header.transactions_per_channel')); ?></div>
            <div class="panel-body">
                <div id="chart1" style="width:100%; margin:0 auto;"></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading"><?php echo e(__('header.transactions_aggregate_summary')); ?></div>
            <!-- <div class="panel-body">
                <div id="chart2" style="width:100%; margin:0 auto;"></div>
            </div> -->
            <div class="panel-body">
                <table class="table table-hover table-condensed">
                    <tbody>
                        <tr class="success"><td><b><?php echo e(__('header.month')); ?></b></td><td><b><?php echo e(__('header.transactions')); ?></b></td><td><b><?php echo e(__('header.amount')); ?></b></td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.january')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.february')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.march')); ?></b></td><td>1</td><td>5.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.april')); ?></b></td><td>8</td><td>7,073.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.may')); ?></b></td><td>310739</td><td>18,565,589.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.june')); ?></b></td><td>177</td><td>111,814.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.july')); ?></b></td><td>84</td><td>131,971.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.august')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.september')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.october')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.november')); ?></b></td><td>0</td><td>0.00</td></tr>
                        <tr class="active"><td><b><?php echo e(__('header.december')); ?></b></td><td>0</td><td>0.00</td></tr>
                        
                        <tr class="success"><td><b><?php echo e(__('header.total')); ?></b></td><td>311009</td><td>18,816,452.00</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>    

<?= Lava::render("ColumnChart", "IMDB", "chart"); ?>

<?= Lava::render("PieChart", "IMDB", "chart1"); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>