<?php $__env->startSection('title', 'Page Title'); ?>

<?php $__env->startSection('menu', 'active'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/"><?php echo e(__('header.dashboard')); ?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo e(__('header.remmittance_account')); ?></li>
  </ol>
</nav>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-primary">
    <div class="panel-heading"><?php echo e(__('header.account_details')); ?> </div>
    <div class="panel-body">
        <div class="table-responsive">
            <?php if(!empty($data)): ?>
            <table class="table table-hover">
                <tr class="info">
                    <td></td>
                    <td></td>
                    <td><b><?php echo e(__('header.bank_details')); ?> </b></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b><?php echo e(__('header.bank_name')); ?> :</b></td>
                    <td><?php echo e($data[0]['mer_bankname']); ?></td>
                    <td><b><?php echo e(__('header.bank_branch')); ?> :</b></td>
                    <td><?php echo e(ucfirst($data[0]['mer_bankbranch'])); ?></td>
                </tr>
                <tr>
                    <td><b><?php echo e(__('header.ac_number')); ?> :</b></td>
                    <td><?php echo e($data[0]['mer_bankaccnum']); ?></td>
                    <td><b><?php echo e(__('header.ac_name')); ?> :</b></td>
                    <td><?php echo e(ucwords($data[0]['mer_bankaccname'])); ?></td>
                </tr>

                <tr class="info">
                    <td></td>
                    <td></td>
                    <td><b><?php echo e(__('header.payment_options')); ?></b></td>
                    <td></td>
                </tr>

                <tr>
                    <td colspan="2"><?php echo e(__('header.pay_at')); ?></td>
                    <td><input type="text" value="<?php echo e($data[0]['release_level']); ?>" readonly=""></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo e(__('header.pay_me_using')); ?></td>
                    <td><?php echo e($data[0]['mer_pay_mode']); ?></td>
                    <td></td>
                </tr>
                
            </table>
            <?php else: ?>
                <strong style="font-size:1.2em">Nothing to show</strong>
            <?php endif; ?>
        </div>
    </div>
</div>


<form action="<?php echo e(route('settle')); ?>" method="POST" accept-charset="utf-8" class="form-vertical" role="form">
    <?php echo csrf_field(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><?php echo e(__('header.frequency_details')); ?></div>
        <div class="panel-body">
            <div class="table-responsive">
                <div class="col-md-12">
                    <b><?php echo e(__('header.settle_me')); ?> <?php echo e(__('header.every')); ?></b>
                    <ul class="list-inline">
                            
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox"  <?php if(!empty($data[0]['mon']) and $data[0]['mon'] != 0): ?>: checked <?php endif; ?>  name="monday" value="1" id = "mon" onchange="weekdays()"><?php echo e(__('header.monday')); ?></label></li>
                        </li><label class="checkbox-inline"><input class="daily_check" type="checkbox" <?php if(!empty($data[0]['tue']) and $data[0]['tue'] != 0): ?>: checked <?php endif; ?> name="tuesday" value="1" id = "tue" onchange="weekdays()"><?php echo e(__('header.tuesday')); ?></label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" <?php if(!empty($data[0]['wed']) and $data[0]['wed'] != 0): ?>: checked <?php endif; ?> name="wednesday" value="1" id = "wed" onchange="weekdays()"><?php echo e(__('header.wednesday')); ?></label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" <?php if(!empty($data[0]['thur']) and $data[0]['thur'] != 0): ?>: checked <?php endif; ?> name="thursday" value="1" id = "thur" onchange="weekdays()"><?php echo e(__('header.thursday')); ?></label></li>
                        <li><label class="checkbox-inline"><input class="daily_check" type="checkbox" <?php if(!empty($data[0]['fri']) and $data[0]['fri'] != 0): ?>: checked <?php endif; ?> name="friday" value="1" id = "fri" onchange="weekdays()"><?php echo e(__('header.friday')); ?></label></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">                
                <div class="col-md-12">
                <b><?php echo e(__('header.settle_me')); ?></b>
                <ul class="list-inline">
                    <li><label class="checkbox-inline"><input class="settle_duration" id="everyday" type="checkbox" <?php if(!empty($data[0]['mon']) and $data[0]['mon'] != 0 and !empty($data[0]['tue']) and $data[0]['tue'] != 0 and !empty($data[0]['wed']) and $data[0]['wed'] != 0 and !empty($data[0]['thur']) and $data[0]['thur'] != 0): ?> and !empty($data[0]['fri']) and $data[0]['fri'] != 0): checked <?php endif; ?> name="everyday" value="1" onclick="days()"><?php echo e(__('header.everyday')); ?></label></li>  
                    <li><label class="checkbox-inline"><input class="settle_duration" id="fortnight" type="checkbox" <?php if(!empty($data[0]['midmnth']) and $data[0]['midmnth'] != 0): ?>: checked <?php endif; ?>  name="midmonth" value="1" onchange = "settle()"><?php echo e(__('header.fortnight')); ?></label></li>  
                    <li><label class="checkbox-inline"><input class="settle_duration" id="monthly" type="checkbox" <?php if(!empty($data[0]['startmnth']) and $data[0]['startmnth'] != 0): ?>: checked <?php endif; ?> name="startmonth" value="1" onchange = "settles()"><?php echo e(__('header.monthly')); ?></label></li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">                
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-sm"><?php echo e(__('header.save')); ?></button>
            </div>
        </div>
        <div class="table-responsive">                
            <div class="col-md-12">
                <?php echo e(__('header.terms_and_conditions')); ?>

                <br/>
                <small><?php echo e(__('header.t_c')); ?></small>
            </div>
        
    </div>
</form>

<?php $__env->stopSection(); ?>
<script>
    function days()
    {
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');
        var mon = document.getElementById('mon');
        var tue = document.getElementById('tue');
        var wed = document.getElementById('wed');
        var thur= document.getElementById('thur');
        var fri = document.getElementById('fri');

    var all = [mon, tue, wed, thur, fri];

        if(everyday.checked == true){

            for(x = 0; x < all.length; x++)
            {
                all[x].checked = true;
            } 
            fortnight.checked = false;
            month.checked = false;
            }   else {
                for(x = 0; x < all.length; x++)
                    {
                        all[x].checked = false;
                    } 
    }
}
    function weekdays(){
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');
        var mon = document.getElementById('mon');
        var tue = document.getElementById('tue');
        var wed = document.getElementById('wed');
        var thur= document.getElementById('thur');
        var fri = document.getElementById('fri');

        var wkdays = [mon, tue, wed, thur, fri];
        var i;

        for (i = 0; i < wkdays.length; i++){
            if(wkdays[i].checked == false){
                everyday.checked = false;
            }
            else {
                fortnight.checked = false;
                month.checked = false;
            }
        }
    }
        var everyday = document.getElementById('everyday');
        var fortnight = document.getElementById('fortnight');
        var month = document.getElementById('monthly');

        function settle(){
            document.getElementById('monthly').checked = false;
            document.getElementById('everyday').checked = false;
            document.getElementById('mon').checked = false;
            document.getElementById('tue').checked = false;
            document.getElementById('wed').checked = false;
            document.getElementById('thur').checked = false;
            document.getElementById('fri').checked = false;
        }
        
        function settles(){
            document.getElementById('everyday').checked = false;
            document.getElementById('fortnight').checked = false;
            document.getElementById('mon').checked = false;
            document.getElementById('tue').checked = false;
            document.getElementById('wed').checked = false;
            document.getElementById('thur').checked = false;
            document.getElementById('fri').checked = false;
        }
</script>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>