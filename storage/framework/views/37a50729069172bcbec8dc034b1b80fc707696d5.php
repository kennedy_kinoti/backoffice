<?php $__env->startSection('title', 'Payments'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/"><?php echo e(__('header.dashboard')); ?></a></li>
    <li class="breadcrumb-item active" aria-current="page">B2B</li>
  </ol>
</nav>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="panel panel-primary">
    <div class="panel-heading">Business to Business (B2B)</div>
    <div class="panel-body">

        <form>
            <div class="form-group ">
                <label><?php echo e(__('header.account_type')); ?></label>
                <select class="form-control" name="accounttype">
                    <option value=""><?php echo e(__('header.pick_type')); ?></option>
                    <option value="mpesapaybill">Mpesa Paybill</option>
                    <option value="mpesatill">Mpesa Till/ Buy Goods</option>
                </select>
            </div>
            <div class="form-group ">
                <label><?php echo e(__('header.sender_to')); ?> *</label>
                <input type="text" autocomplete="off" required="" name="sendernumber" class="form-control" id="ref" placeholder="eg 510800" size="12" maxlength="12" value="">
            </div>
            <div class="form-group ">
                <label><?php echo e(__('header.narration')); ?></label>
                <input type="text" autocomplete="off" required="" name="account" class="form-control" id="ref" placeholder="eg Paybill account number" disabled="" value="">
            </div>
            <div class="form-group ">
                <label><?php echo e(__('header.amount')); ?> *</label>
                <input type="text" autocomplete="off" required="" name="amount" class="form-control" placeholder="Amount" value="">
            </div>
            <div class="form-group">
                <button disabled="" class="btn btn-hg btn-block btn-info" type="submit"> <?php echo e(__('header.send')); ?></button>
            </div>
        </form>
        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>