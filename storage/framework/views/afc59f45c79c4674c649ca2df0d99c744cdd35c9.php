<?php $__env->startSection('title', 'POS'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/"><?php echo e(__('header.dashboard')); ?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo e(__('header.pos_pdq_service')); ?></li>
  </ol>
</nav>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('successMsg')): ?>
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('successMsg')); ?>

    </div>
<?php elseif(session('failMsg')): ?>
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('failMsg')); ?>

    </div>
<?php endif; ?>

<div class="panel panel-primary">
    <div class="panel-heading"><?php echo e(__('header.pos_pdq_service')); ?></div>
    <div class="panel-body">

        <form method ="POST" action="<?php echo e(url('pos/send')); ?>">
            <input name="_token" type="hidden" value="<?php echo e(csrf_token()); ?>"/>
            
            <div class="form-group ">
                <label><?php echo e(__('header.amount')); ?> *</label>
                <input type="number" min="0" autocomplete="off" required="" name="amount" class="form-control" placeholder="<?php echo e(__('header.amount')); ?>" value="">
            </div>
            <div class="form-group ">
                <label><?php echo e(__('header.narration')); ?></label>
                <input type="text" autocomplete="off" required="" name="desc" class="form-control" id="desc" placeholder="eg POS Cashier 1" value="iPay Backoffice POS Request">
            </div>
            <div class="form-group">
                <button class="btn btn-hg btn-block btn-info" type="submit"> <?php echo e(__('header.send')); ?></button>
            </div>
        </form>
        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>