<?php $__env->startSection('title', 'Payments'); ?>

<?php $__env->startSection('breadcrumb'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/"><?php echo e(__('header.dashboard')); ?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo e('Remittance Account'); ?></li>
  </ol>
</nav>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('successMsg')): ?>
    <div class="alert alert-success alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('successMsg')); ?>

    </div>
<?php elseif(session('failMsg')): ?>
    <div class="alert alert-danger alert-dismissible fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo e(session('failMsg')); ?>

    </div>
<?php endif; ?>
<div class="panel panel-primary">
    <div class="panel-heading"><?php echo e('Remittance Account'); ?></div>
    <div class="panel-body">

        <div class="row">
               
            <form method ="GET" action="<?php echo e(url('payments/search')); ?>">
               <?php echo csrf_field(); ?>
                <div class="form-group col-md-4">
                    <!-- <label>Transaction code</label> -->
                    <input type="text" name="code" class="form-control" id="code" placeholder="<?php echo e(__('header.transaction_code')); ?>" autocomplete="off">
                </div>
                <div class="form-group col-md-4">
                <!-- <label>From</label> -->
                    <input type="text"  class="form-control" name="datefilter" value="" placeholder="<?php echo e(__('header.date_range')); ?>" autocomplete="off"/>
                </div>

                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="search" class="btn btn-success"><?php echo e(__('header.search')); ?></button>
                </div>

                <?php if(!empty($data)): ?>
                <div class="form-group col-md-1">
                    <button type="submit" name="submit" value="export" class="btn btn-danger"
                        
                    > <?php echo e(__('header.export')); ?></a>
                </div>
                <?php endif; ?>
            </form>
        
            <div class="form-group col-md-1">
                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#demo">
                    <span class="glyphicon glyphicon-collapse-down"></span> <?php echo e(__('header.advanced')); ?>

                </button>
            </div>
            <div id="demo" class="collapse col-md-12">
                <div class="form-group col-md-3">
                    <select class="form-control" id="channel" placeholder="Channel" name="channel">
                        <option selected disabled><?php echo e(__('header.channel')); ?></option>
                        <option>MPESA</option>
                        <option>Airtel Money</option>
                        <option>Equitel</option>
                        <option>eLipa</option>
                        <option>Kenswitch</option>
                        <option>Visa</option>
                        <option>Mastercard</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="oid" placeholder="Order ID" name="oid"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="fname" placeholder="Client First Name" name="fname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="lname" placeholder="Client Last Name" name="lname"/>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"/>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed" id="list">
                <thead>
                    <tr>
                        <th><?php echo e(__('header.code')); ?></th>
                        <th><?php echo e(__('header.channel')); ?></th>
                        <th><?php echo e(__('header.date')); ?></th>
                        <th><?php echo e(__('header.amount')); ?></th>
                        <th><?php echo e(__('header.name')); ?></th>
                        <th><?php echo e(__('header.action')); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5" align="center">
                            <?php echo e(__('header.search_first')); ?>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>