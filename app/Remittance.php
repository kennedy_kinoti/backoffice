<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remittance extends Model
{
    public $timestamps = false;
    public $table = "micstation_conf_ipay_ke.ipay_pymnt_tbl_ke";
}
