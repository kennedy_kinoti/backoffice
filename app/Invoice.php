<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * Invoice Model Functions
     * 1. Create sub account
     * 2. View Sub Accounts
     */
    public $table = "ipay_invoices";
    public $timestamps = false;
}
