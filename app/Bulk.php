<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulk extends Model
{
    /**
     * Invoice Model Functions
     * 1. Create sub account
     * 2. View Sub Accounts
     */
    //config('app.second_database')
    //public $table = "micstation_ipay_mer_ke.demo_bulk_inbox";
    public $timestamps = false;
    public $table = "micstation_ipay_mer_ke.0000_bulk_inbox";
}
