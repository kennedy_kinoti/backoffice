<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use View;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //$this->registerPolicies();

        //Defining gates
        // Gate::define('is_checked', function($user, $checked_action){ //eg create_invoices
        //     if (json_decode($user->views_parameters)->$checked_action == 1) {
                
        //        return true;
        //     }
        //     return false;
        // });

        //User role gates
        // Gate::define('user_type', function($user, $user_role){
        //     if($user->user_role == $user_role) {
        //         return true;
        //     }
        //     return false;
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
