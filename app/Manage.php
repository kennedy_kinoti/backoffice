<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manage extends Model
{
    /**
     * Manage Model Functions
     * 1. Create sub account
     * 2. View Sub Accounts
     * 3. Edit Sub Accounts
     * 4. Change roles and responsibilities
     */
    public $table = "ipay_users";

    const UPDATED_AT = 'lastloggedin';
}
