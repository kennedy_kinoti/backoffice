<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    /**
     * Table associated with this model
     */
    protected $table = 'ipay_logs';
    
    const UPDATED_AT = 'complete_date';

    protected $connection = 'mysql';
    // protected  $primaryKey = 'reference_id';
    protected $fillable =  ['completor_id', 'complete_date', 'state'];
    
}
