<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Send email job
        // $data = array('name'=>"Ken Kinoti");

        // Mail::send(['text'=>'mail.email'], $data, function($message) {
        //     $message->to('kennedy@ipayafrica.com', 'Kennedy iPay Africa')->subject
        //        ('Greeting Message from iPay Africa');
        //     $message->from('technical@ipayafrica.com','iPay Africa Backoffice Auto Mail');
        //  });

         $data = array('name' => $names, 'email' => $email, 'invoice_no' => $inv_no, 'date' => $date, 'itinerary' => $itinerary, 'curr' => $curr, 'amount' => $amount);

        Mail::send(['text'=>'mail.initiated_refund'], $data, function($message) use ($names, $email, $subject) {
            $message->to($email, $names)->subject
               ($subject);
            $message->from('technical@ipayafrica.com','iPay Africa Backoffice Auto Mail');
        });
    }
}
