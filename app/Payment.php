<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * Table associated with the model
     * 
     */
    protected $connection = 'mysql2';
    protected $table = '0000_inbox_new';

    public $timestamps = false;
}
