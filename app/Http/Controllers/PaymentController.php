<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Payment;
use PDF;

class PaymentController extends Controller
{
    /**
     * Payment Controller functions
     * 1. Download/ Export option - Done
     * 2. Filter results - Done
     * 3. Modal with more details - Done
     * 4. Modal with Export options - Done
     * 5. Modal with Refund option - Done
     * 6. Logic for filter
     * 7. Logic for modal details
     */

    public function index() {
        return view('payment/index');
    }

    public function vue(){
        return view('payment/vue');
    }

    public function search(Request $request) {

        if($_GET['submit'] == 'search'){

            $code = trim($request->input('code'));
            //$date_to = $request->input('date_to');
            //$date_from = $request->input('date_from');
            $datefilter = $request->input('datefilter');

            if (!empty($datefilter)) {
                $date = explode("/", $datefilter);
                $date_from = trim($date[0]). " 00:00:00";
                $date_to = trim($date[1]). " 23:59:59";
            }else{
                $date_from = $date_to = NULL;
            }

            if (!empty($datefilter)) {
                $data = Payment::whereBetween('receivingdatetime', [$date_from, $date_to])
                ->orderBy('id', 'desc')
                ->paginate(10);
            }
            else if (!empty($code)) {
                $data = Payment::where('txncode', 'like', '%'.$code.'%')
                ->orderBy('id', 'desc')
                ->paginate(10);
            }
            else if (!empty($code) && !empty($datefilter)) {
                $data = Payment::where('txncode', 'like', '%'.$code.'%')
                ->whereBetween('receivingdatetime', [$date_from, $date_to])
                ->orderBy('id', 'desc')
                ->paginate(10);
            }
            else{
                return redirect()->back();
            }

            $data->appends(request()->query());

            if (count($data) > 0) {
                //dd($data);
                return view('payment.index', compact(['data']));

            } else {
                //return $data = "No data";
                return redirect()->back();
            }
            
        } elseif($_GET['submit'] == 'export') {

            $code = trim($request->input('code'));
            $date_from = $request->input('date_from');
            $date_to = $request->input('date_to');

            $data = Payment::where('txncode', 'like', '%'.$code.'%')
                                ->orWhereBetween('receivingdatetime', array($date_from, $date_to))
                                ->orderBy('id', 'desc')
                                ->get();
            
            if ($data->count()) {
                // dd($data);
                $pdf = PDF::loadView('pdf/payment_pdf', compact('data'));

                return $pdf->download('Payments.pdf');

            } else {
                return $data = "";
            }
        }
    }

    public function vue_result(Request $request){
        $search = trim($request->input('search'));
        $date_from = $request->input('date_from');
        $date_to = $request->input('date_to');

        $payments = Payment::where('txncode', 'like', '%'.$search.'%')
                            ->orWhereBetween('receivingdatetime', array($date_from, $date_to))
                            ->orderBy('id', 'desc')
                            ->paginate(5);
        
        // $payments = Payment::all();

        if ($payments->count()) {
            return $payments;
        } else {
            return $payments = "";
        }
    }
}
