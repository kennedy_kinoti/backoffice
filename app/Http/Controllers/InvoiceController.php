<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendEmailJob;
use App\Invoice;
use App\Refund;
use Mail;

class InvoiceController extends Controller
{
    /*
    * Invoice controller functions
    * 1. View invoices
    * 2. Create Invoices
    * 3. Initiate sending
    * 4. Approve or reject invoice sending
    * 5. Send an email
    */
    public function index(){
        // View all the pending invoices

        $pending_invoices = Invoice::where('state', '=', 0)  
                            ->orderBy('date', 'desc')
                            ->paginate(10);

        if ($pending_invoices->count()) {
            return view('invoice/index', compact('pending_invoices'));
        } else {
            $pending_invoices = "";
            return view('invoice/index', compact('pending_invoices'));
        }

    }

    public function view_sent(){
        // View all invoices
        $invoices = Invoice::orderBy('date', 'desc')
                            ->paginate(10);
        
        if ($invoices->count()) {
            return view('invoice/sent', compact('invoices'));
        } else {
            return $invoice = "";
        }
    }

    public function create(){
        // Create new invoice
        return view('invoice/create');
    }

    public function initiate(Request $request){
        // Initiate the sending of an invoice
        $names = $request->name;
        $email = $request->email;
        $tel_number = $request->tel_number;
        $itinerary = $request->itinerary;
        $inv_no = $request->inv_no;
        $order_id = $request->order_id;
        $curr = $request->curr;
        $date = date('Y-m-d h:i:s');
        $amount = $request->amount;
        $token = $request->_token;
        $subject = 'iPay Notice: Invoice Initiated';

        // Upload the photo to local files
        $image = $request->file('logo');
        $destinationPathImg = public_path('invoice_logos');
        if (!$image->move($destinationPathImg, $image->getClientOriginalName())) {
            return 'Error saving the file.';
        }

        // Update Invoice table
        $insert_trans = Invoice::insert(['date' => $date, 'vendor_id' => '0000', 'names' => $names, 'email' => $email, 'phonenumber' => $tel_number, 'invoice_number' => $inv_no, 'order_id' => $order_id, 'currency' => $curr, 'amount' => $amount, 'itinerary' => $itinerary, 'state' => '0', 'token' => $token, 'proformainvoice' => '0', 'logo' => $image->getClientOriginalName()]);   

        // Create log data
        $log = Refund::insert(['initiate_id' => '0', 'initiate_date' => date("Y-m-d H:i:s"),            'completor_id' => '0', 'complete_date' => date("Y-m-d H:i:s"), 'state' => '0', 'reference_type' => 'Invoice', 'reference_id' => $inv_no, 'vendor_id' => '0000', 'reference_details' => 'invoice', 'country' => 'KE']);

        // Send email to confirm initiate email
        // SendEmailJob::dispatch()
        //         ->delay(now()->addSeconds(5));
        $data = array('name' => $names, 'email' => $email, 'invoice_no' => $inv_no, 'date' => $date, 'itinerary' => $itinerary, 'curr' => $curr, 'amount' => $amount);

        Mail::send(['text'=>'mail.initiated_refund'], $data, function($message) use ($names, $email, $subject) {
            $message->to($email, $names)->subject
               ($subject);
            $message->from('technical@ipayafrica.com','iPay Africa Backoffice Auto Mail');
         });

        return redirect('invoice/create')->with('successMsg', 'Invoice Saved. This invoice will be sent when relevant user confirms');
    }

    public function confirm(Request $request){
        // Approve
        // dd($request->id);
        $id = $request->id;
        $subject = 'iPay Company Invoice';

        if($_POST['submit'] == 'confirm'){

            // SendEmailJob::dispatch()
            //     ->delay(now()->addSeconds(5));
            
            // Update database
            $invoices = Invoice::where('id', '=', $id)  
                            ->update(["state" => '1']);

            dd($invoices);

            // Update logs data
            // $update_logs = Refund::where('reference_id', '=', $ref_id)
            //                     ->update(["completor_id" => "98", "complete_date" => date("Y-m-d H:i:s"), "state" => "1" ]);

            $data = array('name'=>"Ken Kinoti");

            // Send email to confirm delivery
            Mail::send(['text'=>'mail.invoice'], $data, function($message) use($subject) {
                $message->to('kennedy@ipayafrica.com', 'Kennedy iPay Africa')->subject
                   ($subject);
                $message->from('technical@ipayafrica.com','iPay Africa Backoffice Auto Mail');
            });

            return redirect('/invoice')->with('successMsg', 'Invoice has been completed and sent');
        }
        // Reject 
        elseif($_POST['submit'] == 'reject') {
            return redirect('/invoice')->with('failMsg', 'Invoice has been rejected');
        }

        // Update record in database
       
    }

    public function resend_invoice(Request $request){
        // send invoice to recepient
        dd("Resend this email function ".$request->id);
    }
}
