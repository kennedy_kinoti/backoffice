<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Bulk;
use DB;

class BulkController extends Controller
{
    /*
    * Bulk controller functions
    * 1. View 
    * 2. Manage 
    */
    public function index(){
        $invoices = Bulk::orderBy('dttime', 'desc')
                            ->paginate(15);
                            //->get();
                            
        if ($invoices) {
            return view('bulk/index', compact('invoices'));
        } else {
            return $invoice = "";
        }
    }

    public function search(Request $request){

        if($_GET['submit'] == 'search'){
            
            $search = $request->get('search');
            $invoices = Bulk::where('ref', 'like', '%'.$search.'%')
            ->orWhere('telephone', 'like', '%'.$search.'%')
            ->orWhere('mmref', 'like', '%'.$search.'%')
            ->orWhere('dttime', 'like', '%'.$search.'%')
            ->orderBy('dttime', 'desc')
            ->paginate(15);

            $invoices->appends(request()->query());

            if (count($invoices) > 0) {
                return view('bulk/index', compact('invoices'));
            } else {
                return redirect()->back();
            }            
        }
    }

    public function manage(){
        // Manage
        return view('bulk/manage');
    }

    public function archived(){
        // Archived
        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        // foreach ($month as $key => $value) {
        //     echo ($key+1).' - '.$value.'<br>';

        // }
        return view('bulk/archived', compact('months'));
    }

    public function archived_data(Request $request){
        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

        $month = $request->get('month'); //2 yymmdd
        $year = $request->get('duration');

        if($month != "Pick Month To Archive" && $year == "Pick Year To Archive")
        {
            $query = Bulk::whereMonth('dttime', '=', date($month))
                    ->orderBy('dttime', 'desc')
                    ->paginate(15);
        }
        else if ($year != "Pick Year To Archive" && $month == "Pick Month To Archive") 
        {
            $query = Bulk::whereYear('dttime', '=', date($year))
                    ->orderBy('dttime', 'desc')
                    ->paginate(15);
        }
        else if ($month != "Pick Month To Archive" && $year != "Pick Year To Archive")
        {
            $query = Bulk::whereMonth('dttime', '=', date($month))
                    ->whereYear('dttime', '=', date($year))
                    ->orderBy('dttime', 'desc')
                    ->paginate(15);
        }
        else    {
            return redirect()->back();
        }

        $query->appends(request()->query());
        
        if (count($query) > 0)
        {
            return view('bulk/archived', compact(['query', 'months']));
        } else {
            return redirect()->back();
        } 
    }
}
