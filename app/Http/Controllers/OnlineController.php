<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class OnlineController extends Controller
{
    /**
     * Online Controller Functions
     * 1. Success
     * 2. Fail
     * 3. Suspense
     * 4. Refund
     * 5. Download PDF
     */

    public function success() {
        return view('/online/success');
    }

    public function suspense() {
        return view('/online/suspense');
    }

    public function fail() {
        return view('/online/fail');
    }

    public function refund() {
        return view('/online/refund');
    }

    //Dowload PDF files
    public function pdf() {
        // return view('/account/index');
        $data = ['title' => 'Online Successful Transactions'];
        $pdf = PDF::loadView('pdf/payment_pdf', $data);

        return $pdf->download('Payment.pdf');
    }

    // Filter the search query
    public function filter() {
        return view('/online/fail');
    }
}
