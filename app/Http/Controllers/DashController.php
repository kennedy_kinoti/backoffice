<?php

namespace App\Http\Controllers;
use App\Manage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Dash;
use Lava;
use Mail;

class DashController extends Controller
{
    /**
     * Online Controller Functions
     * 1. Dashboard Charts
     * 2. My Profile
     * 3. Manage Users
     */

    public function index() {
        $reasons = Lava::DataTable();

        $bar_graph = Lava::Datatable();

        $reasons->addStringColumn('Reasons')
                ->addNumberColumn('Amount')
                ->addRow(['Check Reviews', 5])
                ->addRow(['Watch Trailers', 2])
                ->addRow(['See Actors Other Work', 4])
                ->addRow(['Settle Argument', 89]);

        $bar_graph->addStringColumn('Months')
                ->addNumberColumn('Percent')
                ->addRow(['January', 15])
                ->addRow(['Fabruary', 53])
                ->addRow(['March',3])
                ->addRow(['April', 40])
                ->addRow(['May', 2])
                ->addRow(['June', 55])
                ->addRow(['July', 75])
                ->addRow(['August', 85])
                ->addRow(['September', 105])
                ->addRow(['October', 45])
                ->addRow(['November', 35])
                ->addRow(['December', 5]);

        Lava::PieChart('IMDB', $reasons, [
            'title'  => 'Reasons I visit IMDB',
            'is3D'   => true,
            'slices' => [
                ['offset' => 0.2],
                ['offset' => 0.25],
                ['offset' => 0.3]
            ]
        ]);

        Lava::ColumnChart('IMDB', $bar_graph, [
            'title'  => 'Transactions',
            'is3D'   => false
        ]);
       
        return view('/dashboard/index', ["reasons" => $reasons]);
    
    }

    public function profile() {
        return view('/dashboard/profile');
    }

    public function create(){
        return view('/dashboard/create');   
    }

    public function manage() {
        // Get the users from database
        $users = DB::table('ipay_users')
                    ->where('state', '=', '1')
                    ->get();

        return view('dashboard/manage', ['users' => $users]);

        // User access control
    }

    public function store(Request $request) {
        // Add users to the database

        // $request->validate([
        //     // ... other stuff
        //     'password' => 'required|confirmed'
        // ]);

        $names = $request->name;
        $email = $request->email;
        $username = $request->username;
        $password = bcrypt($request->password);
        // var_dump($names, $email,$username, $password);
        // die();

        $views_parameters = json_encode(['create_invoices' => $request->create_invoices, 'manage_invoices' => $request->manage_invoices,'confirm_invoices' => $request->confirm_invoices,'add_users' => $request->add_users,'manage_users' => $request->manage_users, 'refunded_payments' => $request->refunded_payments, 'initiated' => $request->initiated, 'completed' => $request->completed, 'online_success' => $request->online_success,'online_suspense' => $request->online_suspense,'online_failed' => $request->online_failed,'online_initiate_ref' => $request->online_initiate_ref,'online_complete_ref' => $request->online_complete_ref,'virtual_pos' => $request->virtual_pos,'pos_pdq_service' => $request->pos_pdq_service, 'add_sub_accounts' => $request->add_sub_accounts, 'sub_accounts_action' => $request->sub_accounts_action, 'manage_bulk_pay' => $request->manage_bulk_pay,'init_bulkpay' => $request->init_bulkpay,'fund_bulkpay' => $request->fund_bulkpay,'view_bp_reports' => $request->view_bp_reports,'view_bp_archived' => $request->view_bp_archived,'bank_remittance' => $request->bank_remittance,'device_web' => $request->device_web,'device_app' => $request->device_app,'device_all' => $request->device_all]);

        $timestamp = date('Y-m-d H:i:s');

        // dd($views_parameters);
        $insert_trans = Dash::insert([ 'vendor_id' => '0000', 'sub_account' => '001', 'name' => $names, 'email' => $email, 'username' => $username, 'password' => $password, 'views_parameters' => $views_parameters, 'login_device' => '3', 'app_trans_view' => '1', 'online_state' => '1', 'state' => '1', 'main_account' => '0', 'user_database' => 'micstation_ipay_mer_ke', 'currency' => 'KES', 'country_code' => 'KE', 'currentstamp' => $timestamp, 'lastloggedin' => $timestamp, 'remember_token' => '']);

        // Send an email to the created user
        $data = array('name' => $names);
     
        // Mail::send(['text'=>'mail.user_create'], $data, function($message) {
        //    $message->to('kennedy@ipayafrica.com', 'Kennedy Kinoti')->subject
        //       ('Created as a user. Backoffice iPay Africa');
        //    $message->from('technical@ipayafrica.com','Backoffice iPay Africa Auto Email');
        // });
        Log::channel('daily')->info('Successfully created user');

        return redirect('user/manage')->with('successMsg', 'Success! User *'.$names.'* has been created');
    }

    function updated_profile(Request $request){
        $name = $request->name;
        $email = $request->email;
        $username = $request->username;
        $vid = $request->vid;
        
        if(!empty($request->password)){
            $password = bcrypt($request->password);
            Manage::where('id', auth()->user()->id)
            ->update(['name'=>$name, 'email' => $email, 'vendor_id' => $vid, 'username' => $username, 'password' => $password]);
        } else {
            Manage::where('id', auth()->user()->id)
            ->update(['name'=>$name, 'email' => $email, 'vendor_id' => $vid, 'username' => $username]);
        }

        // var_dump($query);
        // die();

            return view('dashboard/profile');
    }

    public function edit($id) {
        // Edit a user details
        $users = \App\Manage::find($id);

        return view('dashboard/edit', ['users' => $users]);
    }

    
    public function delete($id) {
        // Delete a user from the database
        // dd($id);
        // $users = \App\Manage::find($id);

        // dd($users);

        $update_logs = \App\Manage::where('id', '=', $id)
                                ->update(["state" => "0"]);

        return redirect('user/manage')->with('successMsg', 'The user has been deleted');
                
    }

    
    public function update(Request $request, $user) {
        // update a users roles and profile
        $users = \App\Manage::find($user);

        $users->name = $request->name;
        $users->email = $request->email;
        $users->username = $request->username;
        if(!empty($request->password)){
            $users->password = bcrypt($request->password);
        }

        // var_dump ($users->name, $users->email, $users->username, $users->password);
        // die();

        $users->views_parameters = json_encode(['create_invoices' => $request->create_invoices, 'manage_invoices' => $request->manage_invoices,'confirm_invoices' => $request->confirm_invoices,'add_users' => $request->add_users,'manage_users' => $request->manage_users, 'refunded_payments' => $request->refunded_payments, 'initiated' => $request->initiated, 'completed' => $request->completed, 'online_success' => $request->online_success,'online_suspense' => $request->online_suspense,'online_failed' => $request->online_failed,'online_initiate_ref' => $request->online_initiate_ref,'online_complete_ref' => $request->online_complete_ref,'virtual_pos' => $request->virtual_pos,'pos_pdq_service' => $request->pos_pdq_service, 'add_sub_accounts' => $request->add_sub_accounts, 'sub_accounts_action' => $request->sub_accounts_action, 'manage_bulk_pay' => $request->manage_bulk_pay,'init_bulkpay' => $request->init_bulkpay,'fund_bulkpay' => $request->fund_bulkpay,'view_bp_reports' => $request->view_bp_reports,'view_bp_archived' => $request->view_bp_archived,'bank_remittance' => $request->bank_remittance,'device_web' => $request->device_web,'device_app' => $request->device_app,'device_all' => $request->device_all]);

        $users->save();

        return redirect('user/manage')->with('successMsg', 'Success! Data for *'.$request->name.'* has been updated');
    }

    public function notification() {
        // $client = new Client([
        //     'base_uri' => 'localhost:9000',
        //     'timeout'  => 2.0,
        // ]);

        // $response = $client->request('GET', '/');

        // dd($response->getBody()->getContents());
        // $answer = $response->getBody()->getContents();

        return view('dashboard/notification');
    }

}