<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Remittance;
use Auth;

class RemittanceController extends Controller
{
    //View Refund List
    public function index(){
        return view('remittance.index');
    }
    public function remittance_account(){
    	return view('account.rem_acc');
    }

    public function accounts(){
    	$data = Remittance::where('id', auth()->user()->id)
    			->get()->toArray();
    
    	return view('/account/index', compact('data'));
    }

    public function settlements (Request $request){
    	$mon = $request->get('monday');
    	$tue = $request->get('tuesday');
    	$wed = $request->get('wednesday');
    	$thur = $request->get('thursday');
    	$fri = $request->get('friday');
    	$monthly = $request->get('startmonth');
    	$fortnight = $request->get('midmonth');

    	if (is_null($mon)) {
    		$mon = 0;
    	}if (is_null($tue)) {
    		$tue = 0;
    	}if (is_null($wed)) {
    		$wed = 0;
    	}if (is_null($thur)) {
    		$thur = 0;
    	}if (is_null($fri)) {
    		$fri = 0;
    	}if (is_null($monthly)) {
    		$monthly = 0;
    	}if (is_null($fortnight)){
    		$fortnight = 0;
    	}
    	$today_date = date('Y-m-d H:i:s');

    	// var_dump();
    	// die();

    	Remittance::where('id', auth()->user()->id)
    			->update(['mon'=>$mon, 'tue'=>$tue, 'wed'=>$wed, 'thur'=>$thur, 'fri'=>$fri, 'startmnth' => $monthly, 'midmnth'=>$fortnight]);

    	return redirect('account');
    }
}
