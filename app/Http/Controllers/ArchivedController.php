<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArchivedController extends Controller
{
    /*
    * Archived controller functions
    * 1. Success 
    * 2. Suspense
    * 3. POS 
    */
    public function success(){
        // View success archived data
        return view('archived/success');
    }

    public function suspense(){
        // View suspense archived data
        return view('archived/suspense');
    }

    public function pos(){
        // View pos archived data
        return view('archived/pos');    
    }
}
