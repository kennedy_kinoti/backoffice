<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ManageController extends Controller 
{
    /**
     * Manage Controller Functions
     * 1. Create sub account
     * 2. View Sub Accounts
     * 3. Edit Sub Accounts
     * 4. Change roles and responsibilities
     */
    public function create() {
        return view('managesub/create');
    }

    public function list() {
        return view('managesub/list');
    }

}