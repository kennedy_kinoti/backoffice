<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendEmailJob;
use Carbon\Carbon;
use Mail;

class MailController extends Controller
{
    //Forgot password
    public function basic_email(){
     
        SendEmailJob::dispatch()
            ->delay(now()->addSeconds(5));

        return redirect('/')->with('successMsg', 'Test email sent. Kindly check your inbox');
     }
}
