<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Refund;
use Mail;
use PDF;

class RefundController extends Controller
{
    /**
     * 1. Create a refund (insert) - Not necessary
     * 2. Send email to all that complete refunds
     * 3. View refunds created - Done
     * 4. Complete a refund
     * 5. Send email on state of refund. Complete/ Processing
     */

    public function index() {
        return view('refund.index');
    }

    public function view() {
        $logged_trans = Refund::select('id','initiate_date','reference_id','vendor_id','reference_type')
                            ->where('state','=','0')
                            ->orderBy('initiate_date', 'DESC')
                            ->paginate(10);

        return view('refund.view', compact('logged_trans'));
        // return $logged_trans;
    }

    public function view_all() {
        $logged_trans = Refund::select('id','initiate_date','reference_id','vendor_id','reference_type','state')
                            ->where('state','!=','0')
                            ->orderBy('initiate_date', 'DESC')
                            ->paginate(10);

        return view('refunded_payment.index', compact('logged_trans'));
        // echo $logged_trans;
    }

    public function refund(Request $request) {
        $ref_id = $request->id;
        // Update logs state
        $update_logs = Refund::where('reference_id', '=', $ref_id)
                                ->update(["completor_id" => "98", "complete_date" => "2018-07-01 12:26:39", "state" => "1" ]);

        // Insert into inbox_refunded table
        $db_ext = DB::connection('mysql2');

        $complete_refund = $db_ext->table('0000_inbox_refunded')
                                    ->insert(['updatedindb' => date("Y-m-d H:i:s"),'receivingdatetime' => date("Y-m-d H:i:s"),'sendernumber' => 'MPESA','txncode' => '12yees12','fname' => 'Kennedy','lname' => 'Kinoti','msisdn' => '0715916968','vendorid' => '000','idscomm_fee' => '0.00','txnamt' => '0.00','curr' => 'KES','reconciled' => 'no']);

        // Delete from inbox_new
        $delete_refund_inbox = $db_ext->table('0000_inbox_new')
                                    ->where('txncode', '=', $ref_id)
                                    ->delete();

        // Email initiator
        $data = array('name'=>"Ken Kinoti");
     
        Mail::send(['text'=>'mail.initiated_refund'], $data, function($message) {
           $message->to('kennedy@ipayafrica.com', 'Kennedy iPay Africa')->subject
              ('Initiated Refund iPay Africa');
        //    $message->from('kinotikennedy@gmail.com','Ken Client');
        });

        // return redirect('/')->with('successMsg', 'Test email sent. Kindly check your inbox');

        // Redirect URL
        return redirect('refund/view')->with('successMsg', 'The transaction code '.$ref_id.' has been refunded');
    }

    public function decline_refund($ref_id){
        // Update logs state
        $update_logs = Refund::where('reference_id', '=', $ref_id)
                                ->update(["completor_id" => "98", "complete_date" => date("Y-m-d H:i:s"), "state" => "2" ]);

        // Email initiator
        $data = array('name'=>"Ken Kinoti");
     
        Mail::send(['text'=>'mail.declined_refund'], $data, function($message) {
           $message->to('kennedy@ipayafrica.com', 'Kennedy iPay Africa')->subject
              ('Declined Refund iPay Africa');
        //    $message->from('kinotikennedy@gmail.com','Ken Client');
        });

        // Delete from inbox_new
        $db_ext = DB::connection('mysql2');

        $delete_refund_inbox = $db_ext->table('0000_inbox_new')
                                    ->where('txncode', '=', $ref_id)
                                    ->delete();

        // Redirect URL
        return redirect('refund/view')->with('failMsg', 'Rejection of transaction code '.$ref_id.' has been completed');
    }

    public function complete() {
        $db_ext = DB::connection('mysql2');

        $refunded = $db_ext->table('0000_inbox_refunded')->select('id','updatedindb', 'receivingdatetime','sendernumber','txncode','fname','lname','vendorid','txnamt')
                            ->orderBy('receivingdatetime', 'DESC')
                            ->paginate(10);

        return view('refund.complete', compact('refunded'));
    }

    public function partial_initiate($id){
        // push transaction to logs table
        $db_ext = DB::connection('mysql2');

        $logs_data = $db_ext->table('0000_inbox_new')->select('*')
                                ->where('id', '=', $id)
                                ->get();    

        return view('refund.partial_refund', compact('logs_data'));
        
    }

    public function partial_submit(Request $request){
        // push transaction to logs table
        $id = $request->id;
        $db_ext = DB::connection('mysql2');

        $logs_data = $db_ext->table('0000_inbox_new')->select('vendorid', 'txncode')
                            ->where('id', '=', $id)
                            ->get();      
                                
        // dd($logs_data);

        if ($logs_data->count()){
            echo($logs_data[0]->vendorid);

            $log = Refund::insert(['initiate_id' => '0', 'initiate_date' => date("Y-m-d H:i:s"), 'completor_id' => '0', 'complete_date' => date("Y-m-d H:i:s"), 'state' => '0', 'reference_type' => 'partial_refund', 'reference_id' => $logs_data["0"]->txncode, 'vendor_id' => $logs_data["0"]->vendorid, 'reference_details' => '', 'country' => 'KE']);

            return redirect('refund/view')->with('successMsg', 'Partial Refund Successful');

        } else {
            echo 'No results found';
        }


        // trigger webhook and send email for partial refund to all who complete refund
        
    }

    public function full_initiate($id){
        // push transaction to logs table
        $db_ext = DB::connection('mysql2');

        $logs_data = $db_ext->table('0000_inbox_new')->select('vendorid', 'txncode')
                                ->where('id', '=', $id)
                                ->get();                        

        if ($logs_data->count()){
            echo($logs_data[0]->vendorid);

            $log = Refund::insert(['initiate_id' => '0', 'initiate_date' => date("Y-m-d H:i:s"), 'completor_id' => '0', 'complete_date' => date("Y-m-d H:i:s"), 'state' => '0', 'reference_type' => 'full_refund', 'reference_id' => $logs_data["0"]->txncode, 'vendor_id' => $logs_data["0"]->vendorid, 'reference_details' => '', 'country' => 'KE']);

            return redirect('refund/view')->with('successMsg', 'Full Refund Successful');

        } else {
            echo 'No results found';
        }

        // send email to all who complete refund

        
    }
}
