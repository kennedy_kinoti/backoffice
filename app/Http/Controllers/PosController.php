<?php

namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PosController extends Controller
{
    //POS COntroller Functions
    // 1. Inintiate PDQ 
    // 2. Payment details post to endpoint
    public function index(){
        
        return view('pos.index');
    }

    public function virtual_pos(){
        return view('pos.virtual');
    }

    public function virtual_pos_ipay(){

        $fields = array("live"=> "0",
            "oid"=> $_POST['oid'],
            "inv"=> $_POST['inv'],
            "ttl"=> $_POST['ttl'],
            "tel"=> $_POST['tel'],
            "eml"=> $_POST['eml'],
            "vid"=> "demoChanged",
            "curr"=> $_POST['curr'],
            "p1"=> "airtel",
            "p2"=> "020102292999",
            "p3"=> "",
            "p4"=> $_POST['ttl'],
            "cbk"=> $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"],
            "cst"=> "1",
            "crl"=> "2"
            );

        $datastring =  $fields['live'].$fields['oid'].$fields['inv'].$fields['ttl'].$fields['tel'].$fields['eml'].$fields['vid'].$fields['curr'].$fields['p1'].$fields['p2'].$fields['p3'].$fields['p4'].$fields['cbk'].$fields['cst'].$fields['crl'];

        $hashkey ="demoChanged";//use "demo" for testing where vid also is set to "demo"

        $generated_hash = hash_hmac('sha1',$datastring , $hashkey);

        // dd($datastring);

        $url = "https://payments.ipayafrica.com/v3/ke";
        return Redirect::to($url);
            
    }

    public function send(Request $request){
        // Post values to a virtual server that is created by a node
        
        $data = [
            'amount' => $request->amount,
            'desc' => $request->desc,
        ];
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('APP_POS')."/process_post",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            // echo "cURL Error #:" . $err;
            return redirect('pos')->with('failMsg', 'There has been an error connecting to the iPay PDQ. Transaction failed');
        } else {
            // print_r(json_decode($response));
            return redirect('pos')->with('successMsg', 'A payment request of KES '.$request->amount.' has been sent to the PDQ successfully');
        }

    }

    public function form(){
        
        return view('pos.form');
    }

    public function submit(Request $request){
        // Insert new transaction into inbox_new_table
        // dd($request->sendernubmer);
        $db_ext = DB::connection('mysql2');

        $sendernumber = $request->sendernumber;
        $txncode = $request->txncode;
        $fname = $request->fname;
        $lname = $request->lname;
        $amount = $request->amount;
        $ldate = date('Y-m-d H:i:s');

        $insert_trans = $db_ext->table('0000_inbox_new')
                                    ->insert(['updatedindb' => $ldate,'receivingdatetime' => $ldate,'sendernumber' => $sendernumber,'txncode' => $txncode,'fname' => $fname,'lname' => $lname,'msisdn' => '0715916968','vendorid' => '0000','idscomm_fee' => '1.00','txnamt' => $amount,'curr' => 'KES','runid' => 'no']);

        return redirect('payments')->with('successMsg', 'A payment request of KES '.$request->amount.' has been sent to the database successfully');
    }

    public function webhooks(){
        
    }
}
