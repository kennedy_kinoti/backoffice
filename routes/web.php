<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Session selected Language
Route::get('setlocale/{locale}',function($lang){
       \Session::put('locale',$lang);
       return redirect()->back();
})->name('lang');

//Creating a group language route
Route::group(['middleware'=>'language'], function (){

Route::get('/', ['middleware' => 'auth','uses' => 'DashController@notification']);

// Dashboard
Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'DashController@index']);

Route::group(['prefix' => 'user',  'middleware' => 'auth'], function(){
    Route::get('profile', 'DashController@profile');
    Route::post('profile_update', 'DashController@updated_profile')->name('profile_update');
    Route::get('create', 'DashController@create');
    Route::get('manage', 'DashController@manage');
    Route::get('manage/{id}/edit', 'DashController@edit');
    Route::get('manage/{id}/delete', 'DashController@delete');
    Route::post('store', 'DashController@store');
    Route::post('manage/{id}/update', 'DashController@update');
});

// Manage Sub Accounts
Route::get('add-sub', 'ManageController@create');
Route::get('list-sub', 'ManageController@list');

// Payment routes
Route::group(['prefix' => 'payments',  'middleware' => 'auth'], function(){
    Route::get('/', 'PaymentController@index')->name('payments');
    // Search and Export
    Route::get('search', 'PaymentController@search')->name('payments/search');
});

// Payment
Route::get('payment', 'PaymentController@vue');

// Vue JS Routes
Route::get('vue-payment', 'PaymentController@vue');
Route::post('fetch', 'PaymentController@vue_result');
Route::post('fetch-dump', 'PaymentController@vue_dump');
Route::post('get-modal', 'PaymentController@get_modal');

// Refunds
Route::group(['prefix' => 'refund',  'middleware' => 'auth'], function(){  
    Route::get('view', 'RefundController@view');
    Route::get('create', 'RefundController@index');
    Route::get('process/{id}', 'RefundController@refund');
    Route::get('decline/{id}', 'RefundController@decline_refund');
    Route::get('complete', 'RefundController@complete');
    Route::get('pending', 'RefundController@pending');
    Route::get('partial_initiate/{id}', 'RefundController@partial_initiate');
    Route::post('partial_submit/{id}', 'RefundController@partial_submit');
    Route::get('full_initiate/{id}', 'RefundController@full_initiate');    
});

// Refunded Payments
Route::get('refunded', 'RefundController@view_all');

// Account
Route::get('account', 'RemittanceController@accounts');
Route::post('settlement', 'RemittanceController@settlements')->name('settle');
// Route::get('account', function ($lang = null) {

//     $currentRouteName = Route::currentRouteName();    
//     return view('/account/index');
// });

Route::get('rem_account', 'RemittanceController@remittance_account');

// Bank Remittance
Route::get('remittance', 'RemittanceController@index');

// Invoice
Route::group(['prefix' => 'invoice',  'middleware' => 'auth'], function(){
    Route::get('/', 'InvoiceController@index');
    Route::get('create', 'InvoiceController@create');
    Route::get('list','InvoiceController@view_sent');
    Route::post('initiate', 'InvoiceController@initiate');
    Route::post('confirm/{id}', 'InvoiceController@confirm');
    Route::post('resend_invoice/{id}','InvoiceController@resend_invoice');
});

// Bulk Pay
Route::group(['prefix' => 'bulk',  'middleware' => 'auth'], function(){
    Route::get('/', 'BulkController@index');
    Route::get('/search', 'BulkController@search')->name('bulk/search');
    Route::get('manage', 'BulkController@manage');
    Route::get('archived', 'BulkController@archived');
    Route::get('archived/data', 'BulkController@archived_data')->name('bulk_archive_data');
});

// B2B
Route::get('b2b','B2BController@index');

// POS
Route::group(['prefix' => 'pos',  'middleware' => 'auth'], function(){
    Route::get('/','PosController@index');
    Route::get('virtual','PosController@virtual_pos');
    Route::post('virtual_pay','PosController@virtual_pos_ipay');
    Route::post('send','PosController@send');
    Route::get('form','PosController@form');
    Route::post('submit','PosController@submit');//either post or get no sure, changed something
});

Route::get('webhooks','PosController@webhooks');

// Archived Online Data
Route::group(['prefix' => 'archived',  'middleware' => 'auth'], function(){
    Route::get('success', 'ArchivedController@success');
    Route::get('suspense', 'ArchivedController@suspense');
    Route::get('pos', 'ArchivedController@pos');
});

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

// Socialite URL's
Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');

// Mail 
Route::get('basicemail','MailController@basic_email');
}); //---End of lang group route

use App\Events\TaskEvent;

Route::get('event', function(){
    event(new TaskEvent('Hey there Ken. This is a sample event for test'));
});