#Backoffice iPay Africa

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
- PHP v7.1.3 (minimum)
- Composer
- NPM
- MySQL Database

##Installing
- Copy files to your local machine
- Navigate to the file path in your computer
- Run a composer install command in your terminal so as to install the dependancies
- You also need to have the following php packages installed
    
    ```
    php-xml
    php-gd
    php-mbstring
    ```

- Using the existing .env.example template, configure your environment variables and save the file .env

## Launching the program
- Run a php artisan serve command in your terminal and open the corresponding address

## Running the tests
Onging development and deployment tests

## Deployment
Not yet deployed to a live system

## Built With
- Laravel - The core framework used
- VUE JS - The JS frontend framework used
- Composer - Dependency Management

## Authors
Kennedy Kinoti

## Acknowledgments
Hat tip to anyone whose code was used

As Socrates said:
> True knowledge exists in knowing that you know nothing.

